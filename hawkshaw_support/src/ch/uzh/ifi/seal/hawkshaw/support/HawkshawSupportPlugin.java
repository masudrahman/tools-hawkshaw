package ch.uzh.ifi.seal.hawkshaw.support;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.support
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.eclipse.core.runtime.Plugin;
import org.osgi.framework.BundleContext;

import ch.uzh.ifi.seal.hawkshaw.support.plugin.IHawkshawPlugin;

public class HawkshawSupportPlugin extends Plugin implements IHawkshawPlugin {
	/**
	 *  The plug-in ID.
	 */
	public static final String PLUGIN_ID = "ch.uzh.ifi.seal.hawkshaw.support";
	
	/**
	 *  The shared instance.
	 */
	private static HawkshawSupportPlugin plugin;

	/**
	 * The constructor.
	 */
	public HawkshawSupportPlugin() {
		super();
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.runtime.Plugin#start(org.osgi.framework.BundleContext)
	 */
	@Override
	public void start(BundleContext context) throws Exception {
		super.start(context);

		plugin = this;
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.runtime.Plugin#stop(org.osgi.framework.BundleContext)
	 */
	@Override
	public void stop(BundleContext context) throws Exception {
		plugin = null;

		super.stop(context);
	}
	
	public static HawkshawSupportPlugin getDefault() {
		return plugin;
	}

	@Override
	public String getID() {
		// TODO Auto-generated method stub
		return null;
	}
}
