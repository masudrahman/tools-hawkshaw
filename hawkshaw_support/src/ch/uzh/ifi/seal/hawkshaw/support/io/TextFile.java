package ch.uzh.ifi.seal.hawkshaw.support.io;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.support
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

/*
 * Copyright 2009 University of Zurich, Switzerland
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.File;
import java.util.Iterator;

/**
 * This class allows iterating over a text file. Usage:
 * 
 * <pre>
 * TextFile tf = new TextFile(new File(&quot;myFile.txt&quot;));
 * for (String line : tf) {
 * 	System.out.println(line);
 * }
 * </pre>
 * 
 * @author wuersch
 */
public class TextFile implements Iterable<String> {

	private final File file;

	/**
	 * Constructor.
	 * 
	 * @param fileName
	 *            the file name
	 * @throws NullPointerException
	 *             If the <code>fileName</code> argument is <code>null</code>
	 */
	public TextFile(String fileName) {
		file = new File(fileName);
	}

	/**
	 * Constructor.
	 * 
	 * @param file
	 *            the file
	 * @throws NullPointerException
	 *             If the <code>file</code> argument is <code>null</code>
	 */
	public TextFile(File file) {
		this.file = file;
	}

	/**
	 * Returns the iterator.
	 * 
	 * @return the iterator
	 */
	public Iterator<String> iterator() {
		return new TextFileIterator(file);
	}

	/**
	 * Returns the content of this text file as a single string. Line breaks are
	 * preserved.
	 * 
	 * @return the content of this file.
	 */
	public String asString() {
		StringBuffer result = new StringBuffer();

		for (String line : this) {
			result.append(line).append('\n');
		}

		return result.toString();
	}
}
