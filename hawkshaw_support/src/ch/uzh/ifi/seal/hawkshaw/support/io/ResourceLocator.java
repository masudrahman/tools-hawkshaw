package ch.uzh.ifi.seal.hawkshaw.support.io;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.support
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Plugin;
import org.osgi.framework.Bundle;

import ch.uzh.ifi.seal.hawkshaw.support.HawkshawSupportPlugin;
import ch.uzh.ifi.seal.hawkshaw.support.exceptions.HawkshawException;

public final class ResourceLocator {
	private ResourceLocator() {
		super();
	}
	
	public static URL resolveUrl(Plugin context, String relativePath) {
		return resolveUrl(context.getBundle(), relativePath);
	}
	
	public static URL resolveUrl(Bundle contextBundle, String relativePath) {
		return FileLocator.find(contextBundle, new Path(relativePath), null);
	}
	
	public static String resolveAbsolutePath(Plugin context, String relativePath) {
		try {
			return FileLocator.resolve(resolveUrl(context, relativePath)).getFile();
		} catch (IOException e) {
			throw new HawkshawException(HawkshawSupportPlugin.getDefault(),
										"Could not resolve absolute path for " + relativePath,
										e);
		}
	}
	
	public static InputStream openFile(Plugin context, String relativePath) throws IOException {
		return FileLocator.openStream(context.getBundle(), new Path(relativePath), false);
	}
}
