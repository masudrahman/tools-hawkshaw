package ch.uzh.ifi.seal.hawkshaw.support.io;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.support
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;

/**
 * Iterator that allows to iterate over each line of a text file.
 * 
 * @author wuersch
 */
public class TextFileIterator implements Iterator<String> {
	private BufferedReader in;
	private String nextLine;

	/**
	 * Constructor.
	 */
	public TextFileIterator(File file) {
		try {
			in = new BufferedReader(new FileReader(file));
			nextLine = in.readLine();
		} catch (IOException e) {
			throw new IllegalArgumentException(e);
		}
	}

	/**
	 * Returns whether there are more lines.
	 * 
	 * @return <code>true</code>, if there are more lines; <code>false</code>
	 *         otherwise
	 */
	public boolean hasNext() {
		return nextLine != null;
	}

	/**
	 * Returns the next line.
	 * 
	 * @return next line
	 */
	public String next() {
		try {
			String result = nextLine;
			if (nextLine != null) {
				nextLine = in.readLine();
				if (nextLine == null) {
					in.close();
				}
			}

			return result;
		} catch (IOException e) {
			throw new IllegalArgumentException(e);
		}
	}

	/**
	 * Not supported.
	 * 
	 * @throws UnsupportedOperationException.
	 */
	public void remove() {
		throw new UnsupportedOperationException();
	}
}