package ch.uzh.ifi.seal.hawkshaw.ontology.issue_history.linker;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ontology.issue-history
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;

import ch.uzh.ifi.seal.hawkshaw.ontology.OntologyCore;
import ch.uzh.ifi.seal.hawkshaw.ontology.model.PersistentJenaModel;
import ch.uzh.ifi.seal.hawkshaw.support.jobs.MutexRule;

public class LinkerJob extends Job {
	private IProject project;
	
	private PersistentJenaModel model;
	
	public LinkerJob( IProject project) {
		super("Linking Issues and Revisions");
		this.project = project;
		
		setRule(MutexRule.INSTANCE);

		setUser(true);
	}
	
	@Override
	protected IStatus run(IProgressMonitor monitor) {
		monitor.beginTask("Linking Issues and Revisions", IProgressMonitor.UNKNOWN);
		
		initModel();
		constructLinks();
		
		
		return Status.OK_STATUS;
	}
	
	private void constructLinks() {
		ConstructQuery query = new ConstructQuery(model);
		query.exec();
	}
	
	private void initModel() {
		OntologyCore.waitForInit(null);
		model = OntologyCore.fetchPersistentModelFor(project);
		
		model.readOntologies("http://se-on.org/ontologies/hawkshaw/2012/02/issues-history-code-nl.owl",
							 "http://se-on.org/ontologies/hawkshaw/2012/02/issues-history-code.owl",
							 "http://se-on.org/ontologies/hawkshaw/2012/02/issues-history-nl.owl",
							 "http://se-on.org/ontologies/hawkshaw/2012/02/issues-history.owl");
	}
	
	private static final class ConstructQuery {
		private static final String QUERY = "PREFIX fn: <http://www.w3.org/2005/xpath-functions#>" +
											"PREFIX hwk-h: <http://se-on.org/ontologies/hawkshaw/2012/02/history.owl#> " +
											"PREFIX hwk-i: <http://se-on.org/ontologies/hawkshaw/2012/02/issues.owl#> " +
											"PREFIX hwk-ih: <http://se-on.org/ontologies/hawkshaw/2012/02/issues-history.owl#> " +
											"CONSTRUCT { ?v hwk-ih:fixesIssue ?i . }" +
											"WHERE { ?v hwk-h:hasCommitMessage ?message . " +
											"        ?i hwk-i:hasKey ?key . "+
											"FILTER regex(?message, fn:concat(?key, \"[^0-9]\"), \"i\") }";
		
		private PersistentJenaModel input;
		
		public ConstructQuery(PersistentJenaModel input) {
			this.input = input;
		}
		
		public void exec() {
			input.execConstructQuery(QUERY);
		}
	}
}
