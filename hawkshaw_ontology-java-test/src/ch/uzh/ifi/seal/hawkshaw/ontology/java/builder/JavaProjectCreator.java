package ch.uzh.ifi.seal.hawkshaw.ontology.java.builder;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ontology.java.test
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectDescription;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.jobs.IJobManager;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.launching.JavaRuntime;

import ch.uzh.ifi.seal.hawkshaw.support.io.DirectoryListing;
import ch.uzh.ifi.seal.hawkshaw.support.io.TextFile;
import ch.uzh.ifi.seal.hawkshaw.support.misc.IPredicate;

// TODO check progress handling
public class JavaProjectCreator {
	private File projectRoot;
	private IProject theProject;
	private IJavaProject theJavaProject;
	private IPackageFragmentRoot sourceFolder;

	public JavaProjectCreator(File projectRoot) {
		if (projectRoot == null || !projectRoot.exists() || !projectRoot.isDirectory()) {
			throw new InvalidProjectRootException("Invalid project root.", projectRoot);
		}

		this.projectRoot = projectRoot;
	}

	public void createJavaProject(IProgressMonitor monitor) throws CoreException {
		SubMonitor sub = SubMonitor.convert(monitor, 8);
		sub.beginTask("Creating Java Project.", 8);

		if (theJavaProject == null) {
			createProject(sub.newChild(1));
			convertToJavaProject(sub.newChild(1));
			
			addSystemLibraries(sub.newChild(1));
			addOutputFolder(sub.newChild(1));
			addSourceFolder(sub.newChild(1));
			addLibFolder(sub.newChild(1));
		
			addSourceFiles();
			sub.worked(1);
			addLibFiles();
			sub.worked(1);
		}
	}
	
	public void buildProject(IProgressMonitor monitor) throws CoreException {
		theProject.build(IncrementalProjectBuilder.FULL_BUILD, monitor);
	}
	
	/**
	 * Blocks until all builders have completed their work. If an
	 * {@link InterruptedException} occurs while waiting for completion, the
	 * exception is silently caught and blocking is resumed.
	 */
	public void waitForBuild() {
		IJobManager jobManager = Job.getJobManager();
		
		// we need to ensure that the build has completed:
		// for some strange reason join-thread gets interrupted
		// once in a while (at least on windows).
		boolean buildHasntFinishedYet = true;
		while(buildHasntFinishedYet) { 
			try {
				jobManager.join(ResourcesPlugin.FAMILY_AUTO_BUILD, null);
				jobManager.join(ResourcesPlugin.FAMILY_MANUAL_BUILD, null);
				
				buildHasntFinishedYet = false;
			} catch (InterruptedException e) {
				buildHasntFinishedYet = true; // 
			}
		}
	}
	
	public void addNatureToProject(String natureId, IProgressMonitor monitor) throws CoreException {
		SubMonitor sub = SubMonitor.convert(monitor, 5);
		sub.beginTask("Adding " + natureId + " nature to project.", 5);
		
		IProjectDescription description = theProject.getDescription();
		String[] natures = description.getNatureIds();
		sub.worked(1);
		
		String[] newNatures = new String[natures.length + 1];
		System.arraycopy(natures, 0, newNatures, 0, natures.length);		
		newNatures[natures.length] = natureId;
		sub.worked(1);
		
		description.setNatureIds(newNatures);
		sub.worked(1);
		
		theProject.setDescription(description, sub.newChild(2));
	}

	public void deleteProject(IProgressMonitor monitor) throws CoreException {
		SubMonitor sub = SubMonitor.convert(monitor, 1);
		sub.beginTask("Deleting project.", 1);
	
		if (theProject != null && theProject.exists()) {
			theProject.delete(true, true, sub.newChild(1));
	
			theProject = null;
			theJavaProject = null;
		} else {
			throw new ProjectDoesNotExistException("No project to delete.");
		}
	}

	public IProject getProject() {
		return theProject;
	}

	public IJavaProject getJavaProject() {
		return theJavaProject;
	}

	private void createProject(IProgressMonitor monitor) throws CoreException {
		SubMonitor sub = SubMonitor.convert(monitor, 3);
		sub.beginTask("Creating Project.", 3);
	
		IWorkspaceRoot root = ResourcesPlugin.getWorkspace().getRoot();
		theProject = root.getProject(projectRoot.getName());
	
		if (theProject.exists()) {
			deleteProject(sub.newChild(1));
		}
	
		sub.setWorkRemaining(2);
	
		theProject.create(sub.newChild(1));
		theProject.open(sub.newChild(1));
	}

	private void convertToJavaProject(SubMonitor monitor) throws CoreException {
		SubMonitor sub = SubMonitor.convert(monitor, 1);
		sub.beginTask("Converting project to Java project.", 1);
		
		setJavaNature(sub.newChild(1));
		theJavaProject = JavaCore.create(theProject);
	}

	private void setJavaNature(IProgressMonitor monitor) throws CoreException {
		SubMonitor sub = SubMonitor.convert(monitor, 4);
		sub.beginTask("Adding Java nature.", 4);
		
		IProjectDescription description = theProject.getDescription();
		sub.worked(1);
		
		description.setNatureIds(new String[] { JavaCore.NATURE_ID });
		sub.worked(1);
		
		theProject.setDescription(description, sub.newChild(2));
	}

	private void addSystemLibraries(IProgressMonitor monitor) throws JavaModelException {
		SubMonitor sub = SubMonitor.convert(monitor, 1);
		sub.beginTask("Adding default class path entries.", 1);
		
		// discard existing raw class path (=the project's root). 
		theJavaProject.setRawClasspath(new IClasspathEntry[] { JavaRuntime.getDefaultJREContainerEntry() }, sub.newChild(1));
	}

	private void addOutputFolder(IProgressMonitor monitor) throws CoreException {
		SubMonitor sub = SubMonitor.convert(monitor, 2);
		sub.beginTask("Adding default output folder.", 2);
		
		IFolder binFolder = createFolder("bin", sub.newChild(1));
		theJavaProject.setOutputLocation(binFolder.getFullPath(), sub.newChild(1));
	}

	private void addSourceFolder(IProgressMonitor monitor) throws CoreException {
		SubMonitor sub = SubMonitor.convert(monitor, 2);
		sub.beginTask("Adding default src folder.", 2);
				
		sourceFolder = theJavaProject.getPackageFragmentRoot(createFolder("src", sub.newChild(1)));
		
		addClassPathEntry(JavaCore.newSourceEntry(sourceFolder.getPath()), sub.newChild(1));
	}

	private void addLibFolder(IProgressMonitor monitor) throws CoreException {
		SubMonitor sub = SubMonitor.convert(monitor, 1);
		sub.beginTask("Creating lib folder.", 1);
		
		createFolder("lib", monitor);		
	}

	private void addSourceFiles() throws JavaModelException {
		DirectoryListing sourceListing = new DirectoryListing(new File(projectRoot, "src"), new IPredicate<File>() {
			@Override
			public boolean evaluate(File file) {
				return file.getName().endsWith(".java");
			}
		});
		
		for(File javaFile : sourceListing) {
			addJavaFile(javaFile);
		}
	}

	private void addJavaFile(File file) throws JavaModelException {
		String path = file.getPath();
		String pckg = path.substring(path.indexOf("src") + 4, path.lastIndexOf(File.separator)).replaceAll("\\" + File.separator, ".");
		
		IPackageFragment fragment = sourceFolder.createPackageFragment(pckg, false, null);
		fragment.createCompilationUnit(file.getName(), readFileContent(file), false, null);
	}

	private void addLibFiles() throws CoreException {
		DirectoryListing jarListing = new DirectoryListing(new File(projectRoot, "lib"), new IPredicate<File>() {
			@Override
			public boolean evaluate(File file) {
				return file.getName().endsWith(".jar");
			}
		});
		
		for(File jarFile : jarListing) {
			try {
				addJarFile(jarFile);
			} catch (FileNotFoundException fnfe) {
				// should never happen, but let's play safe and fail-fast.
				throw new RuntimeException("Unexpected error while adding jar file to project.", fnfe); // TODO own exception
			}
		}
	}

	private void addJarFile(File jarFile) throws CoreException, FileNotFoundException {
		InputStream is = new BufferedInputStream(new FileInputStream(jarFile));

		IFile libFile = theProject.getFile("lib" + File.separator + jarFile.getName());
		libFile.create(is, false, null);
		IPath path = libFile.getFullPath();

		addClassPathEntry(JavaCore.newLibraryEntry(path, null, null), null);
	}

	private IFolder createFolder(String folderName, IProgressMonitor monitor) throws CoreException {
		SubMonitor sub = SubMonitor.convert(monitor, 1);
		sub.beginTask("Creating " + folderName + "folder.", 1);
		
		IFolder folder = theProject.getFolder(folderName);
		
		if(!folder.exists()) {
			folder.create(false, true, sub.newChild(1));
		}
		
		return folder;
	}
	
	private void addClassPathEntry(IClasspathEntry entry, IProgressMonitor monitor) throws JavaModelException {
		SubMonitor sub = SubMonitor.convert(monitor, 1);
		sub.beginTask("Adding class path entry.", 1);
		
		IClasspathEntry[] oldEntries= theJavaProject.getRawClasspath();
		IClasspathEntry[] newEntries= new IClasspathEntry[oldEntries.length + 1];
		System.arraycopy(oldEntries, 0, newEntries, 0, oldEntries.length);	
		newEntries[oldEntries.length] = entry;
		
		theJavaProject.setRawClasspath(newEntries, sub.newChild(1));
	}

	private String readFileContent(File file) {			
		return new TextFile(file).asString();
	}
}
