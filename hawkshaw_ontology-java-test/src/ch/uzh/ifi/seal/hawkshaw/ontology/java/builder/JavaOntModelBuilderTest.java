package ch.uzh.ifi.seal.hawkshaw.ontology.java.builder;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ontology.java.test
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */



import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.OperationCanceledException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import ch.uzh.ifi.seal.hawkshaw.ontology.java.JavaOntologyCore;
import ch.uzh.ifi.seal.hawkshaw.ontology.java.builder.model.JavaOntModel;
import ch.uzh.ifi.seal.hawkshaw.ontology.java.owl.vocabulary.IHawkshawJavaOntology;
import ch.uzh.ifi.seal.hawkshaw.support.io.ResourceLocator;

import com.hp.hpl.jena.ontology.OntModel;
import com.hp.hpl.jena.rdf.model.Property;
import com.hp.hpl.jena.rdf.model.RDFNode;
import com.hp.hpl.jena.rdf.model.Resource;

public class JavaOntModelBuilderTest {
	private static JavaProjectCreator creator;
	private static JavaOntModel model;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
//		setUpProjectCreator();
//		waitForBuild();
//		setUpModel();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
//		disposeProjectCreator();
//		disposeModel();
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testDummy() {
		// Just a dummy test for Jenkins testing.
		assertTrue(true);
	}
	
	@Test @Ignore
	public void testPackages() {		
		assertThatPackageHasIdentifier(
				"http://evolizer.org/ontologies/seon/projects/CompanyApp#business",
				"business"
		);
		assertThatPackageHasIdentifier(
				"http://evolizer.org/ontologies/seon/projects/CompanyApp#business.api",
				"business.api"
		);
		assertThatPackageHasIdentifier(
				"http://evolizer.org/ontologies/seon/projects/CompanyApp#business.entities",
				"business.entities"
		);
	}
	
	@Test @Ignore
	public void testClasses() {
		assertThatClassHasIdentifier("http://evolizer.org/ontologies/seon/projects/CompanyApp#business.api.AbstractEmployee", "AbstractEmployee");
		assertThatClassHasIdentifier("http://evolizer.org/ontologies/seon/projects/CompanyApp#business.entities.Assistant", "Assistant");
		assertThatClassHasIdentifier("http://evolizer.org/ontologies/seon/projects/CompanyApp#business.entities.Manager", "Manager");
		assertThatClassHasIdentifier("http://evolizer.org/ontologies/seon/projects/CompanyApp#business.Company", "Company");
		fail("Not yet implemented");
	}
	
	@Test @Ignore
	public void testInterfaces() {
		assertThatInterfaceHasIdentifier("http://evolizer.org/ontologies/seon/projects/CompanyApp#business.api.IResponsible", "IResponsible");
		fail("Not yet implemented");
	}
	
	@Test @Ignore
	public void testInheritance() {
		fail("Not yet implemented");
	}
	
	@Test @Ignore
	public void testFields() {
		assertThatFieldHasIdentifier("http://evolizer.org/ontologies/seon/projects/CompanyApp#business.api.AbstractEmployee_name", "name");
		assertThatFieldHasIdentifier("http://evolizer.org/ontologies/seon/projects/CompanyApp#business.api.AbstractEmployee_baseSalary", "baseSalary");
		
		assertThatFieldHasIdentifier("http://evolizer.org/ontologies/seon/projects/CompanyApp#business.entities.Manager_bonus", "bonus");
		assertThatFieldHasIdentifier("http://evolizer.org/ontologies/seon/projects/CompanyApp#business.entities.Manager_personalAssistant", "personalAssistant");
		
		fail("Not yet implemented");
	}

	@Test @Ignore
	public void testMethods() {
		assertThatMethodHasIdentifier("http://evolizer.org/ontologies/seon/projects/CompanyApp#business.api.AbstractEmployee_getBaseSalary()", "getBaseSalary");
		assertThatMethodHasIdentifier("http://evolizer.org/ontologies/seon/projects/CompanyApp#business.api.AbstractEmployee_getName()", "getName");
		assertThatMethodHasIdentifier("http://evolizer.org/ontologies/seon/projects/CompanyApp#business.api.AbstractEmployee_getSalary()", "getSalary");
		
		assertThatMethodHasIdentifier("http://evolizer.org/ontologies/seon/projects/CompanyApp#business.api.IResponsible_charge()", "charge");
		
		assertThatMethodHasIdentifier("http://evolizer.org/ontologies/seon/projects/CompanyApp#business.entities.Manager_setAssistant(business.entities.Assistant)", "setAssistant");
		assertThatMethodHasIdentifier("http://evolizer.org/ontologies/seon/projects/CompanyApp#business.entities.Manager_getAssistant()", "getAssistant");
		assertThatMethodHasIdentifier("http://evolizer.org/ontologies/seon/projects/CompanyApp#business.entities.Manager_getSalary()", "getSalary");
		assertThatMethodHasIdentifier("http://evolizer.org/ontologies/seon/projects/CompanyApp#business.entities.Manager_manage()", "manage");
		assertThatMethodHasIdentifier("http://evolizer.org/ontologies/seon/projects/CompanyApp#business.entities.Manager_charge()", "charge");
		assertThatMethodHasIdentifier("http://evolizer.org/ontologies/seon/projects/CompanyApp#business.entities.Manager_getCEO()", "getCEO");
		assertThatMethodHasIdentifier("http://evolizer.org/ontologies/seon/projects/CompanyApp#business.entities.Manager_getCEO2()", "getCEO2");

		assertThatMethodHasIdentifier("http://evolizer.org/ontologies/seon/projects/CompanyApp#business.entities.Assistant_getSalary()", "getSalary");
		assertThatMethodHasIdentifier("http://evolizer.org/ontologies/seon/projects/CompanyApp#business.entities.Assistant_work()", "work");

		fail("Not yet implemented");
	}
	
	@Test @Ignore
	public void testConstructors() {
		fail("Not yet implemented");
	}
	
	@Test @Ignore
	public void testMethodInvocations() {
		fail("Not yet implemented");
	}
	
	@Test @Ignore
	public void testFieldAccesses() {
		fail("Not yet implemented");
	}
	
	private static void setUpProjectCreator() throws IOException, CoreException {
		String path = ResourceLocator.resolveAbsolutePath(JavaOntologyCore.getDefault(), "test_data/java_projects/CompanyApp");
		
		creator = new JavaProjectCreator(new File(path));
		creator.createJavaProject(null);
		creator.addNatureToProject(JavaOntModelNature.NATURE_ID, null);
	}

	private static void setUpModel() {
		model = JavaOntologyCore.fetchModelFor(creator.getJavaProject());
	}

	
	private static void waitForBuild() throws OperationCanceledException, CoreException {
		creator.buildProject(null);
		creator.waitForBuild();	// blocks until build has been completed.
	}

	private static void disposeProjectCreator() throws CoreException {
		creator.deleteProject(null);
		creator = null;
	}

	private void assertThatPackageHasIdentifier(String packageURI, String identifier) {
		assertThatResourceHasIdentifier(packageURI, identifier, IHawkshawJavaOntology.PACKAGE);
	}
	
	private void assertThatClassHasIdentifier(String classURI, String identifier) {
		assertThatResourceHasIdentifier(classURI, identifier, IHawkshawJavaOntology.CLASS);
	}
	
	private void assertThatInterfaceHasIdentifier(String interfaceURI, String identifier) {
		assertThatResourceHasIdentifier(interfaceURI, identifier, IHawkshawJavaOntology.INTERFACE);
	}

	private void assertThatFieldHasIdentifier(String fieldURI, String identifier) {
		assertThatResourceHasIdentifier(fieldURI, identifier, IHawkshawJavaOntology.FIELD);
	}

	private void assertThatMethodHasIdentifier(String methodURI, String identifier) {
		try {
			String base = methodURI.substring(0, methodURI.indexOf('#') + 1);
			String segment = methodURI.substring(methodURI.indexOf('#') + 1);
			String encodedMethodURI = base + URLEncoder.encode(segment, "UTF-8");
			
			assertThatResourceHasIdentifier(encodedMethodURI, identifier, IHawkshawJavaOntology.METHOD);
		} catch (UnsupportedEncodingException e) {
			fail(e.getMessage());
		}
	}

	private void assertThatResourceHasIdentifier(String uri, String identifier, String type) {
//		OntModel ontModel = model.getJenaOntModel();
//		Resource theResource = ResourceFactory.createResource(uri);
//		
//		assertThatModelContainsStatement(ontModel, theResource, RDF.type, type);
//		assertThatModelContainsStatement(ontModel, theResource, IJavaModelEntities.HAS_IDENTIFIER,  ResourceFactory.createTypedLiteral(identifier));
	}

	private void assertThatModelContainsStatement(OntModel ontModel, Resource subject, Property predicate, RDFNode object) {
		assertThat(ontModel.contains(subject, predicate, object), is(true));
	}
}
