package business.entities;
import business.api.AbstractEmployee;


public class Assistant extends AbstractEmployee {

	public Assistant(String name, double baseSalary) {
		super(name, baseSalary);
	}

	@Override
	public double getSalary() {
		return getBaseSalary();
	}

	public void work() {
		System.out.println(getName() + " mourns: *mimimi*");		
	}

}
