package ch.uzh.ifi.seal.hawkshaw.core.grammar.symbols;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.core.test
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

import java.util.Iterator;
import java.util.Set;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import ch.uzh.ifi.seal.hawkshaw.core.grammar.ChainingRestriction;
import ch.uzh.ifi.seal.hawkshaw.core.grammar.Grammar;
import ch.uzh.ifi.seal.hawkshaw.core.query.Proposal;

public class TestTerminalSymbol {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testAsProposal() {
		TestSymbol symbol = new TestSymbol("word", null);
		Set<Proposal> proposals = symbol.asProposal(ChainingRestriction.EMPTY, ChainingRestriction.EMPTY);
		
		Iterator<Proposal> iter = proposals.iterator();
		assertThat(iter.next().getLabel(), is("word"));
	}

	class TestSymbol extends TerminalSymbol {

		public TestSymbol(String label, Grammar grammar) {
			super(label, grammar);
		}

		@Override
		public Set<Proposal> asProposal(ChainingRestriction restrictionForNouns, ChainingRestriction restrictionForVerbs) {
			return super.asProposal(restrictionForNouns, restrictionForVerbs);
		}		
	}
}
