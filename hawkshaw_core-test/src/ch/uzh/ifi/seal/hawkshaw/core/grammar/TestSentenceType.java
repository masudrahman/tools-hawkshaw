package ch.uzh.ifi.seal.hawkshaw.core.grammar;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.core.test
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import java.util.EnumSet;

import org.junit.Test;

import ch.uzh.ifi.seal.hawkshaw.core.grammar.SentenceType;

public class TestSentenceType {

	/**
	 * Tests whether the lookup of the sentence type works as expected.
	 */
	@Test
	public void testLookUp() {
		assertThat("Did you add/remove sentence types without adapting the tests?", EnumSet.allOf(SentenceType.class).size(), is(8));
		assertThat(SentenceType.lookup("<SPO_QOBJ>"), is(equalTo(SentenceType.OPEN_QUESTION)));
		assertThat(SentenceType.lookup("<SPO_QCLOSED>"), is(equalTo(SentenceType.CLOSED_QUESTION)));
		assertThat(SentenceType.lookup("<SPO_QENUM>"), is(equalTo(SentenceType.ENUM_QUESTION)));
		assertThat(SentenceType.lookup("<SPO_QTEMP>"), is(equalTo(SentenceType.TEMP_QUESTION)));
		assertThat(SentenceType.lookup("<SPO_QSPAT>"), is(equalTo(SentenceType.SPAT_QUESTION)));
		assertThat(SentenceType.lookup("<SPO_DECL>"), is(equalTo(SentenceType.DECLARATION)));
		assertThat(SentenceType.lookup("<SPO_IMPER>"), is(equalTo(SentenceType.IMPERATIVE)));
		assertThat(SentenceType.lookup("<UNDEF>"), is(equalTo(SentenceType.UNDEF)));
		assertThat(SentenceType.lookup("<GREMMLING>"), is(equalTo(SentenceType.UNDEF)));
	}	
}
