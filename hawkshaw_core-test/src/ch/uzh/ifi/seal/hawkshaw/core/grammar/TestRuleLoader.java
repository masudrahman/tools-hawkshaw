package ch.uzh.ifi.seal.hawkshaw.core.grammar;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.core.test
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import ch.uzh.ifi.seal.hawkshaw.core.QueryCore;
import ch.uzh.ifi.seal.hawkshaw.support.io.ResourceLocator;

public class TestRuleLoader extends Grammar {
	private List<Rule> ruleList;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		ruleList = new ArrayList<>();
	}

	@After
	public void tearDown() throws Exception {
		ruleList.clear();
		ruleList = null;
	}
	
	/**
	 * Tests whether the rules are parsed correctly. The following rules are read from a test file:
	 * <br>
	 * <br>
	 * <code>
	 * SPARQL &lt;START&gt; ::= <SPO_QOBJ> ?<br>
	 * &lt;SPO_QOBJ&gt; ::= w1  w2a <non-terminal><br>
	 * &lt;SPO_QOBJ&gt; ::= w1  w2b <non-terminal><br>
	 * &lt;SPO_QOBJ&gt; ::= w1c w2c <non-terminal> w4c<br>
	 * <br>
	 * &lt;non-terminal&gt; ::= w3 w4d<br>
	 * &lt;non-terminal&gt; ::= w3<br>
	 * &lt;non-terminal&gt; ::= w3f<br>
	 * ::= this rule has an invalid syntax and should produce a warning ::=
	 * </code>
	 * 
	 * 
	 * @throws IOException
	 */
	@Test
	public void testLoadGrammarDescriptionFile() throws IOException {
		String grammarLocation = ResourceLocator.resolveAbsolutePath(QueryCore.getDefault(), "test_data/grammar/sparql/basic/basic_test.hgf");
		new RuleLoader(this).loadGrammarDescriptionFile(grammarLocation);
		
		assertThat(ruleList.size(), is(not(0)));
		
		checkRule(0, /* rule number */
				  "<START>", /* label */
				  Token.asTokens("<SPO_QOBJ>", "?"), /* tokens */
				  "SPARQL", /* query language */
				  SentenceType.UNDEF, /* sentence type */
				  new String[]{"", "", ""} /* query parts */
		);
		checkRule(1,
				 "<SPO_QOBJ>",
				 Token.asTokens("w1", "w2a", "<non-terminal>"),
				 null,
				 SentenceType.OPEN_QUESTION,
				 new String[]{"", "", ""}
		);
		checkRule(2,
				  "<SPO_QOBJ>",
				  Token.asTokens("w1", "w2b", "<non-terminal>"),
				  null,
				  SentenceType.OPEN_QUESTION,
				  new String[]{"", "", ""}
		);
		checkRule(3,
				  "<SPO_QOBJ>",
				  Token.asTokens("w1c", "w2c", "<non-terminal>", "w4c"),
				  null,
				  SentenceType.OPEN_QUESTION,
				  new String[]{"", "", ""}
		);
		checkRule(4,
				  "<non-terminal>",
				  Token.asTokens("w3", "w4d"),
				  null,
				  SentenceType.UNDEF,
				  new String[]{"", "", ""}
		);
		checkRule(5,
				  "<non-terminal>",
				  Token.asTokens("w3"),
				  null,
				  SentenceType.UNDEF,
				  new String[]{"", "", ""}
		);
		checkRule(6,
				  "<non-terminal>",
				  Token.asTokens("w3f"),
				  null,
				  SentenceType.UNDEF,
				  new String[]{"", "", ""}
		);
	}

	/**
	 * If an invalid path for a grammar file or directory is specified, a
	 * {@link FileNotFoundException} is thrown and then wrapped in an
	 * {@link RuntimeException}.
	 */
	@Test(expected=RuntimeException.class)
	public void testFileNotFoundException() {
		Grammar grammar = new Grammar();
		
		assertFalse(grammar.hasRule("<START>"));
		new RuleLoader(grammar).loadGrammarDescriptionFile("foobi.egf");
	}
	
	/**
	 * Compares the contents of the rule at the specified position in the
	 * {@link TestRuleLoader#ruleList} with the given parameters.
	 * 
	 * @param ruleNr
	 *            position in {@link TestRuleLoader#ruleList}
	 * @param label
	 *            the expected rule label
	 * @param tokens
	 *            the expected tokens
	 * @param language
	 *            the expected query language
	 * @param sentenceType
	 *            the expected sentence type
	 * @param statements
	 *            the expected query statements
	 */
	private void checkRule(int ruleNr, String label, Queue<Token> tokens, String language, SentenceType sentenceType, String[] statements) {
		Rule rule = ruleList.get(ruleNr);
		assertThat(rule.ruleLabel, is(equalTo(label)));
		assertThat(rule.tokens, is(equalTo(tokens)));
		assertThat(rule.queryStatement, is(equalTo(new QueryStatement(language, sentenceType, statements))));
	}

	/**
	 * Grabs a rule and stores it in {@link TestRuleLoader#ruleList}.
	 */
	@Override
	public void addRule(String ruleLabel, Queue<Token> tokens, QueryStatement queryStatement) {
		Rule nextRule = new Rule();
		nextRule.ruleLabel = ruleLabel;
		nextRule.tokens = tokens;
		nextRule.queryStatement = queryStatement;
		
		ruleList.add(nextRule);		
	}
	
	/**
	 * Helper class for storing rule contents.
	 */
	class Rule {
		String ruleLabel;
		Queue<Token> tokens;
		QueryStatement queryStatement;
	}
}
