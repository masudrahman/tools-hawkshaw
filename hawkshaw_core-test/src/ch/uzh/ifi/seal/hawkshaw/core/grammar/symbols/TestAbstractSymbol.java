package ch.uzh.ifi.seal.hawkshaw.core.grammar.symbols;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.core.test
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.util.Collections;
import java.util.Set;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import ch.uzh.ifi.seal.hawkshaw.core.grammar.ChainingRestriction;
import ch.uzh.ifi.seal.hawkshaw.core.grammar.Grammar;
import ch.uzh.ifi.seal.hawkshaw.core.grammar.QueryStatement;
import ch.uzh.ifi.seal.hawkshaw.core.grammar.SentenceType;
import ch.uzh.ifi.seal.hawkshaw.core.grammar.Token;
import ch.uzh.ifi.seal.hawkshaw.core.query.Proposal;

public class TestAbstractSymbol {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testAddNextSymbolFrom() {
		Grammar grammar = new Grammar();
		TestSymbol rule = new TestSymbol("<START>", grammar);
		rule.addNextSymbolFrom(Token.asTokens("w1", "[w2]", "w3"),
				new QueryStatement("LANGUAGE", SentenceType.UNDEF, "part1",
						"part2", "part3"));

		assertThat("Unexpected label.", rule.getLabel(), is(equalTo("<START>")));
		assertFalse("The symbol shouldn't be marked as the last one in a rule.", rule.isLastSymbolOfRule());
		
		SymbolContainer container = rule.getSymbolContainer();
		assertTrue("The rule does not contain the expected symbol", container.containsSymbol("w1"));
		
		AbstractSymbol firstSymbol = container.fetchOrCreateSymbol("w1", grammar);
		assertFalse("The symbol shouldn't be marked as the last one in a rule.", firstSymbol.isLastSymbolOfRule());
		
		
		
		fail("Finish me!");
	}

	class TestSymbol extends AbstractSymbol {
		public TestSymbol(String label, Grammar grammar) {
			super(label, grammar);
		}

		@Override
		public Set<Proposal> asProposal(ChainingRestriction restrictionForNouns, ChainingRestriction restrictionForVerbs) {
			return Collections.emptySet();
		}
		
		@Override
		public SymbolContainer getSymbolContainer() {
			return super.getSymbolContainer();
		}
	}
}
