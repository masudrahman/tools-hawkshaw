package ch.uzh.ifi.seal.hawkshaw.core.grammar;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.core.test
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertThat;
import static org.junit.matchers.JUnitMatchers.hasItem;

import java.util.HashSet;
import java.util.Set;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import ch.uzh.ifi.seal.hawkshaw.core.grammar.Grammar;
import ch.uzh.ifi.seal.hawkshaw.core.grammar.QueryStatement;
import ch.uzh.ifi.seal.hawkshaw.core.grammar.SentenceType;
import ch.uzh.ifi.seal.hawkshaw.core.grammar.Token;
import ch.uzh.ifi.seal.hawkshaw.core.grammar.symbols.AbstractSymbol;
import ch.uzh.ifi.seal.hawkshaw.core.query.Proposal;

public class TestGrammar {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	/**
	 * Tests whether adding a rule to a {@link Grammar} instance works. A rule
	 * is formed by a label (e.g., &lt;START&gt;) and list of {@link Token}s.
	 * The rule was added successfully if {@link Grammar#hasRule(String)}
	 * returns true.
	 */
	@Test
	public void testAddRuleAndHasRule() {
		Grammar grammar = new Grammar();
		
		grammar.addRule("<START>", Token.asTokens("<SPO_QOBJ>", "?"), new QueryStatement("SPARQL", SentenceType.UNDEF, ""));
		grammar.addRule("<START>", Token.asTokens("<SPO_QENUM>", "?"), new QueryStatement("SPARQL", SentenceType.UNDEF, ""));
		grammar.addRule("<SPO_QOBJ>", Token.asTokens("Where", "is", "<person>"), new QueryStatement("SPARQL", SentenceType.SPAT_QUESTION, ""));
		grammar.addRule("<SPO_QENUM>", Token.asTokens("How", "old", "is", "<person>"), new QueryStatement("SPARQL", SentenceType.ENUM_QUESTION, ""));
		grammar.addRule("<person>", Token.asTokens("michael"), new QueryStatement("SPARQL", SentenceType.UNDEF, ""));
		
		assertThat("Adding the rule to the grammar failed.", grammar.hasRule("<START>"), is(true));
		assertThat("Adding the rule to the grammar failed.", grammar.hasRule("<SPO_QOBJ>"), is(true));
		assertThat("Adding the rule to the grammar failed.", grammar.hasRule("<SPO_QENUM>"), is(true));
		assertThat("Adding the rule to the grammar failed.", grammar.hasRule("<person>"), is(true));
		assertThat("Found a rule that should not be there. Looks like the tests are screwed-up.", grammar.hasRule("<FOO_BAR>"), is(false));
	}
	
	/**
	 * Tests whether retrieving a rule from {@link Grammar} works. This is the
	 * case if the returned rule is not null, has the right label, and is stored
	 * in the grammar afterwards.
	 */
	@Test
	public void testGetRuleFor() {
		Grammar grammar = new Grammar();
		
		assertThat("Found a rule that should not be there. Looks like the tests are screwed-up.", grammar.hasRule("<FOO_BAR>"), is(false));
		AbstractSymbol rule = grammar.getRuleFor("<FOO_BAR>");
		assertThat(rule, is(not(nullValue())));
		assertThat(rule.getLabel(), is("<FOO_BAR>"));
		assertThat("Rule is missing.", grammar.hasRule("<FOO_BAR>"), is(true));
	}
	
	/**
	 * Tests whether {@link Grammar#startWors()} returns the correct words to start a new question.
	 */
	@Test
	public void testStartWords() {
		Grammar grammar = new Grammar();
		
		grammar.addRule("<START>", Token.asTokens("Hello", "World"), new QueryStatement("SPARQL", SentenceType.UNDEF, ""));
	
		Set<String> startWords = new HashSet<>();
		for(Proposal proposal : grammar.startWords()) {
			startWords.add(proposal.getLabel());
		}
		
		assertThat(startWords, hasItem("Hello"));
	}
}
