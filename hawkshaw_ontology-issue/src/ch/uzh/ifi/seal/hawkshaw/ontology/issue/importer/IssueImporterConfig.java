package ch.uzh.ifi.seal.hawkshaw.ontology.issue.importer;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ontology.issue
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import org.eclipse.core.resources.IProject;

public class IssueImporterConfig {
	// private String trackerType; TODO additional importers
	private IProject project;
	private String trackerUrl;
	private String issueKey;
	
	private int maxErrors;
	private int sleepAfter;
	private long sleepMillis;
	
	public IssueImporterConfig() {
		// no op
	}
	
	public boolean isValid() {
		boolean projectIsValid = project != null && project.exists();
		boolean trackerUrlIsValid = trackerUrl != null && trackerUrl.startsWith("http"); // TODO proper validation
		boolean issueKeyIsValid = issueKey != null && issueKey.length() > 0;
		
		return projectIsValid && trackerUrlIsValid && issueKeyIsValid;
	}

	public IProject getProject() {
		return project;
	}

	public void setProject(IProject project) {
		this.project = project;
	}

	public String getTrackerUrl() {
		return trackerUrl;
	}

	public void setTrackerUrl(String trackerUrl) {
		this.trackerUrl = trackerUrl;
	}

	public String getIssueKey() {
		return issueKey;
	}

	public void setIssueKey(String issueKey) {
		this.issueKey = issueKey;
	}

	public int getMaxErrors() {
		return maxErrors;
	}

	public void setMaxErrors(int maxErrors) {
		this.maxErrors = maxErrors;
	}

	public int getSleepAfter() {
		return sleepAfter;
	}

	public void setSleepAfter(int sleepAfter) {
		this.sleepAfter = sleepAfter;
	}

	public long getSleepMillis() {
		return sleepMillis;
	}

	public void setSleepMillis(long sleepMillis) {
		this.sleepMillis = sleepMillis;
	}
}