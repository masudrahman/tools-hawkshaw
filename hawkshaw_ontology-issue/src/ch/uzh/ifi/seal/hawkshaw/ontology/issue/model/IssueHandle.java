package ch.uzh.ifi.seal.hawkshaw.ontology.issue.model;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ontology.issue
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.net.URI;
import java.util.Calendar;

import ch.uzh.ifi.seal.hawkshaw.ontology.issue.owl.vocabulary.IHawkshawIssueOntology;
import ch.uzh.ifi.seal.hawkshaw.ontology.model.AbstractModelEntityHandle;
import ch.uzh.ifi.seal.hawkshaw.ontology.vocabulary.IHawkshawOntology;

import com.hp.hpl.jena.ontology.Individual;

public class IssueHandle extends AbstractModelEntityHandle<IssueOntModel> {
	public IssueHandle(Individual theEntity, String key, IssueOntModel model) {
		super(theEntity, model);
	}

	public void setKey(String key) {
		addLiteral(IHawkshawIssueOntology.HAS_KEY, key);
	}

	public void setCreationDate(Calendar calendar) {
		addLiteral(IHawkshawIssueOntology.CREATED_ON, calendar);
	}

	public void setDescription(String description) {
		addLiteral(IHawkshawIssueOntology.HAS_DESCRIPTION, description);
	}
	
	public CommentHandle addComment(URI uri, String text) {
		CommentHandle commentHandle = model().getCommentHandle(uri);
		commentHandle.setText(text);
		
		addRelation(IHawkshawIssueOntology.HAS_COMMENT, commentHandle);
		
		return commentHandle;
	}

	public AssigneeHandle addAssignee(String acc, String name) {
		AssigneeHandle assigneeHandle = model().getAssigneeHandle(acc, name);
		addRelation(IHawkshawIssueOntology.HAS_ASSIGNEE, assigneeHandle);
		return assigneeHandle;
	}
	
	public ReporterHandle addReporter(String acc, String name) {
		ReporterHandle reporterHandle = model().getReporterHandle(acc, name);
		addRelation(IHawkshawIssueOntology.IS_REPORTED_BY, reporterHandle);
		return reporterHandle;
	}

	public void setPriority(String priority) {
		Priorities p = Priorities.parse(priority);
		
		if(p != Priorities.UNKNOWN) {
			addRelation(IHawkshawIssueOntology.HAS_PRIORITY, p.toIndividual());
			addLiteral(p.toProperty(), true);
		}
	}

	public void setStatus(String status) {
		Status s = Status.parse(status);
		if(s != Status.UNKNOWN) {
			addRelation(IHawkshawIssueOntology.HAS_STATUS, s.toIndividual());
		}
	}

	public void setResolved() {
		addLiteral(IHawkshawIssueOntology.IS_RESOLVED, true);
	}

	public void setOpen() {
		addLiteral(IHawkshawIssueOntology.IS_OPEN, true);
	}
	
	public void setFixTime(int days) {
		addLiteral(IHawkshawIssueOntology.FIX_TIME, days);
	}

	public void addLink(IssueHandle targetIssueHandle, String linkTypeDesc) {
		addRelation(IssueOntModel.getIssueLinkProperty(linkTypeDesc), targetIssueHandle);
	}

	public void setUrl(String url) {
		addLiteral(IHawkshawOntology.HAS_URL, url);
	}
}
