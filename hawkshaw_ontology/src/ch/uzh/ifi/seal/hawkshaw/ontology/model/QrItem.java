package ch.uzh.ifi.seal.hawkshaw.ontology.model;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ontology
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


public class QrItem {
	private PersistentJenaModel model;
	
	private ResultType type;
	
	private String uri;
	private String readable;
	
	public QrItem(PersistentJenaModel model, String readable, ResultType type) {
		this.model = model;
		this.readable = readable;
		this.type = type;
	}
	
	public PersistentJenaModel getModel() {
		return model;
	}
	
	public String getUri() {
		return uri;
	}
	
	public void setUri(String uri) {
		this.uri = uri;
	}
	
	public ResultType getType() {
		return type;
	}
	
	public String asReadable() {
		return readable;
	}
	
	public boolean isIndividual() {
		return type == ResultType.INDIVIDUAL;
	}
	
	public boolean isClass() {
		return type == ResultType.CLASS;
	}
	
	public boolean isLiteral() {
		return type == ResultType.LITERAL;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		if(uri != null) {
			result = uri.hashCode();
		} else {			
			result = prime * result + ((type == null) ? 0 : type.hashCode());
			result = prime * result + ((readable == null) ? 0 : readable.hashCode());
		}
		
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		QrItem other = (QrItem) obj;
		if(uri != null && other.uri != null) {
			return uri.equals(other.uri);
		}
		if (readable == null) {
			if (other.readable != null) {
				return false;
			}
		} else if (!readable.equals(other.readable)) {
			return false;
		}
		if (type != other.type) {
			return false;
		}
		
		return true;
	}
}
