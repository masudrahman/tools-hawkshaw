package ch.uzh.ifi.seal.hawkshaw.ontology.model;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ontology
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.util.Calendar;

import ch.uzh.ifi.seal.hawkshaw.ontology.exceptions.InvalidResourceException;
import ch.uzh.ifi.seal.hawkshaw.ontology.vocabulary.IRDFS;

import com.hp.hpl.jena.ontology.Individual;

public abstract class AbstractHawkshawModel {
	private PersistentJenaModel baseModel;

	public AbstractHawkshawModel(PersistentJenaModel baseModel) {
		this.baseModel = baseModel;
	}
	
	public abstract void clear();

	public String getBase() {
		return baseModel.getBase();
	}

	public long size() {
		return baseModel.size();
	}
	
	protected Individual fetchIndividual(String uniqueName, String label, String classUri, IPostCreationHook... hooks) throws InvalidResourceException {
		Payload<Individual> payload =  getBaseModel().fetchIndividual(uniqueName, label, classUri);
		
		Individual individual = payload.getPayload();
		if(payload.created()) {
			for(IPostCreationHook hook : hooks) {
				hook.created(individual);
			}
		}
		
		return individual;
	}
	
	protected void addLabelTo(AbstractModelEntityHandle<? extends AbstractHawkshawModel> handle, String label) {
		addLiteralTo(handle, IRDFS.LABEL, label);
	}

	protected void addPropertyBetween(AbstractModelEntityHandle<? extends AbstractHawkshawModel> subjectHandle, String propertyUri, AbstractModelEntityHandle<? extends AbstractHawkshawModel> objectHandle) {
		baseModel.addRelationTo(subjectHandle.asIndividual(), propertyUri, objectHandle.asIndividual());
	}
	
	protected void addPropertyBetween(AbstractModelEntityHandle<? extends AbstractHawkshawModel> subjectHandle, String propertyUri, String individualUri) {
		baseModel.addRelationTo(subjectHandle.asIndividual(), propertyUri, individualUri);
	}

	protected void addLiteralTo(AbstractModelEntityHandle<? extends AbstractHawkshawModel> handle, String propertyUri, int value) {
		baseModel.addLiteralTo(handle.asIndividual(), propertyUri, value);
	}
	
	protected void addLiteralTo(AbstractModelEntityHandle<? extends AbstractHawkshawModel> handle, String propertyUri, boolean value) {
		baseModel.addLiteralTo(handle.asIndividual(), propertyUri, value);
	}

	protected void addLiteralTo(AbstractModelEntityHandle<? extends AbstractHawkshawModel> handle, String propertyUri, String value) {
		baseModel.addLiteralTo(handle.asIndividual(), propertyUri, value);
	}
	
	protected void addLiteralTo(AbstractModelEntityHandle<? extends AbstractHawkshawModel> handle, String propertyUri, Calendar value) {
		baseModel.addLiteralTo(handle.asIndividual(), propertyUri, value);
	}
	
	protected void addLiteralTo(Individual individual, String propertyUri, String value) {
		baseModel.addLiteralTo(individual, propertyUri, value);
	}

	protected PersistentJenaModel getBaseModel() {
		return baseModel;
	}

}