package ch.uzh.ifi.seal.hawkshaw.ontology.misc;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ontology
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import com.hp.hpl.jena.query.Query;
import com.hp.hpl.jena.query.QueryFactory;
import com.hp.hpl.jena.query.QueryParseException;

public class QueryValidator {
	private Query query;
	
	private String queryString;
	private String errorMessage;

	private int errorLine;
	private int errorColumn;
	
	public void setQueryString(String queryString) {
		this.queryString = queryString;
	}
	
	public boolean validates() {
		if(queryString == null) {
			return false;
		}
		
		
		boolean validates;
		
		try {
			query = QueryFactory.create(queryString);
			validates = true;
		} catch(QueryParseException e) {
			validates = false;
			
			errorMessage = e.getMessage();
			errorLine = e.getLine();
			errorColumn = e.getColumn();
		}
		
		return validates;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public int getErrorLine() {
		return errorLine;
	}

	public int getErrorColumn() {
		return errorColumn;
	}

	public String format() {
		return query.toString();
	}
}
