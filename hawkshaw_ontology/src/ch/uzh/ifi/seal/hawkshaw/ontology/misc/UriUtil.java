package ch.uzh.ifi.seal.hawkshaw.ontology.misc;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ontology
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;

import ch.uzh.ifi.seal.hawkshaw.ontology.OntologyCore;
import ch.uzh.ifi.seal.hawkshaw.support.exceptions.HawkshawException;

public final class UriUtil {
	private UriUtil() {
		super(); // private constructor to avoid instantiation.
	}
	
	public static String makeAbsolute(String base, String localName) {
		return base + '#' + localName;
	}

	public static String encode(String string) throws HawkshawException {
			try {
				return URLEncoder.encode(string, "UTF-8");
			} catch (UnsupportedEncodingException ex) {
				// Should never happen.
				throw new HawkshawException(OntologyCore.getDefault(), "UTF-8 encoding is not supported for identifier/URI.", ex); // TODO own exception
		
			}
	}
	public static String decode(String string) throws HawkshawException {
		try {
			return URLDecoder.decode(string, "UTF-8");
		} catch (UnsupportedEncodingException ex) {
			// Should never happen.
			throw new HawkshawException(OntologyCore.getDefault(), "UTF-8 decoding is not supported for identifier/URI.", ex); // TODO own exception
		}
	}
}
