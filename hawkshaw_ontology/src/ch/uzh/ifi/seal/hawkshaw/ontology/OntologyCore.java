package ch.uzh.ifi.seal.hawkshaw.ontology;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ontology
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Plugin;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.jobs.IJobManager;
import org.eclipse.core.runtime.jobs.Job;
import org.osgi.framework.BundleContext;

import ch.uzh.ifi.seal.hawkshaw.ontology.misc.UriUtil;
import ch.uzh.ifi.seal.hawkshaw.ontology.model.HawkshawModelFactory;
import ch.uzh.ifi.seal.hawkshaw.ontology.model.ModelCache;
import ch.uzh.ifi.seal.hawkshaw.ontology.model.PersistentJenaModel;
import ch.uzh.ifi.seal.hawkshaw.support.plugin.IHawkshawPlugin;

/**
 * The activator class controls the plug-in life cycle
 */
public class OntologyCore extends Plugin implements IHawkshawPlugin {
	// The plug-in ID
	public static final String PLUGIN_ID = "ch.uzh.ifi.seal.hawkshaw.ontology";
	public static final String FAMILY_INIT = PLUGIN_ID + ".jobs.init";
	
	public static final String EXTENSION_ID_ALTENTRY = "ch.uzh.ifi.seal.hawkshaw.ontology.altEntry";

	private static final String TBD_STORE = "tdb_store";
	
	// The shared instance
	private static OntologyCore plugin;
	

	private HawkshawModelFactory modelFactory;
	private ModelCache modelCache;

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.runtime.Plugins#start(org.osgi.framework.BundleContext)
	 */
	@Override
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
		
		startModelFactory();
		createModelCache();
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.runtime.Plugin#stop(org.osgi.framework.BundleContext)
	 */
	@Override
	public void stop(BundleContext context) throws Exception {
		disposeModelFactory();
		disposeModelCache();
		
		plugin = null;
		super.stop(context);
	}

	@Override
	public String getID() {
		return PLUGIN_ID;
	}
	
	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static OntologyCore getDefault() {
		return plugin;
	}
	
	public static boolean ready() {
		return getDefault().modelFactory.initComplete();
	}
	
	public static void waitForInit(IProgressMonitor monitor) {
		SubMonitor progress = SubMonitor.convert(monitor, "Waiting for Ontology Core Plug-in...", 100);
		
		IJobManager jobManager = Job.getJobManager();
			
		boolean initHasntFinishedYet = true;
		while(initHasntFinishedYet && !progress.isCanceled()) {
			progress.setWorkRemaining(100);
				
			try {
				jobManager.join(OntologyCore.FAMILY_INIT, progress.newChild(100));
					
				initHasntFinishedYet = false;
			} catch (InterruptedException e) {
				initHasntFinishedYet = true;
			}
		}
	}
	
	public static String getUriForProject(IProject project) {
		return "http://se-on.org/ontologies/hawkshaw/projects/" + UriUtil.encode(project.getName());
	}
	
	public static boolean modelExists(IProject project) {
		return modelExists(getUriForProject(project));
	}
	
	public static boolean modelExists(String modelUri) {
		return getDefault().modelFactory.exists(modelUri);
	}
	
	public static boolean modelClean(IProject project) {
		PersistentJenaModel model = getDefault().modelCache.getModelFor(project);
		
		boolean isClean;
		
		if(model != null) {
			isClean = !model.isDirty();
		} else {
			isClean = modelExists(project); // if model is null but exists it has to be clean
		}
		
		return isClean;
	}
	
	public static PersistentJenaModel createPersistentBaseModel(String modelUri) {
		return getDefault().modelFactory.createPersistentBaseModel(modelUri);
	}
	
	public static PersistentJenaModel fetchPersistentModelFor(IProject project) {
		ModelCache cache = getDefault().modelCache;
		PersistentJenaModel model = getDefault().modelCache.getModelFor(project);
		
		if(model == null) {
			model = createPersistentBaseModel(getUriForProject(project));
			cache.cache(project, model);
		}
		
		return model;
	}
	
	private void startModelFactory() {
		IPath modelStoragePath = getStateLocation().append(TBD_STORE);
		
		modelFactory = new HawkshawModelFactory(modelStoragePath);
		modelFactory.startInit();
	}
	
	private void createModelCache() {
		modelCache = new ModelCache();
	}
	
	private void disposeModelFactory() {
		if(modelFactory != null) {
			modelFactory.dispose();
			modelFactory = null;
		}
	}
	
	private void disposeModelCache() {
		modelCache.clear();
		modelCache = null;
	}
	
	public static void wipeModelStore() {
		getDefault().modelFactory.wipeModelStore();
	}
}
