package ch.uzh.ifi.seal.hawkshaw.ontology.changes.model;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ontology.changes
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import com.hp.hpl.jena.ontology.Individual;

import ch.uzh.ifi.seal.hawkshaw.ontology.java.owl.vocabulary.IHawkshawJavaOntology;
import ch.uzh.ifi.seal.hawkshaw.ontology.model.AbstractHawkshawModel;
import ch.uzh.ifi.seal.hawkshaw.ontology.model.PersistentJenaModel;
import ch.uzh.ifi.seal.hawkshaw.ontology.team.owl.vocabulary.IHawkshawHistory;

public class ChangeOntModel extends AbstractHawkshawModel {

	public ChangeOntModel(PersistentJenaModel baseModel) {
		super(baseModel);
		init();
	}
	
	@Override
	public void clear() {
		getBaseModel().clear();
		init();
	}
	
	public CodeEntityHandle getMethodHandleFor(String uniqueName, String localName) {
		Individual individual = fetchIndividual(uniqueName, localName + "()", IHawkshawJavaOntology.METHOD);
		
		return new CodeEntityHandle(individual, this);
	}
	
	public CodeEntityHandle getTypeHandleFor(String uniqueName, String localName) {
		Individual individual = fetchIndividual(uniqueName, localName, IHawkshawJavaOntology.COMPLEX_TYPE);
		
		return new CodeEntityHandle(individual, this);
	}
	
	public VersionHandle getVersionHandleFor(String uniqueName, String identifier) {
		Individual individual = fetchIndividual(uniqueName, identifier, IHawkshawHistory.VERSION);
		
		return new VersionHandle(individual, this);
	}
	
	private void init() {
		getBaseModel().readOntologies("http://se-on.org/ontologies/hawkshaw/2012/02/history-code.owl",
									  "http://se-on.org/ontologies/hawkshaw/2012/02/history-code-nl.owl");
	}
}
