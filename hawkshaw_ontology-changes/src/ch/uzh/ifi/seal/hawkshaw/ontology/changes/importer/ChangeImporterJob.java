package ch.uzh.ifi.seal.hawkshaw.ontology.changes.importer;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ontology.changes
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IStorage;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.team.core.RepositoryProvider;
import org.eclipse.team.core.history.IFileHistory;
import org.eclipse.team.core.history.IFileHistoryProvider;
import org.eclipse.team.core.history.IFileRevision;
import ch.uzh.ifi.seal.changedistiller.ChangeDistiller;
import ch.uzh.ifi.seal.changedistiller.ChangeDistiller.Language;
import ch.uzh.ifi.seal.changedistiller.distilling.FileDistiller;
import ch.uzh.ifi.seal.changedistiller.model.classifiers.EntityType;
import ch.uzh.ifi.seal.changedistiller.model.entities.SourceCodeChange;
import ch.uzh.ifi.seal.changedistiller.model.entities.SourceCodeEntity;

import ch.uzh.ifi.seal.hawkshaw.ontology.OntologyCore;
import ch.uzh.ifi.seal.hawkshaw.ontology.changes.ChangeOntologyCore;
import ch.uzh.ifi.seal.hawkshaw.ontology.changes.model.ChangeOntModel;
import ch.uzh.ifi.seal.hawkshaw.ontology.changes.model.CodeEntityHandle;
import ch.uzh.ifi.seal.hawkshaw.ontology.changes.model.VersionHandle;
import ch.uzh.ifi.seal.hawkshaw.ontology.misc.UriUtil;
import ch.uzh.ifi.seal.hawkshaw.ontology.model.PersistentJenaModel;
import ch.uzh.ifi.seal.hawkshaw.support.exceptions.HawkshawException;

public class ChangeImporterJob extends Job {
	public static final Object CHANGE_JOB_FAMILY = new Object();
	
	private static final IPath STATE_LOC = ChangeOntologyCore.getDefault().getStateLocation();
	
	private IProject project;
	private ChangeOntModel model;
	
	private IFileHistoryProvider historyProvider;
	
	private int totalCompilationUnits;
	private int worked;
	private int totalPackages;
	private int revisionCounter;
	private int changeCounter;
	
	private long duration;
	
	public ChangeImporterJob(IProject project) {
		super("Analyzing Change History");
		this.project = project;
		
		totalPackages         = 0;
		totalCompilationUnits = 0;
		worked                = 0;
		revisionCounter       = 0;
		changeCounter         = 0;
		
		duration              = 0;
		
		setUser(true);
	}
	
	@Override
	public boolean belongsTo(Object family) {
		return CHANGE_JOB_FAMILY.equals(family);
	}

	@Override
	protected IStatus run(IProgressMonitor monitor) {
		SubMonitor subMonitor = SubMonitor.convert(monitor);
		
		IStatus status;
		
		boolean success = initHistoryProvider();
		if(success) {
			initModel();
			
			long start = System.currentTimeMillis();
			status = processJavaProject(JavaCore.create(project), subMonitor);
			long end   = System.currentTimeMillis();
			
			duration = end - start;
		} else {
			status = new Status(Status.ERROR,
								ChangeOntologyCore.getDefault().getID(),
								"Could not initialize the team provider. " +
								"It seems like project " + project.getName() +
								" is not under source control.");
		}
		
		
		return status;
	}
	
	public String getProjectName() {
		return project.getName();
	}
	
	public int getNumberOfAnalyzedRevisions() {
		return revisionCounter;
	}

	public int getNumberOfAnalyzedChanges() {
		return changeCounter;
	}
	
	public String getDurationString() {
		return String.format("%d min, %d sec", 
							 TimeUnit.MILLISECONDS.toMinutes(duration),
							 TimeUnit.MILLISECONDS.toSeconds(duration) - 
							 TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(duration))
		);
	}
	
	private boolean initHistoryProvider() {
		RepositoryProvider repositoryProvider = RepositoryProvider.getProvider(project);
		
		boolean success;
		if(repositoryProvider != null) {
			historyProvider = repositoryProvider.getFileHistoryProvider();
			success = true;
		} else {
			success = false;
		}
		
		return success;
	}
	
	private IStatus processJavaProject(IJavaProject javaProject, SubMonitor subMonitor) {
		IStatus result = Status.OK_STATUS; // we're optimistic
		
		assessAmountOfWork(javaProject);
		subMonitor.subTask("Performing change analysis of " + javaProject.getElementName() + " (" + totalCompilationUnits + " Java files)");
		
		try {
			IPackageFragment[] packageFragments = javaProject.getPackageFragments();
			
			subMonitor.setWorkRemaining(totalPackages);
			
			for(IPackageFragment packageFragment : packageFragments) {
				if(subMonitor.isCanceled()) {
					result = Status.CANCEL_STATUS;
					break;
				}
				
				if(packageFragment.getKind() == IPackageFragmentRoot.K_SOURCE && packageFragment.containsJavaResources()) {
					processPackage(packageFragment, subMonitor.newChild(1));
				}
			}
		} catch(JavaModelException jme) {
			result = new Status(Status.ERROR,
								ChangeOntologyCore.getDefault().getID(),
								Status.OK /* unused */,
								"Error while processing changes of " + project.getName() + ".",
								jme);
		}
		
		return result;
	}

	private void processPackage(IPackageFragment packageFragment, IProgressMonitor monitor) {
		SubMonitor subMonitor = SubMonitor.convert(monitor);
		subMonitor.subTask("Processing " + packageFragment.getElementName());
		
		try {
			ICompilationUnit[] compilationUnits = packageFragment.getCompilationUnits();
			
			subMonitor.setWorkRemaining(compilationUnits.length);
			
			for (ICompilationUnit compilationUnit : compilationUnits) {
				if(subMonitor.isCanceled()) {
					return;
				}
				
				processCompilationUnit(compilationUnit, subMonitor.newChild(1));
			}
		} catch(JavaModelException jmex) {
			throw new HawkshawException(ChangeOntologyCore.getDefault(), "Error while processing changes of package '" + packageFragment.getElementName() + "'. ", jmex); // TODO Build exception?
		}
	}

	private void processCompilationUnit(ICompilationUnit compilationUnit, IProgressMonitor monitor) {
		initModel();
		
		IResource resource = compilationUnit.getResource();
		String filePath = resource.getProjectRelativePath().toString();
		
		IFileHistory history = historyProvider.getFileHistoryFor(resource, IFileHistoryProvider.NONE, null);
		IFileRevision[] revisions = history.getFileRevisions();		
		
		SubMonitor subMonitor = SubMonitor.convert(monitor, revisions.length);
		
		File prev = null;
		try {
			for(int i = revisions.length - 1; i >= 0; i--) {
				if(subMonitor.isCanceled()) {
					return;
				}
				subMonitor.subTask("Analyzing changes in version " + (revisions.length - i) + '/' + revisions.length + " of " + compilationUnit.getElementName() + " (analyzed " + (worked) + " of " + totalCompilationUnits + " files)");
				
				IFileRevision revision = revisions[i];
				String contentIdentifier = revision.getContentIdentifier();
				VersionHandle version = model.getVersionHandleFor(UriUtil.encode(filePath + '/' + contentIdentifier), contentIdentifier);
				
				File curr = createTmpFile(revision);
				
				diff(version, prev, curr);
				
				delete(prev);
				prev = curr;
				
				revisionCounter++;
				subMonitor.worked(1);
			}
		} finally {
			delete(prev);
		}
		
		worked++;
	}

	private void diff(VersionHandle version, File left, File right) {
		if(left != null && right != null) {
			
			FileDistiller distiller = ChangeDistiller.createFileDistiller(Language.JAVA);
			try {
				distiller.extractClassifiedSourceCodeChanges(left, right);
			} catch(Exception e) {
				System.err.println("Warning: error while change distilling. " + e.getMessage());
			}
				
			List<SourceCodeChange> changes = distiller.getSourceCodeChanges();
			if(changes != null) {
				for(SourceCodeChange change : changes) {
					SourceCodeEntity entity = change.getChangedEntity();
					EntityType etype = entity.getType();
					if(etype.isMethod()) {
						changeMethod(version, entity);
					} else if(etype.isStatement()) {
						SourceCodeEntity parent = change.getParentEntity();
						EntityType ptype = parent.getType();
						if(ptype.isMethod()) {
							changeMethod(version, parent);
						}
					}
				}
				
				changeCounter += changes.size();
			}
		}
	}

	private void changeMethod(VersionHandle version, SourceCodeEntity entity) {
		String uniqueName = entity.getUniqueName();
		int dotpos = uniqueName.lastIndexOf('.');
		int brpos = uniqueName.lastIndexOf('(');
		if(brpos > -1 && dotpos + 1 < brpos) {
			CodeEntityHandle method = model.getMethodHandleFor(UriUtil.encode(uniqueName), uniqueName.substring(dotpos + 1, brpos));
			method.changesIn(version);
		}
	}
	
	private File createTmpFile(IFileRevision revision) {
		File tmp;
		
		try {
			IStorage storage = revision.getStorage(null);
			InputStream in = storage.getContents();
			
			IPath p = STATE_LOC.append("/file" + revision.getContentIdentifier() + ".java");
			tmp = p.toFile();
			
			delete(tmp);
			
			OutputStream out = new FileOutputStream(tmp);
			byte buf[] = new byte[1024];
			int len;
			while((len = in.read(buf)) > 0) {
				out.write(buf, 0, len);
			}
			
			out.close();
			in.close();
		} catch(Exception e) {
			tmp = null;
		}
		
		return tmp;
	}

	private void delete(File file) {
		if(file != null && file.exists()) {
			file.delete();
		}
	}

	private void assessAmountOfWork(IJavaProject javaProject) {
		int packageCounter = 0;
		int compilationUnitCounter = 0;
		
		try {
			for(IPackageFragment packageFragment :javaProject.getPackageFragments()) {
				if(packageFragment.getKind() == IPackageFragmentRoot.K_SOURCE && packageFragment.containsJavaResources()) {
					ICompilationUnit[] units = packageFragment.getCompilationUnits();
					
					packageCounter++;
					compilationUnitCounter += units.length;
				}
			}
		} catch(JavaModelException jme) {
			throw new HawkshawException(ChangeOntologyCore.getDefault(), "Error while assessing the amount of work.", jme);
		}
		
		totalPackages = packageCounter;
		totalCompilationUnits = compilationUnitCounter;
	}
	
	private void initModel() {
		OntologyCore.waitForInit(null);
		PersistentJenaModel base = OntologyCore.fetchPersistentModelFor(project);
		model = new ChangeOntModel(base);
	}
}
