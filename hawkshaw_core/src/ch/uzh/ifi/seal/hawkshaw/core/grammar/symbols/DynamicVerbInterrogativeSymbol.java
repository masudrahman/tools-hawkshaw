package ch.uzh.ifi.seal.hawkshaw.core.grammar.symbols;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.core
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.util.HashSet;
import java.util.Queue;
import java.util.Set;

import ch.uzh.ifi.seal.hawkshaw.core.grammar.ChainingRestriction;
import ch.uzh.ifi.seal.hawkshaw.core.grammar.Grammar;
import ch.uzh.ifi.seal.hawkshaw.core.grammar.QueryStatement;
import ch.uzh.ifi.seal.hawkshaw.core.grammar.SentenceType;
import ch.uzh.ifi.seal.hawkshaw.core.grammar.Token;
import ch.uzh.ifi.seal.hawkshaw.core.query.Proposal;

public class DynamicVerbInterrogativeSymbol extends AbstractSymbol {

	public DynamicVerbInterrogativeSymbol(String label, Grammar grammar) {
		super(label, grammar);
	}

	@Override
	protected Set<Proposal> asProposal(ChainingRestriction restrictionForNouns, ChainingRestriction restrictionForVerbs) {
		Set<Proposal> result = new HashSet<>();
		
		for(String propUri:  model().getDatatypePropertiesThatMatch(restrictionForVerbs)) {
			for(String interrogativePhrase : model().getInterrogativesFor(propUri)) {
				Queue<Token> tokens = Token.tokenize(interrogativePhrase);
				
				AbstractSymbol symbol = new TerminalSymbol(tokens.remove().getValue(), getGrammar());
				// Data type properties are used like nouns - range and domain are interchanged.
				symbol.addDomain(model().rangeOf(propUri)); // TODO Static imports
				symbol.addRange(model().domainOf(propUri));
				symbol.flagAsVerb();
				
				QueryStatement q = new QueryStatement(null, SentenceType.UNDEF, '?' + propUri.substring(propUri.lastIndexOf('#') + 1).toLowerCase(), "<" + propUri + ">");
				q.setRule(getGrammar().getRuleFor(getLabel()));
				
				symbol.addNextSymbolFrom(tokens, q);
				
				Proposal proposal = new Proposal(symbol);
				proposal.addUpperSymbol(this);
				result.add(proposal);
			}		
		}
				
		return result;
	}
}
