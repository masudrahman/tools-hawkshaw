package ch.uzh.ifi.seal.hawkshaw.core.query;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.core
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import ch.uzh.ifi.seal.hawkshaw.core.exceptions.QuestionIncompleteException;
import ch.uzh.ifi.seal.hawkshaw.core.exceptions.UnexpectedWordException;
import ch.uzh.ifi.seal.hawkshaw.core.grammar.Grammar;
import ch.uzh.ifi.seal.hawkshaw.core.grammar.QueryStatement;

/**
 * A question object provides the behavior for composing queries. One can ask
 * for proposals (i.e., words that are valid for being added) and add words.
 * 
 * This class is partially based on code from the Ginseng project. See
 * http://www.ifi.uzh.ch/ddis for more information.
 * 
 * @author wuersch
 * 
 */
public class Question {
	private int length;
	private Grammar grammar;
	private Set<Path> activePaths;
	private Map<Integer, Set<Path>> resolvedPaths;
		
	public Question(Grammar grammar) {
		length = 0;
		this.grammar = grammar;
		initActivePaths();
		initResolvedPaths();
	}

	public Set<String> nextWords() {
		Set<String> result = new TreeSet<>();
		
		for(Path activePath : activePaths) {
			result.addAll(activePath.nextWords());	
		}
		
		return result;
	}
	
	@Override
	public String toString() {
		Path firstPath = activePaths.iterator().next();
		return firstPath.phrase();
	}
	
	/**
	 * 
	 * @param word
	 * @return returns simply an instance of the current question to allow
	 *         chained calls to this method.
	 * @throws UnexpectedWordException
	 *             if <code>word</code> can not be appended to the current
	 *             question.
	 */
	public Question add(String word) throws UnexpectedWordException {
		Set<Path> branches = new HashSet<>();
		Set<Path> resolved = new HashSet<>();
		
		for(Path activePath : activePaths) {
			Set<Path> localBranches = activePath.add(word);
						
			if(localBranches == null) {
				resolved.add(activePath);
			} else {
				for(Path branch : localBranches) {
					branch.setCreatedAt(getWordCount());
				}
				
				branches.addAll(localBranches);
			}
		}
		
		if(!resolved.equals(activePaths)) {
			activate(branches);
			suspend(resolved);
			incrementWordCount();
		} else {
			throw new UnexpectedWordException("Word can not be added to the current question.", word, nextWords());
		}
		
		return this;
	}

	/**
	 * Removes the last word from the current question.
	 */
	public void removeLastWord() {
		/*
		 * the order of the following statements is irrelevant, except that
		 * removing duplicates first will not waste time for their truncation.
		 */
		removeDuplicatedPaths();
		removeLastElementsFromActivePaths();
		restorePaths();
		
		decrementWordCount();
	}
	
	/**
	 * Asks the current question, i.e., compiles it into one or several query
	 * statements, which are then executed against the model maintained by the
	 * context. The results are wrapped in an {@link Answer} object.
	 * 
	 * @return an answer containing the results of asking the current question.
	 * @throws QuestionIncompleteException
	 */
	public Answer ask() throws QuestionIncompleteException {
		if(!isComplete()) {
			throw new QuestionIncompleteException("Error. You can not ask an incomplete question.");
		}
		
		Answer globalAnswer = new Answer();
		globalAnswer.setQuestionPhrase(toString());
		
		// Compilation of query statements
		long compilationStart = System.currentTimeMillis();
		
		Set<QueryStatement> queryStatements = compileQueryStatements();
		
		long compilationEnd = System.currentTimeMillis();
		globalAnswer.setCompilationTime(compilationEnd - compilationStart);
		
		// Execution of sub-queries
		long queryExecutionStart = System.currentTimeMillis();
		
		for(QueryStatement queryStatement : queryStatements) {
			Answer localAnswer = execute(queryStatement); // TODO collect exceptions and create MultiStatus ?
			globalAnswer.include(localAnswer);
		}
		
		long queryExecutionEnd = System.currentTimeMillis();
		globalAnswer.setQueryExecutionTime(queryExecutionEnd - queryExecutionStart);
		
		return globalAnswer;
	}
	
	protected Answer execute(QueryStatement queryStatement) {
		return grammar.getModel().execute(queryStatement);
	}

	/**
	 * Returns a set of formal query statements, compiled from the current
	 * question. Useful for testing purpose or if one wants to bypass the usual
	 * query execution mechanism (see {@link #ask()}).
	 * 
	 * Note that this method only succeeds if the question is complete. A set of
	 * incomplete formal query statements can be retrieved during question
	 * composition by calling {@link #internalCompile()} or via the
	 * {@link QuestionStatus} object returned by {@link #getStatus()} method.
	 * 
	 * @return a set of formal query statements
	 * @throws QuestionIncompleteException
	 *         if the question is not yet complete.
	 */
	public Set<String> compile() throws QuestionIncompleteException {
		if(!isComplete()) {
			throw new QuestionIncompleteException("Error. Can not compile incomplete question.");
		}
		
		Set<String> compiledQueries = new HashSet<>();
		
		for(QueryStatement queryStatement : compileQueryStatements()) {
			compiledQueries.add(queryStatement.toString());
		}
	
		return compiledQueries;
	}
	
	public boolean ready() {
		return grammar != null && grammar.ready();
	}

	/**
	 * If one or more active paths are complete then the question is complete.
	 * 
	 * @return <code>true</code> if one or more paths and therefore the question
	 *         is complete, <code>false</code> otherwise.
	 */
	public boolean isComplete() {
		for(Path path : activePaths) {
			if(path.isComplete()) {
				return true;
			}
		}
	
		return false;
	}
	
	// TODO refactor? better solution? See constructor.
	public void clear() {
		initActivePaths();
		initResolvedPaths();
		length = 0;
	}

	public QuestionStatus getStatus() {
		return new QuestionStatus(this);
	}

	protected int getWordCount() {
		return length;
	}

	protected int getNrOfActivePaths() {
		return activePaths.size();
	}

	protected int getNrOfResolvedPaths() {
		int result = 0;
		
		for(Set<Path> resolvedSet : resolvedPaths.values()) {
			result += resolvedSet.size();
		}
		
		return result;
	}

	private void initActivePaths() {
		activePaths = new HashSet<>();
		activePaths.add(new Path(grammar));
	}

	private void initResolvedPaths() {
		resolvedPaths = new HashMap<>();
	}

	private void activate(Set<Path> paths) {
		if(paths != null && paths.size() > 0) {
			activePaths.addAll(paths);
		}
	}

	private void suspend(Set<Path> paths) {
		if(paths != null && paths.size() > 0) {
			activePaths.removeAll(paths);
			resolvedPaths.put(getWordCount(), paths);
		}
	}

	private void incrementWordCount() {
		length++;
	}
	
	private void restorePaths() {
		Set<Path> pathsToRestore = resolvedPaths.get(getWordCount() - 1);
		activate(pathsToRestore);
		resolvedPaths.remove(getWordCount() - 1);
	}

	private void removeLastElementsFromActivePaths() {
		for(Path activePath : activePaths) {
			activePath.removeLastElement();
		}
	}

	private void removeDuplicatedPaths() {
		Iterator<Path> activePathIterator = activePaths.iterator();
		while(activePathIterator.hasNext()) {
			Path activePath = activePathIterator.next();
			if(activePath.wasCreatedAt(getWordCount() - 1)) {
				activePathIterator.remove();
			}
		}
	}

	private void decrementWordCount() {
		if(length > 0) {
			length--;
		}
	}
	
	private Set<QueryStatement> compileQueryStatements() {
		Set<QueryStatement> compiledQueryStatements = new HashSet<>();
		
		for(Path activePath : activePaths) {
			compiledQueryStatements.addAll(activePath.compile());
		}
		
		return compiledQueryStatements;
	}
}