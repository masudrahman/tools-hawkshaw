package ch.uzh.ifi.seal.hawkshaw.core.grammar;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.core
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.LinkedList;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


import ch.uzh.ifi.seal.hawkshaw.core.QueryCore;
import ch.uzh.ifi.seal.hawkshaw.core.exceptions.RuleLoaderException;
import ch.uzh.ifi.seal.hawkshaw.support.io.DirectoryListing;
import ch.uzh.ifi.seal.hawkshaw.support.io.ResourceLocator;
import ch.uzh.ifi.seal.hawkshaw.support.logging.Log;
import ch.uzh.ifi.seal.hawkshaw.support.misc.IPredicate;

/**
 * Loads grammar files containing grammar rules and adds them to the
 * {@link Grammar} by invoking {@link Grammar#addRule(String, LinkedList, QueryStatement)}.
 * Usually it is fine to call {@link RuleLoader#loadDefaultGrammar()} to initialize the
 * grammar. The method {@link RuleLoader#loadGrammarDescriptionFile(String)} is mainly intended to
 * add static rules later or for testing purposes.
 * 
 * @author wuersch
 */
public class RuleLoader {
	/**
	 * Default character set encoding for grammar files.
	 */
	private static final String DEFAULT_CHARSET = "UTF-8";
	
	/**
	 * Specifies the syntax of a rule. Example rule:
	 * <code>SPARQL &lt;START&gt; ::= Hello World</code> 
	 */
	private static final Pattern RULE_PATTERN = Pattern.compile("((\\S++)\\s++)?(\\S++)\\s*+::= (.*+)");
	/**
	 * The default folder to look for grammar files, if no other folder was specified.
	 */
	private static final String DEFAULT_GRAMMAR_FOLDER = "grammar";
	/**
	 * The default file ending of grammar files.
	 */
	private static final String DEFAULT_GRAMMAR_SUFFIX = ".egf";
	/**
	 * The grammar to which the loaded rules shall be added.
	 */
	private Grammar grammar;
	
	/**
	 * Constructor.
	 * 
	 * @param grammar
	 *            the grammar to which the loaded rules shall be added.
	 */
	public RuleLoader(Grammar grammar) {
		this.grammar = grammar;
	}
	
	/**
	 * Loads the all the grammar files that are contained in
	 * {@link RuleLoader#DEFAULT_GRAMMAR_FOLDER}.
	 * 
	 * @throws RuleLoaderException
	 *             if an {@link IOException} occurs while reading the default grammar
	 *             folder.
	 */
	public void loadRulesFromDefaultLocation() throws RuleLoaderException {
		String grammarLocation = ResourceLocator.resolveAbsolutePath(QueryCore.getDefault(),DEFAULT_GRAMMAR_FOLDER);
		loadGrammarDescriptionFile(grammarLocation);
	}

	/**
	 * Loads grammar files residing at a given path. The path can point either
	 * to a file or a directory. In the later case, all the files in the
	 * directory (or its subdirectories) are loaded.
	 * 
	 * @param path a pathname string.
	 * @throws NullpointerException if path is <code>null</code>.
	 */
	public void loadGrammarDescriptionFile(String path) {
		loadGrammarDescription(new File(path));
	}
	
	public void loadGrammarDescriptionFile(URL url) {
		File file;
		try {
			file = new File(url.toURI());
		} catch (URISyntaxException e) {
			file = new File(url.getPath());
		}

		loadGrammarDescription(file);
	}
	
	private void loadGrammarDescription(File fileOrDir) {
		if(!fileOrDir.exists()) {
			String absolutePath = fileOrDir.getAbsolutePath();
			throw new RuleLoaderException(
										  "Grammar file does not exist. Could not find " + absolutePath + ".",
										  grammar,
										  absolutePath
										  );
		} else if(fileOrDir.isDirectory()) {
			readDirectory(fileOrDir);
		} else {
			readFile(fileOrDir);
		}
	}

	/**
	 * Reads all the files from the given directory that have suffix (file
	 * ending) specified in {@link RuleLoader#DEFAULT_GRAMMAR_SUFFIX}.
	 * 
	 * @param directory
	 *            the directory to read.
	 */
	private void readDirectory(File directory) {
		readDirectory(directory, DEFAULT_GRAMMAR_SUFFIX);
	}
	
	/**
	 * Reads all the files with a given suffix in a given directory (or its
	 * sub-directories).
	 * 
	 * @param directory the directory to read.
	 * @param suffix the file ending (e.g., '.egf').
	 */
	private void readDirectory(File directory, String suffix) {
		for(File grammarFile : listDirectoryContent(directory, suffix)) {
			readFile(grammarFile);
		}
	}

	/**
	 * Walks recursively through a directory and its sub-directories and returns
	 * a {@link DirectoryListing} containing all the files with the given
	 * suffix.
	 * 
	 * @param dir
	 *            the directory to walk through.
	 * @param suffix
	 *            only files having this suffix (e.g., *.egf) will be returned.
	 * @return a {@link DirectoryListing} containing all the files with the
	 *         given suffix, contained within the given directory or its
	 *         sub-directories.
	 */
	private DirectoryListing listDirectoryContent(File dir, final String suffix) {
		return new DirectoryListing(dir, new SuffixFilter(suffix));
	}

	/**
	 * Parses a single file for grammar rules.
	 * 
	 * @param file the file to parse.
	 * @throws RuleLoaderException if file does not exist.
	 */
	private void readFile(File file) throws RuleLoaderException {		
		Scanner scanner = null;
		try {
			scanner  = new Scanner(file, DEFAULT_CHARSET);
			
			while(scanner.hasNextLine()) {
				readLine(scanner.nextLine());
			}
		} catch(FileNotFoundException fnfe) {
			// checked earlier in loadGrammarDescriptionFile(...) - should never happen
			String absolutePath = file.getAbsolutePath();
			throw new RuleLoaderException("Could not load rules from " + absolutePath, grammar, absolutePath, fnfe); // TODO own exception
		} finally {
			if(scanner != null) {
				scanner.close();
			}
		}
		
	}

	/**
	 * Parses a single line and passes the result, i.e., a rule, to
	 * {@link Grammar#addRule(String, LinkedList, QueryStatement)}.
	 * 
	 * Parsing errors are ignored, a warning is logged instead.
	 * 
	 * @param line
	 *            the line to parse.
	 */
	private void readLine(String line) {
		boolean filtered = filterCommentsAndEmptyLines(line);
		
		if(!filtered) {
			Matcher matcher = RULE_PATTERN.matcher(line);
			if(matcher.matches()) {
				String language = matcher.group(2);
				String leftHandSide = matcher.group(3);
				String rightHandSideWithQueryParts = matcher.group(4);
				
				SentenceType sentenceType = SentenceType.lookup(leftHandSide);
				
				Scanner scanner = new Scanner(rightHandSideWithQueryParts + "|||"); // ||| makes sure that the scanner has always (empty) query parts to consume.
				scanner.useDelimiter("\\|");
				String rightHandSide = scanner.next().trim();
				String[] queryParts = extractQueryParts(scanner);
				scanner.close();
				
				grammar.addRule(leftHandSide, Token.tokenize(rightHandSide), new QueryStatement(language, sentenceType, queryParts));
			} // TODO else log something
		}
	}	

	/**
	 * Blanks lines or lines starting with '#' are not parsed. A special case
	 * are lines starting with '###'. They are considered to contain information
	 * that should somehow be presented to the user (e.g., in a log that is
	 * accessible from the UI). This method indicates whether a line should be
	 * filtered (i.e, discarded for further parsing) and, if this is the case but
	 * the line is a user-relevant comment, passes the line to the logger. 
	 * 
	 * @param line
	 *            a line of text, e.g., read from a grammar file.
	 * @return <code>true</code> if the line is filtered, i.e., if it is empty
	 *         or contains a comment. <code>false</code>, otherwise.
	 */
	private boolean filterCommentsAndEmptyLines(String line) {
		boolean isNotRule = false;
		
		if(line.trim().equals("") || (line.charAt(0) == '#')) {
			if (line.startsWith("###")) { // comments that are meant to be logged/shown to the user.
				Log.info(this, "\t\t| # " + line.substring(3, line.length()).trim());
			}
			
			isNotRule = true;
		}
		
		return isNotRule;
	}

	/**
	 * Each rule contains up to three parts of a formal query language statement.
	 * This method extracts these parts from the underlying {@link Scanner} instance.
	 * 
	 * Callers of this method need to take care of closing the scanner after invoking
	 * this method.
	 * 
	 * @param scanner the scanner that parses the rules.
	 * @return an array of query parts.
	 */
	private String[] extractQueryParts(Scanner scanner) {
		String[] queryParts = new String[3];
		for(int i = 0; i < queryParts.length; i++) {
			String token = "";
			if(scanner.hasNext()) {
				token = scanner.next().trim();
				if(token.equals("-")) { // sometimes, certain query parts can be omitted by writing '-' instead.
					token = "";
				}
			}
			queryParts[i] = token;
		}
		return queryParts;
	}

	private static final class SuffixFilter implements IPredicate<File> {
		private final String suffix;
	
		private SuffixFilter(String suffix) {
			this.suffix = suffix;
		}
	
		public boolean evaluate(File f) {
			return f.getName().endsWith(suffix);
		}
	}
}

