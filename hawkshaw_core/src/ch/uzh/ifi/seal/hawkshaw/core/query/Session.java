package ch.uzh.ifi.seal.hawkshaw.core.query;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.core
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.Set;

import ch.uzh.ifi.seal.hawkshaw.core.grammar.Grammar;

/**
 * 
 * @author wuersch
 *
 */
public class Session {
	private Grammar grammar;
	private Question activeQuestion;
	private Answer lastAnswer;
	

	
	public Session(Grammar grammar) {
		this.grammar = grammar;
				
		startQuestion(); // TODO evaluate this
	}
	
	public final void startQuestion() {
		activeQuestion = new Question(grammar);
	}
	
	public Set<String> nextWords() {
		return activeQuestion.nextWords();
	}
	
	public void add(String word) {
		activeQuestion.add(word);
		
//		fireSessionChanged();
	}
	
	public void removeLastWord() {
		activeQuestion.removeLastWord();
		
//		fireSessionChanged();
	}

	public void clearActiveQuestion() {
		activeQuestion.clear();
		
//		fireSessionChanged();
	}

	public Question getActiveQuestion() {
		return activeQuestion;
	}

	public String getActiveQuestionString() {
		return activeQuestion.toString();
	}

	public Answer ask() {
		lastAnswer = activeQuestion.ask();
		return lastAnswer;
	}
	
	public Grammar getGrammar() {
		return grammar;
	}
	
	public Answer getLastAnswer() {
		return lastAnswer;
	}
}
