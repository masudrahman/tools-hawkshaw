package ch.uzh.ifi.seal.hawkshaw.core.grammar.symbols;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.core
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Set;

import ch.uzh.ifi.seal.hawkshaw.core.grammar.ChainingRestriction;
import ch.uzh.ifi.seal.hawkshaw.core.grammar.Grammar;
import ch.uzh.ifi.seal.hawkshaw.core.grammar.PartOfSpeech;
import ch.uzh.ifi.seal.hawkshaw.core.grammar.QueryStatement;
import ch.uzh.ifi.seal.hawkshaw.core.grammar.Token;
import ch.uzh.ifi.seal.hawkshaw.core.ontology.ModelAdapter;
import ch.uzh.ifi.seal.hawkshaw.core.query.Proposal;

/**
 * 
 * @author wuersch
 *
 */
public abstract class AbstractSymbol {
	private String label;
	private PartOfSpeech partOfSpeech;
	private ChainingRestriction restriction;
	
	private Grammar grammar;
	private boolean isLastSymbolOfRule;
	
	private SymbolContainer nextSymbols;
	
	private Set<QueryStatement> queryStatements;
	
	public AbstractSymbol(String label, Grammar grammar) {
		this.label = label;
		this.partOfSpeech = PartOfSpeech.UNDEF;
		restriction = new ChainingRestriction();
		this.grammar = grammar;
		isLastSymbolOfRule = false;
		
		nextSymbols = new SymbolContainer();
		queryStatements = new HashSet<>();
	}

	public String getLabel() {
		return label;
	}
	
	public boolean isNoun() {
		return partOfSpeech == PartOfSpeech.NOUN || partOfSpeech == PartOfSpeech.SUBJECT;
	}
	
	public boolean isSubject() {
		return partOfSpeech == PartOfSpeech.SUBJECT;
	}
	
	public boolean isVerb() {
		return partOfSpeech == PartOfSpeech.VERB;
	}

	public boolean isLastSymbolOfRule() {
		return isLastSymbolOfRule;
	}

	public boolean hasNext() {
		return !nextSymbols.isEmpty();
	}

	// TODO Does this really need to be a set? One query statement per symbol should be enough, shouldn't it? - No, you can have two identical rules with two different query statements (verify).
	public Set<QueryStatement> getQueryStatements() {
		Set<QueryStatement> duplicates = new HashSet<>();
		
		for(QueryStatement queryStatement : queryStatements) {
			duplicates.add(queryStatement.duplicate());
		}
		
		return duplicates;
	}

	/**
	 * This method is method is called by
	 * {@link Grammar#addRule(String, String, QueryStatement)} on an instance of {@code
	 * AbstractSymbol} that is the first one of either a newly created or an existing
	 * rule. The {@code tokens} contains one or more symbols that are added
	 * recursively to this symbol.
	 * 
	 * @param remainingTokens
	 *            the remaining tokens of the rule.
	 * @param queryStatement
	 *            the formal query statement associated with this rule.
	 */
	public void addNextSymbolFrom(Queue<Token> remainingTokens, QueryStatement queryStatement) {
		if(remainingTokens.isEmpty()) {
			addQueryStatement(queryStatement);
			flagAsLastSymbolOfRule();
		} else {
			Token nextToken = remainingTokens.remove();
			if(nextToken.isOptional()) {
				// also add directly the remaining tokens to the current symbol, not only the optional one
				addNextSymbolFrom(new LinkedList<>(remainingTokens), queryStatement);
			}
			
			AbstractSymbol symbol = nextSymbols.fetchOrCreateSymbol(nextToken.getValue(), grammar);
			
			if(nextToken.isSubject()) {
				symbol.flagAsSubject();
			}
			
			symbol.addNextSymbolFrom(remainingTokens, queryStatement);
		}
	}
	
	public Set<Proposal> nextProposals() {
		return nextProposals(ChainingRestriction.EMPTY, ChainingRestriction.EMPTY);
	}

	public Set<Proposal> nextProposals(ChainingRestriction restrictionForNouns, ChainingRestriction restrictionForVerbs) {
		Set<Proposal> result = new HashSet<>();
		
		for(AbstractSymbol nextSymbol : nextSymbols) {
			result.addAll(nextSymbol.asProposal(restrictionForNouns, restrictionForVerbs));
		}
		
		return result;
	}

	/**
	 * For debugging purposes.
	 */
	@Override
	public String toString() {
		return label;
	}

	/**
	 * Template-method. 
	 */
	protected abstract Set<Proposal> asProposal(ChainingRestriction restrictionForNouns, ChainingRestriction restrictionForVerbs);
	
	public ChainingRestriction getChainingRestriction() {
		return restriction;
	}
	
	protected Grammar getGrammar() {
		return grammar;
	}
	
	protected ModelAdapter model() {
		return getGrammar().getModel();
	}

	protected SymbolContainer getSymbolContainer() {
		return nextSymbols;
	}
	
	protected void flagAsNoun() {
		partOfSpeech = PartOfSpeech.NOUN;
	}
	
	protected void flagAsSubject() {
		partOfSpeech = PartOfSpeech.SUBJECT;
	}
	
	protected void flagAsVerb() {
		partOfSpeech = PartOfSpeech.VERB;
	}
	
	private void flagAsLastSymbolOfRule() {
		isLastSymbolOfRule = true;
	}

	private void addQueryStatement(QueryStatement queryStatement) {
		queryStatements.add(queryStatement);
	}

	protected void addDomain(String domain) {
		restriction.addDomain(domain);		
	}

	protected void addDomain(Set<String> domain) {
		restriction.addDomain(domain);		
	}

	protected void addRange(Set<String> range) {
		restriction.addRange(range);		
	}
}
