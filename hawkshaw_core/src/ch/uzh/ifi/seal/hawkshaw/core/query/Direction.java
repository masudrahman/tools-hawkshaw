package ch.uzh.ifi.seal.hawkshaw.core.query;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.core
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */



/**
 * The constants defined by this {@link Enum} are used to describe to which
 * {@link Element} in an existing {@link Path} a {@link Proposal} should be
 * appended.
 * 
 * Proposals are retrieved by performing a recursive depth-first search along an
 * active path. When the search returns, for each step back towards the first
 * element of the path, the direction info is added to the proposal. This
 * information is used once a user has chosen a proposal to reach again the
 * element where the proposal originates from.
 * 
 * @author wuersch
 * 
 * @see Element#nextProposals()
 * @see Element#add(Proposal)
 */
public enum Direction {
	/**
	 * Stop traversal at the current {@link Element}
	 */
	CURRENT,
	/**
	 * Continue traversal at the previous {@link Element}. This happens if a
	 * lower rule is complete while its upper rule still has more elements.
	 * Example:
	 * 
	 * <pre>
	 * w1 - w2 - &lt;nt&gt; - w4
	 *             |
	 *             |- w3a w3b w3c
	 * </pre>
	 * 
	 * Once end of the lower rule, i.e., the element <code>w3c</code> has been
	 * reached, we need to step back twice (to <code>w3b</code> and
	 * <code>w3a</code>) and once upwards (<code>&lt;nt&gt;</code>), to complete
	 * the upper rule <code>w4</code>.
	 */
	PREVIOUS,
	/**
	 * Continue traversal at the next {@link Element}.
	 */
	NEXT,
	/**
	 * Continue traversal at the upper {@link Element}.
	 * 
	 * @see {@link #PREVIOUS}.
	 */
	UPPER,
	/**
	 * Continue traversal at the lower {@link Element}.
	 */
	LOWER
}
