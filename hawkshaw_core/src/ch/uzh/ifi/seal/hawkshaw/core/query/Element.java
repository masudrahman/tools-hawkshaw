package ch.uzh.ifi.seal.hawkshaw.core.query;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.core
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.util.Collections;
import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

import ch.uzh.ifi.seal.hawkshaw.core.QueryCore;
import ch.uzh.ifi.seal.hawkshaw.core.grammar.ChainingRestriction;
import ch.uzh.ifi.seal.hawkshaw.core.grammar.QueryStatement;
import ch.uzh.ifi.seal.hawkshaw.core.grammar.symbols.AbstractSymbol;
import ch.uzh.ifi.seal.hawkshaw.support.exceptions.HawkshawException;

/**
 * This class resembles a word/symbol that was added to a {@link Question} (this
 * also includes non-terminal symbols). A tree of such words is a (question)
 * path, rooted at a {@link Path} object. A path has a tree structure, rather
 * than a simple linked-list one, because the grammar also contains non-terminal
 * symbols. Non-terminal symbols cause the addition of upper and lower elements.
 * 
 * This class is partially based on code from the Ginseng project. See
 * http://www.ifi.uzh.ch/ddis for more information.
 * 
 * @author wuersch
 * 
 */
public class Element {
	/**
	 * The symbol that was added to a question.
	 */
	private AbstractSymbol symbol;
	/**
	 * The previous element.
	 */
	private Element previous;
	/**
	 * The next element.
	 */
	private Element next;
	/**
	 * The upper element. Set whenever a symbol has a non-terminal symbol as parent.
	 */
	private Element upper;
	/**
	 * The lower element. Set if the current symbol is a non-terminal symbol.
	 */
	private Element lower;
	
	/**
	 * Constructor. Called by {@link Path} when the very first word is added.
	 * 
	 * @param proposal
	 */
	public Element(Proposal proposal) {
		// the root is basically discarded, i.e., replaced by the current element.
		Element root = subPathFor(proposal);
		symbol = root.symbol;
		
		if(root.lower != null) {
			// there's no lower symbol, e.g., if <START> rule has only terminal symbols.
			setLowerElement(root.lower);
		}
	}

	/**
	 * Constructor. Called whenever a new word is added to a question, except if
	 * this word is the very first one. In this case {@link #Element(Proposal)}
	 * is called instead.
	 * 
	 * @param symbol
	 */
	public Element(AbstractSymbol symbol) {
		this.symbol = symbol;
	}
	
	/**
	 * Copy constructor. Clones the <code>otherElement</code>, as well as the
	 * whole subtree of elements attached to it. Called whenever a path is cloned.
	 * 
	 * @param otherElement
	 *            the element to copy.
	 */
	private Element(Element otherElement) {
		if(otherElement != null) {
			this.symbol = otherElement.symbol;
			
			if(otherElement.next != null) {
				next = new Element(otherElement.next);
				next.previous = this;
			}
						
			if(otherElement.lower != null) {
				lower = new Element(otherElement.lower);
				lower.upper = this;
			}
		}
	}
	
	/**
	 * For debugging purposes only.
	 */
	@Override
	public String toString() {
		return "Element [" + symbol.toString() + "]"; 
	}

	/**
	 * Creates a copy of the remaining question structure, starting at the
	 * current element.
	 * 
	 * @return a copy of the current element and the subtree of elements
	 *         attached to it.
	 * @see #Element(Element)
	 */
	protected Element duplicate() {
		return new Element(this);
	}

	/**
	 * Recursively searches for valid words that can be attached
	 * to the current subtree of elements. Returns an empty set, if no such
	 * proposals are found.
	 * 
	 * When the recursive search returns, direction information is added to each
	 * proposal for each step (e.g., if search returns from a lower symbol,
	 * {@link Direction#LOWER} is added). If a proposal is chosen for adding it
	 * to a question, a new element is created and attached to the current
	 * structure. The direction information is exploited to locate the element
	 * that will become the parent (=previous) element of the newly created one.
	 * 
	 * @return a set of proposals for words that can be added to the current
	 *         question.
	 * @see {@link Path#nextWords()}.
	 */
	protected Set<Proposal> nextProposals() {
		if(next != null) {
			return proposalsOfNextElement();
		} else if(lower != null) {
			return proposalsOfLowerElement();
		} else {
			return proposalsOfCurrentElement();
		}
	}
		
	protected String phrase() {
		StringBuilder phrase = new StringBuilder();
		
		if(lower != null) {
			phrase.append(lower.phrase());
		} else {
			phrase.append(' ')
			      .append(getLabel());
		}
		
		if(next != null) {
			phrase.append(next.phrase());
		}
		
		return phrase.toString();
	}

	/**
	 * Adds a proposal to the current structure.
	 * 
	 * @param proposal the proposal to add.
	 * @see #nextProposals()
	 */
	protected void add(Proposal proposal) {
		Direction direction = proposal.direction();
		
		switch(direction) {
		case CURRENT:
			setNextElement(subPathFor(proposal));
			break;
		case PREVIOUS:
			previous.add(proposal);
			break;
		case NEXT:
			next.add(proposal);
			break;
		case UPPER:
			upper.add(proposal);
			break;
		case LOWER:
			lower.add(proposal);
			break;
		default:
			// should never happen, unless someone adds additional direction constants.
			throw new HawkshawException(QueryCore.getDefault(), "Error while adding a proposal. Invalid direction information found.");
		}
	}
	
	/**
	 * Returns whether the current subtree is complete. Complete means that no additional symbols can be added.
	 * 
	 * @return <code>true</code> if no more symbols can be added, <code>false</code> otherwise.
	 */
	// TODO What happens if last symbol of a rule is optional? Two paths? Test.
	protected boolean isComplete() {
		boolean nextIsComplete  = symbol.isLastSymbolOfRule() ||
								  ((next != null) && next.isComplete());
		boolean lowerIsComplete = (lower == null) || lower.isComplete();
		
		return nextIsComplete && lowerIsComplete;
	}

	/**
	 * Removes the last element from this subtree. This is triggered when a word
	 * is deleted from a question.
	 */
	protected void removeLastElement() {
		if(next != null) {
			if(next.isLastElement()) {
				next = null;
			} else {
				next.removeLastElement();
			}
		} else if(lower != null) {
			lower.removeLastElement();
		}
	}

	/**
	 * Checks whether this element is currently a leaf in the subtree, i.e., if it has no next and lower elements.
	 * 
	 * @return <code>true</code> if no next and no lower element exists, <code>false</code> otherwise.
	 */
	protected boolean isLastElement() {
		if(next != null) {
			return false;
		} else if(lower != null) {
			return lower.isLastElement();
		} else {
			return true;
		}
	}

	protected Set<QueryStatement> mergeQueryStatements() {
		Set<QueryStatement> result = new HashSet<>();
		
		if(lower != null && next != null) {
			// e.g., non-terminal in the middle of a sentence.
			Set<QueryStatement> lowerStmts = lower.mergeQueryStatements();
			Set<QueryStatement> nextStmts = next.mergeQueryStatements();
			
			for(QueryStatement lowerStmt : lowerStmts) {
				for(QueryStatement nextStmt : nextStmts) {		
					result.add(nextStmt.merge(lowerStmt));
				}
			}
		} else if(lower != null && next == null) {
			// e.g., non-terminal at the end of a sentence.
			Set<QueryStatement> lowerStmts = lower.mergeQueryStatements();
			Set<QueryStatement> thisStmts = symbol.getQueryStatements();
			
			for(QueryStatement lowerStmt : lowerStmts) {
				for(QueryStatement thisStmt : thisStmts) {		
					result.add(thisStmt.merge(lowerStmt));
				}
			}
		} else if(lower == null && next != null) {
			// normal word, somewhere before the end of the sentence.
			result.addAll(next.mergeQueryStatements());
		} else {
			// normal word at the end of the sentence.
			result.addAll(symbol.getQueryStatements());
		}
		
		return result;
	}

	/**
	 * Continues the search for proposals at the next element and adds the
	 * direction information accordingly ({@link Direction#NEXT}) when search
	 * returns.
	 * 
	 * @return a set of proposals for words that can be added to the subtree
	 *         rooted at the next element.
	 */
	private Set<Proposal> proposalsOfNextElement() {
		Set<Proposal> proposalsOfNextElement = next.nextProposals();
		addDirectionInfoTo(proposalsOfNextElement, Direction.NEXT);
		return proposalsOfNextElement;
	}

	/**
	 * Continues the search for proposals at the lower element and adds the
	 * direction information accordingly ({@link Direction#LOWER}) when search
	 * returns.
	 * 
	 * @return a set of proposals for words that can be added to the subtree
	 *         rooted at the lower element.
	 */
	private Set<Proposal> proposalsOfLowerElement() {
		Set<Proposal> proposalsOfLowerElement = lower.nextProposals();
		addDirectionInfoTo(proposalsOfLowerElement, Direction.LOWER);
		return proposalsOfLowerElement;
	}

	/**
	 * When search ends at the current element, the encapsulated {@link #symbol}
	 * is queried for its proposals and direction information (
	 * {@link Direction.CURRENT}) is added to them.
	 * 
	 * In case that the current symbol has no next symbols/proposals, this means
	 * that the current rule is complete and search continues at the upper
	 * element to check for incomplete upper rules.
	 * 
	 * There is a special case, where a rule can be complete although the
	 * {@link #symbol} has more proposals. Consider the following rules: <br>
	 * <br>
	 * 
	 * <pre>
	 * 1. &lt;START&gt; ::= Where is &lt;name&gt; now ?
	 * 2. &lt;name&gt;  ::= Michael
	 * 3. &lt;name&gt;  ::= Michael Wuersch
	 * </pre>
	 * 
	 * The rules in line 2 and 3 are merged together and both symbols, the one
	 * with the label "Michael" and the one with the label "Wuersch", are
	 * flagged as the last symbol of a rule. In this case, the next proposals of
	 * the upper symbol "&lt;name&gt;", as well as the proposals of the
	 * "Michael" symbol, are relevant.
	 * 
	 * FIXME What if last word is optional?
	 * 
	 * @return a set of proposals of words that can succeed the current element.
	 */
	private Set<Proposal> proposalsOfCurrentElement() {
		Set<Proposal> proposals;
		
		if(symbol.hasNext()) {
			// Non-Terminals do not have domain/range information attached,
			// therefore we need to look for the lowest word, which has to be a terminal symbol.
			Element lowestWord = lowestWord();
			proposals =  symbol.nextProposals(lowestWord.nounRestriction(), lowestWord.verbRestriction());
			addDirectionInfoTo(proposals, Direction.CURRENT);
			
			if(symbol.isLastSymbolOfRule()) { // check whether there is an upper rule that has symbols left.
				proposals.addAll(proposalsOfUpperElement());
			}
		} else {
			proposals = proposalsOfUpperElement();
		}
		
		return proposals;
	}
	
	/**
	 * Continues the search for proposals at the upper element and adds the
	 * direction information accordingly ({@link Direction#UPPER}) when search
	 * returns.
	 * 
	 * @return a set of proposals for words that can be added to the subtree
	 *         rooted at the upper element.
	 */
	private Set<Proposal> proposalsOfUpperElement() {
		Set<Proposal> proposalsOfUpperElement = null;
		
		if(upper != null) {			
			proposalsOfUpperElement = upper.proposalsOfCurrentElement();
			addDirectionInfoTo(proposalsOfUpperElement, Direction.UPPER);
			return proposalsOfUpperElement;
		} else if(previous != null) {
			proposalsOfUpperElement = previous.proposalsOfUpperElement();
			addDirectionInfoTo(proposalsOfUpperElement, Direction.PREVIOUS);
		} else {
			proposalsOfUpperElement = Collections.emptySet();
		}
		
		return proposalsOfUpperElement;
	}

	private Element lowestWord() {
		return (lower == null) ? this : lower.lowestWord();
	}
	
	private ChainingRestriction nounRestriction() {
		ChainingRestriction restriction;
		
		if(isVerb()) {
			restriction =  symbol.getChainingRestriction();
		} else {
			Element previousVerb = previousVerb();
			restriction = previousVerb != null ? previousVerb.nounRestriction() : ChainingRestriction.EMPTY;
		}
		
		return restriction;
	}
	
	private ChainingRestriction verbRestriction() {
		ChainingRestriction restriction;
		
		if(isSubject()) {
			restriction =  symbol.getChainingRestriction();
		} else {
			Element previousSubject = previousSubject();
			restriction = previousSubject != null ? previousSubject.verbRestriction() : verbRestrictionFallback();
		}
		
		return restriction;
	}
	
	private ChainingRestriction verbRestrictionFallback() {
		ChainingRestriction restriction;
		
		if(isNoun()) {
			restriction =  symbol.getChainingRestriction();
		} else {
			Element previousNoun = previousNoun();
			restriction = previousNoun != null ? previousNoun.verbRestriction() : ChainingRestriction.EMPTY;
		}
		
		return restriction;
	}
	
	private Element previousWord() {
		Element element = this;
		
		while(element.upper != null) {
			element = element.upper;
		}
		
		if(element.previous == null) {
			return null;
		} else {
			element = element.previous;
			while(true) {
				if(element.lower == null) {
					return element;
				} else {
					element = element.lower;
					while(element.next != null) {
						element = element.next;
					}
				}
			}
		}
	}
	
	private Element previousNoun() {
		Element element = previousWord();
		
		if(element != null && !element.isNoun()) {
			element = element.previousNoun();
		}
		
		return element;
	}

	private Element previousSubject() {
		Element element = previousWord();
		
		if(element != null && !element.isSubject()) {
			element = element.previousSubject();
		}
		
		return element;
	}

	private Element previousVerb() {
		Element element = previousWord();
		
		if(element != null && !element.isVerb()) {
			element = element.previousVerb();
		}
		
		return element;
	}

	private boolean isNoun() {
		boolean isNoun = symbol.isNoun();
		
		if(!isNoun && upper != null) {
			isNoun = upper.isNoun();
		}
		
		return isNoun;
	}

	private boolean isSubject() {
		boolean isSubject = symbol.isSubject();
		
		if(!isSubject && upper != null) {
			isSubject = upper.isSubject();
		}
		
		return isSubject;
	}

	private boolean isVerb() {
		boolean isVerb = symbol.isVerb();
		
		if(!isVerb && upper != null) {
			isVerb = upper.isVerb();
		}
		
		return isVerb;
	}

	/**
	 * Adds the given direction information to each proposal in the given set.
	 * 
	 * @param proposals the proposals that should receive the direction information.
	 * @param direction the direction information that should be added to each proposal.
	 * @see Proposal#addDirectionInformation(Direction)
	 */
	private void addDirectionInfoTo(Set<Proposal> proposals, Direction direction) {
		for(Proposal proposal : proposals) {
			proposal.addDirectionInformation(direction);
		}
	}

	/**
	 * In addition to the direction information, a proposal also stores the
	 * upper symbols of the proposed one. For each of these non-terminal
	 * symbols, it is also necessary to add a new element to the structure. This
	 * method creates this subtree recursively and returns the root element that
	 * can then be added to the existing structure.
	 * 
	 * @param proposal
	 *            the proposal to add.
	 * @return a chain of elements if the proposal's symbol had upper symbols,
	 *         or just an element containing the symbol has no upper symbols.
	 */
	private Element subPathFor(Proposal proposal) {
		Stack<AbstractSymbol> upperSymbols = proposal.getUpperSymbols();
	
		Element element = null;
		if(upperSymbols.empty()) {
			element = new Element(proposal.getSymbol());
		} else {
			element = new Element(upperSymbols.pop());
			element.setLowerElement(subPathFor(proposal));
		}
		
		return element;
	}

	/**
	 * Sets the next element, and for the next element, the previous one.
	 * 
	 * @param element the next element of the current one.
	 */
	private void setNextElement(Element element) {
		next = element;
		element.previous = this;
	}

	/**
	 * Sets the lower element, and for the lower element, the upper one.
	 * 
	 * @param element the lower element of the current one.
	 */
	private void setLowerElement(Element element) {
		lower = element;
		element.upper = this;
	}
	
	private String getLabel() {
		return symbol.getLabel();
	}
}
