package ch.uzh.ifi.seal.hawkshaw.core.grammar;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.core
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * Classification of the types of sentences that are possible. Also provides a lookup
 * table to convert a String representation of the sentence type to an enum.
 * 
 * 
 * @author wuersch
 * 
 */
public enum SentenceType {
	/**
	 * Default sentence type, i.e. used when the type is unknown/not important.
	 */
	UNDEF("UNDEF"),

	/**
	 * Open questions are likely to receive a long answer. They begin with
	 * question words, such as: what, why, how, describe.
	 */
	OPEN_QUESTION("SPO_QOBJ"),

	/**
	 * Generally, closed questions can either be answered with a single word or
	 * a short phrase. Here, we use the more limiting definition that those kind
	 * of questions can be answered with either 'yes' or 'no'.
	 */
	CLOSED_QUESTION("SPO_QCLOSED"),

	/**
	 * How-many questions, where we expect to receive some kind of number as
	 * answer.
	 */
	ENUM_QUESTION("SPO_QENUM"),

	/**
	 * Spatial questions, where we expect to receive some kind of location as
	 * answer. E.g., "Where did the last modification take place?"
	 */
	SPAT_QUESTION("SPO_QSPAT"),

	/**
	 * Temporal questions, where we expect to receive a time or date as answer,
	 * E.g., "When did the last modification take place?"
	 */
	TEMP_QUESTION("SPO_QTEMP"),

	/**
	 * A sentence that commonly makes a statement, i.e., expresses some kind of
	 * knowledge. E.g. "Module A belongs to System Y."
	 */
	DECLARATION("SPO_DECL"),

	// /**
	// * TODO Useful?
	// */
	// REQUEST,
	//	
	/**
	* TODO aka. Command. Useful?
	*/
	IMPERATIVE("SPO_IMPER");

	private static final Map<String, SentenceType> LOOKUP = new HashMap<>();

	static {
		for (SentenceType type : EnumSet.allOf(SentenceType.class)) {
			LOOKUP.put(type.getSymbol(), type);
		}
	}

	private String symbol;

	private SentenceType(String symbol) {
		this.symbol = symbol;
	}

	/**
	 * Sentence types have textual representations in the grammar, e.g.,
	 * SPO_QSPAT for spatial questions. This method returns the symbol for a
	 * given sentence type.
	 * 
	 * @return a string representation of the sentence type, found in the
	 *         grammar files.
	 */
	public String getSymbol() {
		return "<" + symbol + ">";
	}

	/**
	 * Looks up the sentence type, given a symbol from the grammar.
	 * 
	 * @param symbol
	 *            the symbol from the grammar, e.g., "SPO_QOBJ".
	 * @return if the symbol is known, its corresponding sentence type is looked
	 *         up and returned. Otherwise, the method will return UNDEF.
	 */
	public static SentenceType lookup(String symbol) {
		SentenceType result = LOOKUP.get(symbol);
		return result != null ? result : UNDEF;
	}
}
