package ch.uzh.ifi.seal.hawkshaw.core;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.core
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Plugin;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.jobs.IJobManager;
import org.eclipse.core.runtime.jobs.Job;
import org.osgi.framework.BundleContext;

import ch.uzh.ifi.seal.hawkshaw.core.grammar.Grammar;
import ch.uzh.ifi.seal.hawkshaw.core.grammar.GrammarFactory;
import ch.uzh.ifi.seal.hawkshaw.core.query.Session;
import ch.uzh.ifi.seal.hawkshaw.ontology.OntologyCore;
import ch.uzh.ifi.seal.hawkshaw.ontology.model.PersistentJenaModel;
import ch.uzh.ifi.seal.hawkshaw.support.plugin.IHawkshawPlugin;

/**
 * The activator class controls the plug-in life cycle and provides convenient access to files associated with this bundle.
 * 
 * @author wuersch
 * 
 */
// TODO Check exported packages.
// TODO externalize strings
// TODO internal packages
public class QueryCore extends Plugin implements IHawkshawPlugin {

	/**
	 *  The plug-in ID.
	 */
	public static final String PLUGIN_ID = "ch.uzh.ifi.seal.hawkshaw.core";

	public static final String FAMILY_INIT = PLUGIN_ID + ".jobs.init";

	public static final String EXTENSION_ID_GRAMMAR = "ch.uzh.ifi.seal.hawkshaw.grammar";

	/**
	 *  The shared instance.
	 */
	private static QueryCore plugin;
	
	private GrammarFactory grammarFactory;

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.runtime.Plugin#start(org.osgi.framework.BundleContext)
	 */
	@Override
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
		
		grammarFactory = new GrammarFactory();
		grammarFactory.startInit();
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.core.runtime.Plugin#stop(org.osgi.framework.BundleContext)
	 */
	@Override
	public void stop(BundleContext context) throws Exception {
		plugin = null;

		super.stop(context);
	}
	
	public static boolean ready() {
		return getDefault().grammarFactory.initComplete();
	}
	
	public static void waitForInit(IProgressMonitor monitor) {
		SubMonitor progress = SubMonitor.convert(monitor, "Waiting for Query Core Plug-in...", 100);
		
		IJobManager jobManager = Job.getJobManager();
			
		boolean initHasntFinishedYet = true;
		while(initHasntFinishedYet && !progress.isCanceled()) {
			progress.setWorkRemaining(100);
				
			try {
				jobManager.join(QueryCore.FAMILY_INIT, progress.newChild(100));
					
				initHasntFinishedYet = false;
			} catch (InterruptedException e) {
				initHasntFinishedYet = true;
			}
		}
	}
	
	@Override
	public String getID() {
		return PLUGIN_ID;
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static QueryCore getDefault() {
		return plugin;
	}
	
	public static Session startSession(PersistentJenaModel model) {
		Grammar grammar = getDefaultGrammar();
		grammar.setModel(model);
		
		return new Session(grammar);
	}
	
	public static Session startSession(IProject project) {
		return startSession(OntologyCore.fetchPersistentModelFor(project));
	}

	public static Grammar getDefaultGrammar() {
		return getDefault().grammarFactory.getDefaultGrammar();
	}
}
