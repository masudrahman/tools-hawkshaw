package ch.uzh.ifi.seal.hawkshaw.core.grammar.symbols;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.core
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import ch.uzh.ifi.seal.hawkshaw.core.grammar.Grammar;

/**
 * 
 * @author wuersch
 *
 */
public final class SymbolFactory {
	
	/**
	 * Dummy constructor to prevent instantiation.
	 */
	private SymbolFactory() {
		super();
	}
	
	public static AbstractSymbol createSymbolFor(String label, Grammar grammar) {
		if(label.equals("<NC-s>")) {
			return new DynamicNounClassSymbol(label, true, grammar);
		} else if(label.equals("<NC-p>")) {
			return new DynamicNounClassSymbol(label, false, grammar);
		} else if(label.equals("<NI>")) {
			return new DynamicNounInstanceSymbol(label, grammar);
		} else if(label.equals("<V-s>")) {
			return new DynamicVerbSymbol(label, true, grammar);
		} else if(label.equals("<V-p>")) {
			return new DynamicVerbSymbol(label, false, grammar);
		} else if(label.equals("<VATTR>")) {
			return new DynamicVerbAttributeSymbol(label, false, grammar);
		} else if(label.equals("<VATTR-B>")) {
			return new DynamicVerbAttributeSymbol(label, true, grammar);
		} else if(label.equals("<VInter>")) {
			return new DynamicVerbInterrogativeSymbol(label, grammar);
		} else if(label.startsWith("<")) {
			return new NonTerminalSymbol(label, grammar);
		} else {
			return new TerminalSymbol(label, grammar);
		}
	}

}
