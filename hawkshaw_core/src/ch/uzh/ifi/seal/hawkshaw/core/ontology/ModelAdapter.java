package ch.uzh.ifi.seal.hawkshaw.core.ontology;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.core
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.util.Set;

import ch.uzh.ifi.seal.hawkshaw.core.exceptions.QueryExecutionException;
import ch.uzh.ifi.seal.hawkshaw.core.grammar.ChainingRestriction;
import ch.uzh.ifi.seal.hawkshaw.core.grammar.QueryStatement;
import ch.uzh.ifi.seal.hawkshaw.core.ontology.vocabulary.IAnnotations;
import ch.uzh.ifi.seal.hawkshaw.core.query.Answer;
import ch.uzh.ifi.seal.hawkshaw.ontology.model.PersistentJenaModel;

/**
 * Wraps a Jena {@link OntModel}. Provides convenience methods for accessing
 * synonyms, interrogative phrases and query execution.
 * 
 * @author wuersch
 * 
 */
public class ModelAdapter {
	private PersistentJenaModel ontModel;
	
	/**
	 * Constructor.
	 * 
	 * @param ontModel
	 *            the ontology model to wrap. If <code>null</code> subsequent
	 *            calls to most methods of this class will throw an
	 *            {@link NullPointerException}.
	 */
	public ModelAdapter(PersistentJenaModel ontModel) {
		this.ontModel = ontModel;
	}
	
	/**
	 * Sets/replaces the current ontology model.
	 * 
	 * @param ontModel
	 */
	public void setOntModel(PersistentJenaModel ontModel) {
		this.ontModel = ontModel;
	}
	
	/**
	 * Executes a query statement and returns the answer.
	 * 
	 * @param queryStatement
	 *            a complete query statement, i.e., void of any non-terminal
	 *            symbols.
	 * @return the answer.
	 * @throws QueryExecutionException
	 *             if anything goes wrong during query execution (e.g.,
	 *             incorrect query syntax due to a bug).
	 */
	public Answer execute(QueryStatement queryStatement) throws QueryExecutionException {
		// TODO set sentence type
		String formalQueryStatementString = queryStatement.toString();
		
		try {
			Answer answer = execute(formalQueryStatementString);
			answer.setSentenceType(queryStatement.getSentenceType());
			
			return answer;
		} catch(Exception e) {
			throw new QueryExecutionException("Could not execute formal query against ontology model.", queryStatement, e);
		}
	}

	/**
	 * Executes directly a sparql query string
	 * 
	 * @param formalQueryStatementString the sparql query
	 * @return the answer
	 */
	public Answer execute(String formalQueryStatementString) {
		Answer answer = new Answer();
		answer.addFormalQueryStatementString(formalQueryStatementString);
		answer.addAll(ontModel.exec(formalQueryStatementString));
		
		return answer;
	}
	
	/**
	 * Returns the phrases (synonyms) for the given resource, as specified by
	 * {@link IAnnotations#PHRASE}, from the underlying ontology model.
	 * 
	 * @param resource
	 *            the resource whose synonyms we want to fetch.
	 * @return a set of phrases that represent the resource in natural language.
	 */
	public Set<String> getPhrasesFor(String resourceUri) {
		return getPhrasesFor(resourceUri, IAnnotations.PHRASE);
	}
	
	/**
	 * Returns the phrases (synonyms) for the given resource, as specified by
	 * {@link IAnnotations#PHRASE_SINGULAR} or
	 * {@link IAnnotations#PHRASE_PLURAL}, from the underlying ontology model.
	 * 
	 * @param resource
	 *            the resource whose synonyms we want to fetch.
	 * @param singular
	 *            whether singular or plural phrases should be returned.
	 * @return a set of phrases that represent the resource in natural language.
	 */
	public Set<String> getPhrasesFor(String resourceUri, boolean singular) {
		String annotation;
		if(singular) {
			annotation = IAnnotations.PHRASE_SINGULAR;
		} else {
			annotation = IAnnotations.PHRASE_PLURAL;
		}
		
		return getPhrasesFor(resourceUri, annotation);
	}
	
	/**
	 * Returns the interrogative phrases for the given data type property, as
	 * specified by {@link IAnnotations#INTERROGATIVE}, from the underlying
	 * ontology model. An example for such a phrase, given that the data type
	 * property is "hasAge", could be "How old is".
	 * 
	 * @param dProperty
	 *            the data type property whose interrogative phrases we want to
	 *            fetch.
	 * @return a set of interrogative phrases that represent the data type
	 *         property in natural language.
	 */
	public Set<String> getInterrogativesFor(String propertyUri) {
		return ontModel.getLiterals(propertyUri, IAnnotations.INTERROGATIVE);
	}
	
	/**
	 * Returns all the named classes (i.e. all classes, except blank nodes) in the
	 * underlying ontology model that match the range information of the given
	 * {@link ChainingRestriction}.
	 * 
	 * @param restriction
	 *            the chaining restriction.
	 * @return a set of all matching named classes in the underlying
	 *         ontology model.
	 */
	public Set<String> getNamedClassesThatMatch(ChainingRestriction restriction) {
		Set<String> otherRange = restriction.getRange();
		
		return ontModel.getNamedClassesThatMatch(otherRange);
	}
	
	/**
	 * Returns all the individuals (instances) in the underlying ontology model
	 * that match the range information of the given {@link ChainingRestriction}
	 * .
	 * 
	 * @param restriction
	 *            the chaining restriction.
	 * @return a set of all matching individuals in the underlying
	 *         ontology model.
	 */
	public Set<String> getIndividualsThatMatch(ChainingRestriction restriction) {
		Set<String> otherRange = restriction.getRange();
		
		return ontModel.getIndividualsThatMatch(otherRange);
	}

	/**
	 * Returns all the object properties in the underlying ontology model that
	 * match the domain information of the given {@link ChainingRestriction}.
	 * 
	 * @param restriction
	 *            the chaining restriction.
	 * @return a set of all matching object properties in the underlying
	 *         ontology model.
	 */
	public Set<String> getObjectPropertiesThatMatch(ChainingRestriction restriction) {
		Set<String> otherDomain = restriction.getDomain();
		
		return ontModel.getObjectPropertiesThatMatch(otherDomain);
	}

	/**
	 * Returns all the non-boolean data type properties in the underlying ontology model that
	 * match the domain information of the given {@link ChainingRestriction}.
	 * 
	 * @param restriction
	 *            the chaining restriction.
	 * @return a set of all matching non-boolean data type properties in the underlying
	 *         ontology model.
	 */
	public Set<String> getDatatypePropertiesThatMatch(ChainingRestriction restriction) {
		Set<String> otherDomain = restriction.getDomain();
		
		return ontModel.listDatatypePropertiesThatMatch(otherDomain);
	}
	
	/**
	 * Returns all theboolean data type properties in the underlying ontology model that
	 * match the domain information of the given {@link ChainingRestriction}.
	 * 
	 * @param restriction
	 *            the chaining restriction.
	 * @return a set of all matching boolean data type properties in the underlying
	 *         ontology model.
	 */
	public Set<String> getBooleanDatatypePropertiesThatMatch(ChainingRestriction restriction) {
		Set<String> otherDomain = restriction.getDomain();
		
		return ontModel.listBooleanDatatypePropertiesThatMatch(otherDomain);
	}

	/**
	 * Returns the domain of a given data type or object property from the
	 * underlying ontology model. Handles union classes by flattening them.
	 * 
	 * @param property
	 *            the property whose domain we want to fetch.
	 * @return a set URI strings describing the domain of the property.
	 */
	public Set<String> domainOf(String propertyUri) {
		return ontModel.domainOf(propertyUri);
	}
	
	/**
	 * Returns the range of a given data type or object property from the
	 * underlying ontology model. Handles union classes by flattening them.
	 * 
	 * @param property
	 *            the property whose range we want to fetch.
	 * @return a set URI strings describing the range of the property.
	 */
	public Set<String> rangeOf(String propertyUri) {
		return ontModel.rangeOf(propertyUri);
	}
	
	/**
	 * Returns all the classes of the individual.
	 * @param individualUri an individual's uri.
	 * @return the individual's classes uris.
	 */
	public Set<String> getClassesOf(String individualUri) {
		return ontModel.getClassesOf(individualUri);
	}
	
	/**
	 * Returns the labels for the individual.
	 * @param individualUri an individual's uri.
	 * @return the labels.
	 */
	public Set<String> labelsOf(String individualUri) {
		return ontModel.labelsOf(individualUri);
	}

	/**
	 * Helper method for phrases.
	 * 
	 * @param resourceUri the resource uri.
	 * @param annotation the phrase annotation.
	 * @return the phrases.
	 */
	private Set<String> getPhrasesFor(String resourceUri, String annotation) {
		return ontModel.getLiterals(resourceUri, annotation);
	}
}