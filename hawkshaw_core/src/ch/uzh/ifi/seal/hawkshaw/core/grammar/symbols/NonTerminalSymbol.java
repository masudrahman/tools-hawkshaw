package ch.uzh.ifi.seal.hawkshaw.core.grammar.symbols;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.core
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.HashSet;
import java.util.Set;

import ch.uzh.ifi.seal.hawkshaw.core.grammar.ChainingRestriction;
import ch.uzh.ifi.seal.hawkshaw.core.grammar.Grammar;
import ch.uzh.ifi.seal.hawkshaw.core.query.Proposal;

/**
 * 
 * @author wuersch
 *
 */
public class NonTerminalSymbol extends AbstractSymbol {	
	public NonTerminalSymbol(String label, Grammar grammar) {
		super(label, grammar);
	}

	@Override
	protected Set<Proposal> asProposal(ChainingRestriction restrictionForNouns, ChainingRestriction restrictionForVerbs) {
		Set<Proposal> result = new HashSet<>();

		AbstractSymbol rule = getGrammar().getRuleFor(getLabel());
		// if a non-terminal symbol occurs on the right-hand side, we need to
		// replace it by its lower symbols. These are stored by the grammar
		// rule with the same label.
		for (AbstractSymbol lowerSymbol : rule.getSymbolContainer()) {
			for (Proposal proposalsFromLowerSymbol : lowerSymbol.asProposal(restrictionForNouns, restrictionForVerbs)) {
				proposalsFromLowerSymbol.addUpperSymbol(this);
				result.add(proposalsFromLowerSymbol);
			}
		}

		return result;
	}
}
