package ch.uzh.ifi.seal.hawkshaw.core.ontology.vocabulary;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.core
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

public interface IAnnotations {
	String BASE           = "http://se-on.org/ontologies/hawkshaw/2012/02/annotations-nl.owl";
	String LOCAL          = "local_hawkshaw_annotations-nl_12_02";
	
	String PHRASE          = BASE + "#phrase";
	String PHRASE_SINGULAR = BASE + "#phrase-singular";
	String PHRASE_PLURAL   = BASE + "#phrase-plural";
	String INTERROGATIVE   = BASE + "#interrogative";
	String IGNORE          = BASE + "#ignore";
	String EXPLANATION     = BASE + "#explanation";
}
