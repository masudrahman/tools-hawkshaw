package ch.uzh.ifi.seal.hawkshaw.core.grammar;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.core
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.Arrays;

import ch.uzh.ifi.seal.hawkshaw.core.grammar.symbols.AbstractSymbol;

/**
 * Immutable. Represents (parts of) a formal query statement, e.g., in SPARQL. A query
 * statement consists of several parts: the first one contains the variables
 * from the SELECT clause, the second one contains the statements from the WHERE
 * clause.
 * 
 * This class is partially based on code from the Ginseng project. See
 * http://www.ifi.uzh.ch/ddis for more information.
 * 
 * @author wuersch
 * 
 */
public class QueryStatement {
	private String formalLanguage;
	private String[] parts;
	
	private SentenceType sentenceType;
	
	private AbstractSymbol rule; // TODO Should query statement know its rule? Makes query compilation a lot easier...

	/**
	 * Constructor.
	 * 
	 * @param formalLanguage
	 *            an identifier, specifying the formal query language of the
	 *            current statement. Can be empty or null - the identifier is
	 *            only set for query statements belonging to START rules.
	 * @param sentenceType
	 *            the type of the sentence, e.g., 'open question', 'closed
	 *            question', 'spatial question', etc.. This information can be
	 *            used to post-process the results of executing the query
	 *            statements.
	 * @param parts
	 *            the first element contains the variables from the SELECT
	 *            clause, the second one contains the statements from the WHERE
	 *            clause. The third part is for group clauses (optional).
	 */
	public QueryStatement(String formalLanguage, SentenceType sentenceType, String...parts) {
		this.formalLanguage = formalLanguage;
		this.parts = parts;
		this.sentenceType = sentenceType;
	}
	
	/**
	 * Copy-constructor.
	 * 
	 * @param otherStatement a duplicate of the current query statement.
	 * @see #duplicate()
	 */
	private QueryStatement(QueryStatement otherStatement) {
		this.formalLanguage = otherStatement.formalLanguage;
		this.rule = otherStatement.rule;
		this.sentenceType = otherStatement.sentenceType;
		this.parts = Arrays.copyOf(otherStatement.parts, otherStatement.parts.length);
	}
	
	/**
	 * Creates a duplicate of the current query statement.
	 * 
	 * @return a new query statement, identical to the current one.
	 */
	public QueryStatement duplicate() {
		return new QueryStatement(this);
	}
	
	/**
	 * Sets the rule to which this query statement belongs. The label of the
	 * parent rule is needed while merging two query statements.
	 * 
	 * @param rule
	 *            the parent rule.
	 * @see #merge(QueryStatement)
	 */
	public void setRule(AbstractSymbol rule) {
		this.rule = rule;
	}

	/**
	 * Creates a duplicate of the current query statement and merges its parts
	 * with the other query statement by replacing place holders (if the label
	 * of the other statement matches the place holders in the current one).
	 * 
	 * @param other
	 *            the query statement whose parts should be used as a
	 *            replacement for the place holders in the current query
	 *            statement.
	 * @return a new query statement.
	 */
	public QueryStatement merge(QueryStatement other) {
		QueryStatement duplicate = duplicate();
		String[] duplicatedParts = duplicate.parts;
		
		String label =  other.rule.getLabel();
		
		// wild cards, such as <<symbol>>, will get replaced by the other statement's query part with the same index.
		String wildcard = "<" + label + ">";
		// wild cards, such as <<symbol:1>>, refer to the first query part, i.e., the variable.
		String wildcard1 = "<" + label.substring(0, label.length() - 1) + ":1>>";

		for(int i = 0; i < duplicatedParts.length && i < other.parts.length; i++) {
			duplicatedParts[i] = duplicatedParts[i].replaceAll(wildcard, other.parts[i])
												   .replaceAll(wildcard1, other.parts[0]);
		}
		
		// don't override aleady defined sentenceType
		if(duplicate.sentenceType == SentenceType.UNDEF) {
			duplicate.sentenceType = other.sentenceType;
		}

		return duplicate;
	}
	
	public QueryStatement replaceAll(String target, String replacement) {
		QueryStatement result = duplicate();
		
		for(int i = 0; i < result.parts.length; i++) {
			result.parts[i] = result.parts[i].replaceAll(target, replacement);
		}
		
		return result;
	}
	
	public SentenceType getSentenceType() {
		return sentenceType;
	}
	
//	public void execute() {
//		// TODO a query statement should be executable, shouldn't it? Or will this introduce many dependencies to jena stuff? QueryExecutor?
//	}

	/**
	 * Returns a String representation of the current query statement. After all
	 * relevant query statements have been merged, this will result in a valid
	 * formal query statement.
	 */
	@Override
	public String toString() {
		StringBuilder statement = new StringBuilder();
		for(String part : parts) {
			statement.append(part).append(' ');
		}
		
		return statement.toString().trim();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((formalLanguage == null) ? 0 : formalLanguage.hashCode());
		result = prime * result + Arrays.hashCode(parts);
		result = prime * result
				+ ((sentenceType == null) ? 0 : sentenceType.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		QueryStatement other = (QueryStatement) obj;
		if (formalLanguage == null) {
			if (other.formalLanguage != null)
				return false;
		} else if (!formalLanguage.equals(other.formalLanguage))
			return false;
		if (!Arrays.equals(parts, other.parts))
			return false;
		if (sentenceType != other.sentenceType)
			return false;
		return true;
	}
}
