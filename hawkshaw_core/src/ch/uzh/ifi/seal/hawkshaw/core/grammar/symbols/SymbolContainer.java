package ch.uzh.ifi.seal.hawkshaw.core.grammar.symbols;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.core
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.Iterator;
import java.util.Map;
import java.util.TreeMap;

import ch.uzh.ifi.seal.hawkshaw.core.grammar.Grammar;


/**
 * This class stores grammar symbols.
 * 
 * @author wuersch
 *
 */
public class SymbolContainer implements Iterable<AbstractSymbol> {
	private Map<String, AbstractSymbol> symbols;
	
	/**
	 * Constructor.
	 */
	public SymbolContainer() {
		symbols = new TreeMap<>();
	}
		
	/**
	 * Returns the symbol with the given label. Asks the {@link SymbolFactory}
	 * for a new such symbol and stores it, if it does not yet exist.
	 * 
	 * @param label
	 *            the label of the symbol.
	 * @return the symbol with the given label. If no such symbol exists, a new
	 *         one is created and added to {@link #nextSymbols}.
	 */
	public AbstractSymbol fetchOrCreateSymbol(String label, Grammar grammar) {
		AbstractSymbol symbol = symbols.get(label);
		
		if(symbol == null) {
			symbol = SymbolFactory.createSymbolFor(label, grammar);
			symbols.put(label, symbol);
		}
		
		return symbol;
	}
	
	public void removeSymbol(String label) {
		symbols.remove(label);
	}
	
	/**
	 * Returns an iterator over all symbols in this container.
	 * 
	 * @return an iterator over all symbols in this container.
	 */
	@Override
	public Iterator<AbstractSymbol> iterator() {
		return symbols.values().iterator();
	}

	/**
	 * Checks whether a symbol with the given label already exists.
	 * 
	 * @param label
	 *            the label of the symbol we are interested in.
	 * @return <code>true</code> if a symbol with the given label exists,
	 *         <code>false</code> otherwise.
	 */
	public boolean containsSymbol(String label) {
		return symbols.containsKey(label);
	}

	/**
	 * Checks whether there are any symbols in this container.
	 * 
	 * @return <code>true</code> if this container is empty, <code>false</code>
	 *         otherwise.
	 */
	public boolean isEmpty() {
		return symbols.isEmpty();
	}
}
