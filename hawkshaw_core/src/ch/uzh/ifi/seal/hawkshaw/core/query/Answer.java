package ch.uzh.ifi.seal.hawkshaw.core.query;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.core
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.HashSet;
import java.util.Set;

import ch.uzh.ifi.seal.hawkshaw.core.grammar.SentenceType;
import ch.uzh.ifi.seal.hawkshaw.ontology.model.QrItem;

// TODO to string
public class Answer {
	private SentenceType sentenceType;
	
	private Set<QrItem> resultNodes;
	private Set<String> formalQueryStatementStrings;
	private String questionPhrase;
	
	private long compilationTime;
	private long queryExecutionTime;
	
	public Answer() {
		sentenceType = SentenceType.UNDEF;
		resultNodes = new HashSet<>();
		formalQueryStatementStrings = new HashSet<>();
	}
	
	public void setQuestionPhrase(String questionPhrase) {
		this.questionPhrase = questionPhrase;
	}

	public void setCompilationTime(long compilationTime) {
		this.compilationTime = compilationTime;
	}

	public void setQueryExecutionTime(long queryExecutionTime) {
		this.queryExecutionTime = queryExecutionTime;
	}

	public void add(QrItem result) {
		resultNodes.add(result);
	}
	
	public void addAll(Set<QrItem> results) {
		for(QrItem result : results) {
			add(result);
		}
	}

	public void include(Answer other) {
		resultNodes.addAll(other.resultNodes);
		formalQueryStatementStrings.addAll(other.formalQueryStatementStrings);
		
		if(other.sentenceType != SentenceType.UNDEF) {
			sentenceType = other.sentenceType;
		}
	}
	
	public void addFormalQueryStatementString(String queryStatementString) {
		formalQueryStatementStrings.add(queryStatementString);
	}
	
	/**
	 * Returns the set of {@link QrItem}'s that are part of this answer.
	 * 
	 * @return a set of {@link QrItem}'s that were returned as a result of
	 *         executing the queries returned by
	 *         {@link #getFormalQueryStrings()}.
	 */
	public Set<QrItem> getNodes() {
		return new HashSet<>(resultNodes);
	}
	
	/**
	 * Releases the {@link QrItem}'s that are part of this answer.
	 */
	public void dispose() {
		resultNodes.clear();
	}
	
	public Set<String> getFormalQueryStrings() {
		return new HashSet<>(formalQueryStatementStrings);
	}
	
	public String getQuestionPhrase() {
		return questionPhrase;
	}

	public long getCompilationTime() {
		return compilationTime;
	}
	
	public long getQueryExecutionTime() {
		return queryExecutionTime;
	}
	
	public int size() {
		return resultNodes.size();
	}

	public void setSentenceType(SentenceType sentenceType) {
		if(sentenceType == null) {
			this.sentenceType = SentenceType.UNDEF;
		} else {
			this.sentenceType = sentenceType;
		}
	}
	
	public boolean isEnumQuestion() {
		return sentenceType == SentenceType.ENUM_QUESTION;
	}
}
