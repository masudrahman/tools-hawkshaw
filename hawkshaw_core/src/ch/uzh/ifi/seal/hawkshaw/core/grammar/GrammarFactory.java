package ch.uzh.ifi.seal.hawkshaw.core.grammar;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.core
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import org.eclipse.core.runtime.jobs.IJobChangeEvent;
import org.eclipse.core.runtime.jobs.JobChangeAdapter;


public class GrammarFactory extends JobChangeAdapter {
	private boolean initialized;
	
	private Grammar defaultGrammar;
	
	public GrammarFactory() {
		initialized = false;
	}
	
	public void startInit() {
		GrammarInitializer grammarInitializer = new GrammarInitializer();
		grammarInitializer.addJobChangeListener(this);
		grammarInitializer.schedule();
	}
	
	public boolean initComplete() {
		return initialized;
	}

	@Override
	public void done(IJobChangeEvent event) {
		GrammarInitializer grammarInitializer = (GrammarInitializer) event.getJob();
		
		defaultGrammar = grammarInitializer.getGrammar();
		
		initialized = true;
	}
	
	public Grammar getDefaultGrammar() {
		return defaultGrammar;
	}
}
