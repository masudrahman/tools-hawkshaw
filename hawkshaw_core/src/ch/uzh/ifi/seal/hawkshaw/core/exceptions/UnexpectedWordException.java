package ch.uzh.ifi.seal.hawkshaw.core.exceptions;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.core
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.Set;

import ch.uzh.ifi.seal.hawkshaw.core.QueryCore;
import ch.uzh.ifi.seal.hawkshaw.core.query.Question;
import ch.uzh.ifi.seal.hawkshaw.support.exceptions.HawkshawException;

/**
 * This exception occurs if someone tries to add an invalid word to an existing
 * {@link Question} by invoking {@link Question#add(String)} with an invalid
 * parameter, i.e., a <code>String</code> not returned by
 * {@link Question#nextWords()}. The exceptions carries the invalid word, as
 * well as proper propsals.
 * 
 * @author wuersch
 * @see Question#add(String)
 * @see Question#nextWords()
 * 
 */
public class UnexpectedWordException extends HawkshawException {
	private static final long serialVersionUID = -1473290371447635895L;

	private String unexpectedWord;
	private Set<String> alternates;
	
	/**
	 * Constructor.
	 *  
	 * @param message the error message.
	 * @param unexpectedWord the word that could not be added to the current question.
	 * @param alternates alternate proposals of word that can be added to the current question.
	 */
	public UnexpectedWordException(String message, String unexpectedWord, Set<String> alternates) {
		super(QueryCore.getDefault(), message);
		this.unexpectedWord = unexpectedWord;
		this.alternates = alternates;
	}

	/**
	 * Returns the word that could not be added to the current question.
	 * 
	 * @return an invalid word.
	 */
	public String getUnexpectedWord() {
		return unexpectedWord;
	}
	
	/**
	 * Returns a set of words which can be added to the current question.
	 * 
	 * @return a set of valid words.
	 */
	public Set<String> getAlternates() {
		return alternates;
	}
}
