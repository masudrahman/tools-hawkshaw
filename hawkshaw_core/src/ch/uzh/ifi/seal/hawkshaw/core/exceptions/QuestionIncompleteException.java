package ch.uzh.ifi.seal.hawkshaw.core.exceptions;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.core
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import ch.uzh.ifi.seal.hawkshaw.core.QueryCore;
import ch.uzh.ifi.seal.hawkshaw.core.query.Question;
import ch.uzh.ifi.seal.hawkshaw.support.exceptions.HawkshawException;

/**
 * This exception is thrown if someone tries to execute a formal query based on
 * an incomplete {@link Question} by invoking {@link Question#ask()}. A question
 * is incomplete if {@link Question#isComplete()} == false. The exception is
 * unchecked - clients should therefore check for completeness before attempting
 * a query.
 * 
 * @author wuersch
 * 
 */
public class QuestionIncompleteException extends HawkshawException {
	private static final long serialVersionUID = 3172789498124174408L;

	/**
	 * Constructor.
	 * 
	 * @param message
	 *            a message describing the cause of the exception.
	 */
	public QuestionIncompleteException(String message) {
		super(QueryCore.getDefault(), message);
	}

}
