#!/bin/sh
#
# This script invokes the Maven build of Hawkshaw.
#
# Usage:
#
# sh buildall.sh [-i]
#
# -i will install to ~/.m2, otherwise /tmp/seal.localrepo is used.
#

P1="-i"
LOCALREPO=/tmp/seal.localrepo

if [ $# -ne 0 ] ; then
	if [ $1 = $P1 ] ; then
		echo "Installing to ~/.m2"
		mvn -f hawkshaw_lib-parent/pom.xml clean install
		mvn -f hawkshaw_parent/pom.xml clean install
	else
		echo "unrecognized parameter: $1"
	fi
else
	echo "Installing to $LOCALREPO"
	
	mvn -f hawkshaw_lib-parent/pom.xml clean install -Dmaven.repo.local=$LOCALREPO
	mvn -f hawkshaw_parent/pom.xml clean install -Dmaven.repo.local=$LOCALREPO
fi	
