package ch.uzh.ifi.seal.hawkshaw.support.collections.iterators;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.support.test
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import ch.uzh.ifi.seal.hawkshaw.support.collections.iterators.FilteringIterator;
import ch.uzh.ifi.seal.hawkshaw.support.misc.IPredicate;

public class FilteringIteratorTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testEmptyPredicate() {
		List<Dummy> dummyList = new ArrayList<>();
		dummyList.add(new Dummy("first element"));
		dummyList.add(new Dummy("second element"));
		dummyList.add(new Dummy("third element"));
		
		Iterator<Dummy> dummyIterator = dummyList.iterator();
		
		IPredicate<Dummy> emptyPredicate = new IPredicate<Dummy>() {
			public boolean evaluate(Dummy object) {
				return true;
			}
		};
		
		FilteringIterator<Dummy> filteringIterator = new FilteringIterator<>(dummyIterator, emptyPredicate);
		
		assertTrue("The iterator should have more elements.", filteringIterator.hasNext());
		assertEquals("The iterator returned an unexpected element.", "first element", filteringIterator.next().getContent());
		assertTrue("The iterator should have more elements.", filteringIterator.hasNext());
		assertEquals("The iterator returned an unexpected element.", "second element", filteringIterator.next().getContent());
		assertTrue("The iterator should have more elements.", filteringIterator.hasNext());
		assertEquals("The iterator returned an unexpected element.", "third element", filteringIterator.next().getContent());
		assertFalse("The iterator should be empty by now.", filteringIterator.hasNext());
	}
	
	@Test(expected = NoSuchElementException.class)
	public void testEmptyListIterator() {
		List<Dummy> emptyList = new ArrayList<>();
		
		
		Iterator<Dummy> dummyIterator = emptyList.iterator();
		
		IPredicate<Dummy> emptyPredicate = new IPredicate<Dummy>() {
			public boolean evaluate(Dummy object) {
				return true;
			}
		};
		
		FilteringIterator<Dummy> filteringIterator = new FilteringIterator<>(dummyIterator, emptyPredicate);
		
		assertFalse("The iterator should have no elements.", filteringIterator.hasNext());
		filteringIterator.next();
	}
	
	@Test
	public void testSimplePredicate() {
		List<Dummy> dummyList = new ArrayList<>();
		dummyList.add(new Dummy("first element"));
		dummyList.add(new Dummy("second element - remove me!"));
		dummyList.add(new Dummy("third element"));
		
		Iterator<Dummy> dummyIterator = dummyList.iterator();
		
		IPredicate<Dummy> emptyPredicate = new IPredicate<Dummy>() {
			public boolean evaluate(Dummy object) {
				String content = object.getContent();
				return !content.equals("second element - remove me!");
			}
		};
		
		FilteringIterator<Dummy> filteringIterator = new FilteringIterator<>(dummyIterator, emptyPredicate);
		
		assertTrue("The iterator should have more elements.", filteringIterator.hasNext());
		assertEquals("The iterator returned an unexpected element.", "first element", filteringIterator.next().getContent());
		assertTrue("The iterator should have more elements.", filteringIterator.hasNext());
		assertEquals("The iterator returned an unexpected element.", "third element", filteringIterator.next().getContent());
		assertFalse("The iterator should be empty by now.", filteringIterator.hasNext());
	}

	private class Dummy {
		private String content;
	
		public Dummy(String content) {
			this.content = content;
		}
		
		public String getContent() {
			return content;
		}
	}
}
