package ch.uzh.ifi.seal.hawkshaw.support.io;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.support.test
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import java.io.File;
import java.util.Iterator;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import ch.uzh.ifi.seal.hawkshaw.support.HawkshawSupportPlugin;
import ch.uzh.ifi.seal.hawkshaw.support.misc.IPredicate;

public class DirectoryListingTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testDirectoryListing() {
		String path = ResourceLocator.resolveAbsolutePath(HawkshawSupportPlugin.getDefault(), "test_data/folder");
		DirectoryListing content = new DirectoryListing(new File(path), new IPredicate<File>() {

			public boolean evaluate(File f) {
				return f.getName().endsWith(".txt");
			}
			
		});
		
		Iterator<File> fileIterator = content.iterator();
		assertEquals(path + "afile.txt", fileIterator.next().getAbsolutePath());
		assertEquals(path + "subfolder/cfile.txt", fileIterator.next().getAbsolutePath());
		assertFalse(fileIterator.hasNext());
		
		content = new DirectoryListing(new File(path), new IPredicate<File>() {
			
			public boolean evaluate(File f) {
				return f.getName().endsWith(".egf");
			}
			
		});
		
		fileIterator = content.iterator();
		assertEquals(path + "subfolder/bfile.egf", fileIterator.next().getAbsolutePath());
		assertFalse(fileIterator.hasNext());
	}
}
