package ch.uzh.ifi.seal.hawkshaw.support.io;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.support.test
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import static org.junit.Assert.assertThat;
import static org.hamcrest.core.Is.is;

import java.io.File;
import java.io.IOException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class TestFsUtil {
	private static final String TMP_FOLDER = System.getProperty("java.io.tmpdir");

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}
	
	@Test
	public void testFileDeletion() throws IOException {
		File file = new File(TMP_FOLDER + "delete.me");
		assertThat(file.createNewFile(), is(true));
		assertThat(file.exists(), is(true));
		assertThat(FileUtil.delete(file), is(true));
		assertThat(file.exists(), is(false));
	}
	
	@Test
	public void testSimpleDirectoryDeletion() throws IOException {
		File dir = new File(TMP_FOLDER + "delete");
		assertThat(dir.mkdir(), is(true));
		assertThat(dir.exists(), is(true));
		assertThat(dir.isDirectory(), is(true));
		assertThat(FileUtil.delete(dir), is(true));
		assertThat(dir.exists(), is(false));
	}
	
	@Test
	public void testNonEmptyDirectoryDeletion() throws IOException {
		File dir = new File(TMP_FOLDER + "delete/me/after/test");
		File file1 = new File(TMP_FOLDER + "delete/top.txt");
		File file2 = new File(TMP_FOLDER + "delete/me/intermediate.txt");
		File file3 = new File(TMP_FOLDER + "delete/me/after/test/bottom.txt");
		
		assertThat(dir.mkdirs(), is(true));
		assertThat(dir.isDirectory(), is(true));
		
		assertThat(file1.createNewFile(), is(true));
		assertThat(file2.createNewFile(), is(true));
		assertThat(file3.createNewFile(), is(true));
		
		assertThat(file1.exists(), is(true));
		assertThat(file2.exists(), is(true));
		assertThat(file3.exists(), is(true));
		
		assertThat(FileUtil.delete(dir), is(true));
		assertThat(dir.exists(), is(false));
	}
}
