package ch.uzh.ifi.seal.hawkshaw.ui.views.favorites;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ui
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.jdt.core.IJavaProject;

import ch.uzh.ifi.seal.hawkshaw.core.query.Answer;

public class FavoriteQueryItem {
	private String questionPhrase;
	private SparqlQueryItem[] sparqlQueries;
	private String projectHandle;
	
	private boolean isEditable;
	
	public FavoriteQueryItem(String question, Set<String> sparqlQueries, String projectHandle) {
		this.questionPhrase = question;
		this.sparqlQueries = asSparqlQueryItems(sparqlQueries);
		this.projectHandle = projectHandle;
		
		isEditable = false;
	}
	
	public FavoriteQueryItem(Answer answer, IJavaProject project) {
		this(answer.getQuestionPhrase(), answer.getFormalQueryStrings(), project.getHandleIdentifier());
	}
	
	public void setQuestionPhrase(String questionPhrase) {
		this.questionPhrase = questionPhrase;
	}
	
	public String getQuestionPhrase() {
		return questionPhrase;
	}
	
	public Set<String> getSparqlQueries() {
		Set<String> result = new HashSet<>();
		
		for(SparqlQueryItem queryItem : sparqlQueries) {
			result.add(queryItem.toString());
		}
		
		return result;
	}
	
	public Object[] getSparqlQueryItems() {
		return Arrays.copyOf(sparqlQueries, sparqlQueries.length);
	}

	public String getProjectHandle() {
		return projectHandle;
	}

	private SparqlQueryItem[] asSparqlQueryItems(Set<String> sparqlQueryStrings) {
		SparqlQueryItem[] result = new SparqlQueryItem[sparqlQueryStrings.size()];
		
		int i = 0;
		for(String sparqlQuery : sparqlQueryStrings) {
			result[i++] = new SparqlQueryItem(sparqlQuery);
		}
		
		return result;
	}

	public void setEditable(boolean isEditable) {
		this.isEditable = isEditable;
	}

	public boolean isEditable() {
		return isEditable;
	}
}
