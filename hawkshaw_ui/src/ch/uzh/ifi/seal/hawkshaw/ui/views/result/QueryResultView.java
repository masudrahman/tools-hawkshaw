package ch.uzh.ifi.seal.hawkshaw.ui.views.result;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ui
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.eclipse.core.runtime.Platform;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.viewers.IContentProvider;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;

import ch.uzh.ifi.seal.hawkshaw.core.query.Answer;
import ch.uzh.ifi.seal.hawkshaw.ontology.model.PersistentJenaModel;
import ch.uzh.ifi.seal.hawkshaw.ontology.model.QrItem;
import ch.uzh.ifi.seal.hawkshaw.ontology.vocabulary.IHawkshawOntology;
import ch.uzh.ifi.seal.hawkshaw.ui.IHawkshawImages;
import ch.uzh.ifi.seal.hawkshaw.ui.QueryUI;
import ch.uzh.ifi.seal.hawkshaw.ui.actions.result.ClearResultsAction;
import ch.uzh.ifi.seal.hawkshaw.ui.actions.result.CopySparqlToClipboardAction;
import ch.uzh.ifi.seal.hawkshaw.ui.actions.result.OpenInBrowserAction;
import ch.uzh.ifi.seal.hawkshaw.ui.actions.result.SaveAsFavoriteAction;
import ch.uzh.ifi.seal.hawkshaw.ui.actions.result.ShowSourceAction;
import ch.uzh.ifi.seal.hawkshaw.ui.messages.HawkshawMessage;
import ch.uzh.ifi.seal.hawkshaw.ui.util.IField;
import ch.uzh.ifi.seal.hawkshaw.ui.util.TableView;
import ch.uzh.ifi.seal.hawkshaw.ui.views.shared.IQrItemContainer;

public class QueryResultView extends TableView {
	public static final String VIEW_ID = "ch.uzh.ifi.seal.hawkshaw.ui.queryresultview";
	
	private QueryResultProvider contentProvider;
	private List<ResultTreeItem> results;
	
	private ClearResultsAction clearResultsAction;
	
	private boolean showHint;
	
	public QueryResultView() {
		super(SWT.H_SCROLL
			| SWT.V_SCROLL
			| SWT.MULTI
			| SWT.FULL_SELECTION);
		contentProvider = new QueryResultProvider();
		results = new ArrayList<>();
		showHint = true;
	}
	
	@Override
	public void createPartControl(Composite parent) {
		super.createPartControl(parent);
		updateStatusLabelWith("No matches found.");
		
		enableSelectionDrag();
		
		addWelcomeMessage();
	}
	
	public void addResult(Result result) {
		results.add(new ResultTreeItem(result));
		setInput(results);
		
		Answer answer = result.getAnswer();
		updateStatusLabelWith("Answering the question '" + answer.getQuestionPhrase() + "' took " + (answer.getCompilationTime() + answer.getQueryExecutionTime()) +" ms and returned " + answer.size() + " result(s).");
		updateClearResultsAction();
		
		if(showHint) {
			addHintMessage();
			showHint = false;
		}
	}
	
	public void clearResults() {
		results.clear();
		setInput(results);
		
		updateStatusLabelWith("No matches found.");
		updateClearResultsAction();
	}
	
	
	@Override
	protected IField[] getAllFields() {
		return new IField[] { new ResultLabelField(), /* new ResultUriField(),*/ new ResultCompilationTimeField(), new ResultQueryExecutionTimeField() };
	}

	@Override
	protected void initMenu(IMenuManager menu) {
		// TODO Auto-generated method stub
		// TODO export to owl/rdf
	}

	@Override
	protected void initToolBar(IToolBarManager toolbar) {
		clearResultsAction = new ClearResultsAction(this);
		clearResultsAction.setEnabled(false);
		toolbar.add(clearResultsAction);
	}

	@Override
	protected void fillContextMenu(IMenuManager manager) {
		IStructuredSelection selection = (IStructuredSelection) getViewer().getSelection();
		if(!selection.isEmpty()) {
			Object selectedElement = selection.getFirstElement();
			
			IJavaElement javaElement;
			if(selectedElement instanceof IJavaElement) {
				javaElement = (IJavaElement) selectedElement;
			} else {
				javaElement = (IJavaElement) Platform.getAdapterManager().getAdapter(selectedElement, IJavaElement.class);
			}
			
			if(javaElement != null && selection.size() < 2) {
				manager.add(new ShowSourceAction(javaElement));
			}
			
			Answer answer;
			if(selectedElement instanceof Answer && selection.size() < 2) {
				answer = (Answer) selectedElement;
			} else {
				answer = (Answer) Platform.getAdapterManager().getAdapter(selectedElement, Answer.class);
			}
			
			if(answer != null) {
				manager.add(new CopySparqlToClipboardAction(answer));
			}
			
			if(selectedElement instanceof AnswerTreeItem && selection.size() < 2) {
				AnswerTreeItem item = (AnswerTreeItem) selectedElement;
				manager.add(new SaveAsFavoriteAction(item.getItem(), item.getAssociatedJavaProject()));
			}
			
			if(selectedElement instanceof IQrItemContainer && selection.size() < 2) {
				IQrItemContainer container = (IQrItemContainer) selectedElement;
				QrItem[] items = container.getItems();
				if(items.length == 1) {
					QrItem item = items[0];
					PersistentJenaModel model = item.getModel();
					Set<String> urls = model.getLiterals(item.getUri(), IHawkshawOntology.HAS_URL);
					if(urls.size() > 0) {
						manager.add(new OpenInBrowserAction(urls.iterator().next()));
					}
				}
			}
		}
	}

	@Override
	protected Object createViewerInput() {
		return null; // no initial input
	}

	@Override
	protected void viewerSelectionChanged(IStructuredSelection selection) {
		Object element = selection.getFirstElement();
		if(element instanceof AnswerTreeItem) {
			AnswerTreeItem answerTreeItem = (AnswerTreeItem) element;
			Answer answer = answerTreeItem.getItem();
			updateStatusLabelWith("Question '" + answer.getQuestionPhrase() + "' has " + answer.size() + " answer(s).");
		}
	}

	@Override
	protected IContentProvider getContentProvider() {
		return contentProvider;
	}

	private void updateClearResultsAction() {
		clearResultsAction.setEnabled(results.size() > 0);
	}

	private void addWelcomeMessage() {
		HawkshawMessage message = new HawkshawMessage();
		message.setTitle("Hawkshaw Queries");
		message.setImage(QueryUI.getDefault().getImage(IHawkshawImages.SEARCH));
		message.setDescription("Start by asking a <a href=\"open-query-dialog\">Question</a>. The answer will be displayed in the table above.");
		
		setMessage(message);
	}

	private void addHintMessage() {
		HawkshawMessage message = new HawkshawMessage();
		message.setTitle("Additional Hawkshaw Features");
		message.setImage(QueryUI.getDefault().getImage(IHawkshawImages.HAWK_ICON));
		message.setDescription("Drag an answer (or one of its elements) to the <a href=\"show-inspector-view\">Inspector</a> to see additional details. You can right-click on an answer and save the corresponding query as a favorite. This will automatically bring up the <a href=\"show-favorites-view\">Favorites</a> view.");
		
		setMessage(message);
	}

}
