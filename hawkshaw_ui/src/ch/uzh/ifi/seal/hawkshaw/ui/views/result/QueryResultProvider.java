package ch.uzh.ifi.seal.hawkshaw.ui.views.result;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ui
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;


public class QueryResultProvider implements ITreeContentProvider {
	private List<ResultTreeItem> results;
	
	public QueryResultProvider() {
		results = new ArrayList<>();
	}
	
	@Override
	public void dispose() {
		results.clear();
		results = null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		if(newInput == null) {
			results = Collections.emptyList();
		} else {
			results = (List<ResultTreeItem>) newInput;
		}
	}

	@Override
	public Object[] getElements(Object inputElement) {
		Object[] elements = new Object[results.size()];
		
		int i = 0;
		for(ResultTreeItem rtItem : results) {
			elements[i++] = rtItem.getChild();
		}
		
		return elements;
	}

	@Override
	public Object[] getChildren(Object parentElement) {
		Object[] result;
		
		if(parentElement.equals(results)) {
			result =  getElements(parentElement);
		} else if(parentElement instanceof AnswerTreeItem) {
			AnswerTreeItem answerItem = (AnswerTreeItem) parentElement;
			result = answerItem.getChildren();
		} else if(parentElement instanceof QrTreeItem) {
			QrTreeItem rdfNodeItem = (QrTreeItem) parentElement;
			result = rdfNodeItem.getChildren();
		} else {
			result = new Object[0];
		}
		return result;
	}

	@Override
	public Object getParent(Object element) {
		Object parent = null;
		
		if(element instanceof ITreeItem<?>) {
			parent = ((ITreeItem<?>)element).getParent();
		}
		
		return parent;
	}

	@Override
	public boolean hasChildren(Object element) {
		return (element == results && ((List<ResultTreeItem>) results).size() > 0) ||
			   (element instanceof ResultTreeItem) ||
			   (element instanceof AnswerTreeItem && ((AnswerTreeItem)element).getChildren().length > 0) ||
			   (element instanceof QrTreeItem && ((QrTreeItem)element).getChildren().length > 0);
	}

}
