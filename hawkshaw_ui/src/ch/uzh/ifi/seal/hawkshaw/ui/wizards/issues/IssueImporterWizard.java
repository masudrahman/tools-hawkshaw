package ch.uzh.ifi.seal.hawkshaw.ui.wizards.issues;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ui
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.IJobChangeEvent;
import org.eclipse.core.runtime.jobs.JobChangeAdapter;
import org.eclipse.jface.wizard.Wizard;

import ch.uzh.ifi.seal.hawkshaw.ontology.issue.importer.IssueImporterConfig;
import ch.uzh.ifi.seal.hawkshaw.ontology.issue.importer.JiraImporterJob;
import ch.uzh.ifi.seal.hawkshaw.ui.IHawkshawImages;
import ch.uzh.ifi.seal.hawkshaw.ui.QueryUI;
import ch.uzh.ifi.seal.hawkshaw.ui.messages.HawkshawNotificationCenter;

public class IssueImporterWizard extends Wizard {
	private IProject project;
	
	private IssueImporterPage page;
	
	public IssueImporterWizard(IProject project) {
		this.project = project;
	}
	
	@Override
	public void addPages() {
		page = new IssueImporterPage();
		addPage(page);
	}
	
	@Override
	public boolean performFinish() {
		IssueImporterConfig config = createConfig();
		boolean validConfig = config.isValid();
		
		if(validConfig) {
			JiraImporterJob importer = new JiraImporterJob(config);
			importer.addJobChangeListener(new IssueImportJobListener());
			importer.schedule();
		}
		
		return validConfig;
	}

	private IssueImporterConfig createConfig() {
		IssueImporterConfig config = new IssueImporterConfig();
		config.setProject(project);
		config.setTrackerUrl(page.getTrackerUrl());
		config.setIssueKey(page.getIssueKey());
		config.setMaxErrors(page.getMaxErrors());
		config.setSleepAfter(page.getSleepAfter());
		config.setSleepMillis(page.getSleepMillis());
		return config;
	}
	
	private static final class IssueImportJobListener extends JobChangeAdapter {
		@Override
		public void done(IJobChangeEvent event) {
			JiraImporterJob job = (JiraImporterJob) event.getJob();
			IStatus status = event.getResult();
			int severity = status.getSeverity();
			
			String title;
			String notification;
			if(severity == Status.OK) {
				title = "Issues of " + job.getProjectName() + " imported";
				
				notification = "Hawkshaw imported " + job.getNumberOfImportedIssues() + " issues in " + job.getDurationString() + ". Facts about the imported issues can now be queried.";
			} else if(severity == Status.CANCEL) {
					title = "Issue import of " + job.getProjectName() + " incomplete";
					notification = "Import canceled by user after " + job.getNumberOfImportedIssues() + " issues and " + job.getDurationString() + ". Some Facts about the already imported issues can still be queried and import can be restarted at any time.";
			} else { // severity == Status.ERROR
				title = "History of " + job.getProjectName() + " unavailable";
				notification = status.getMessage();
			}
			
			HawkshawNotificationCenter.INSTANCE.notifyUser(title,
														   notification,
														   QueryUI.getDefault().getImage(IHawkshawImages.ISSUE));
		}
	}
}
