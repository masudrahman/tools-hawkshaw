package ch.uzh.ifi.seal.hawkshaw.ui.actions.result;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ui
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jface.action.Action;

import ch.uzh.ifi.seal.hawkshaw.core.query.Answer;
import ch.uzh.ifi.seal.hawkshaw.ui.IHawkshawImages;
import ch.uzh.ifi.seal.hawkshaw.ui.QueryInterfaceDispatcher;
import ch.uzh.ifi.seal.hawkshaw.ui.views.favorites.FavoriteQueryView;

public class SaveAsFavoriteAction extends Action {
	private final Answer answer;
	private final IJavaProject project;
	
	public SaveAsFavoriteAction(Answer answer, IJavaProject project) {
		super("Save as Favorite");
		this.answer = answer;
		this.project = project;
		
		setActionDefinitionId("ch.uzh.ifi.seal.hawkshaw.ui.commands.result.saveAsFavorite");
		setImageDescriptor(IHawkshawImages.FAVS);
	}
	
	@Override
	public void run() {
		FavoriteQueryView view = (FavoriteQueryView) QueryInterfaceDispatcher.INSTANCE.openView(FavoriteQueryView.VIEW_ID);
		view.add(answer, project);
	}
}
