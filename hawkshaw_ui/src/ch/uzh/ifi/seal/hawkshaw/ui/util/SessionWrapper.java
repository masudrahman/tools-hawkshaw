package ch.uzh.ifi.seal.hawkshaw.ui.util;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ui
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import ch.uzh.ifi.seal.hawkshaw.core.grammar.Grammar;
import ch.uzh.ifi.seal.hawkshaw.core.query.Answer;
import ch.uzh.ifi.seal.hawkshaw.core.query.ISessionListener;
import ch.uzh.ifi.seal.hawkshaw.core.query.Question;
import ch.uzh.ifi.seal.hawkshaw.core.query.Session;

public class SessionWrapper {
	private Session session;
	
	private List<ISessionListener> listeners;
	
	public SessionWrapper(Session session) {
		this.session = session;
		
		listeners = new LinkedList<>();
	}
	
	public void startQuestion() {
		session.startQuestion();
	}

	public Set<String> nextWords() {
		return session.nextWords();
	}

	public void add(String word) {
		session.add(word);
		
		fireSessionChanged();
	}

	public void removeLastWord() {
		session.removeLastWord();
		
		fireSessionChanged();
	}
	
	public void clearActiveQuestion() {
		session.clearActiveQuestion();
		
		fireSessionChanged();
	}

	public Question getActiveQuestion() {
		return session.getActiveQuestion();
	}
	
	public String getActiveQuestionString() {
		return session.getActiveQuestionString();
	}
	
	public Answer ask() {
		return session.ask();
	}

	public void setActiveSelection(String name, String uri) {
		Grammar grammar = session.getGrammar();
		grammar.setExternal("<this>", name, uri);
		
		fireSessionChanged();
	}
	
	public void unsetActiveSelection() {
		Grammar grammar = session.getGrammar();
		grammar.unsetExternal("<this>");
		
		fireSessionChanged();
	}
	
	public void register(ISessionListener listener) {
		listeners.add(listener);
	}

	public void unregister(ISessionListener listener) {
		listeners.remove(listener);
	}
//
//	@Override
//	public void selectionChanged() {
//		IJavaElementSelection selection = QueryUI.getCurrentJavaElementSelection();
//		setActiveSelection(selection.toUriString());
//	}

	private void fireSessionChanged() {
		for(ISessionListener listener : listeners) {
			listener.sessionChanged(session);
		}
	}
}
