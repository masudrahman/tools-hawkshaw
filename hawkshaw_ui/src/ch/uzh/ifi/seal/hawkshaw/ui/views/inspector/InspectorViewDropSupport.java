package ch.uzh.ifi.seal.hawkshaw.ui.views.inspector;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ui
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ITreeSelection;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DropTargetAdapter;
import org.eclipse.swt.dnd.DropTargetEvent;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.widgets.Display;

import ch.uzh.ifi.seal.hawkshaw.ontology.model.PersistentJenaModel;
import ch.uzh.ifi.seal.hawkshaw.ontology.model.QrItem;
import ch.uzh.ifi.seal.hawkshaw.ui.IHawkshawImages;
import ch.uzh.ifi.seal.hawkshaw.ui.QueryUI;
import ch.uzh.ifi.seal.hawkshaw.ui.messages.HawkshawMessage;
import ch.uzh.ifi.seal.hawkshaw.ui.views.shared.IQrItemContainer;

public class InspectorViewDropSupport extends DropTargetAdapter {
	private InspectorView callback;
	
	private DropMode preDragMode;
	private DropMode postDropMode;
	
	public InspectorViewDropSupport(InspectorView callback) {
		this.callback = callback;
	}
	
	public void start() {
		int operations = DND.DROP_MOVE | DND.DROP_COPY;
		Transfer[] transferTypes = { LocalSelectionTransfer.getTransfer() };
		callback.getViewer().addDropSupport(operations, transferTypes, this);
	}

	@Override
	public void drop(DropTargetEvent event) {
		if((event.detail & DND.DROP_COPY) != 0) {
			postDropMode = DropMode.UNION;
		}
		
		QrItem[] nodes = getSelectedNodes();
		if(nodes != null && nodes.length > 0) {
			new DropProcessor(nodes).start();
		} else {
			HawkshawMessage message = new HawkshawMessage();
			
			message.setTitle("Unrecognized Input");
			message.setImage(QueryUI.getDefault().getImage(IHawkshawImages.WARNING));
			message.setDescription("The data that you have dragged into the Inspector is not supported. Only elements originating from the <a href=\"show-results-view\">Query Results</a> view are recognized.");
			callback.addMessage(message);
		}

	}
	
	@Override
	public void dragEnter(DropTargetEvent event) {
		preDragMode = callback.getActiveDropMode();
	}

	@Override
	public void dragOver(DropTargetEvent event) {
		if((event.detail & DND.DROP_COPY) != 0) {
			callback.setDropMode(DropMode.UNION);
		} else {
			callback.setDropMode(preDragMode);
		}
	}

	@Override
	public void dragLeave(DropTargetEvent event) {
		callback.setDropMode(preDragMode);
		preDragMode = null;
	}
	
	private QrItem[] getSelectedNodes() {
		LocalSelectionTransfer transfer = LocalSelectionTransfer.getTransfer();
		ISelection selection = transfer.getSelection();

		Set<QrItem> allNodes = new HashSet<>();
		if(selection instanceof ITreeSelection) {
			ITreeSelection treeSelection = (ITreeSelection) selection;
			Iterator<?> iter = treeSelection.iterator();
			
			while(iter.hasNext()) {
				Object element = iter.next();
				
				if(element instanceof IQrItemContainer) {
					QrItem[] uris = ((IQrItemContainer)element).getItems();
					allNodes.addAll(Arrays.asList(uris));
				}
			}
		}
		
		return allNodes.toArray(new QrItem[allNodes.size()]);
	}

	class DropProcessor extends Thread {
		private QrItem[] nodes;
		
		public DropProcessor(QrItem[] nodes) {
			this.nodes = Arrays.copyOf(nodes, nodes.length);
		}
		
		@Override
		public void run() {
			Set<QrItemWrapper> resources = new HashSet<>();
			
			boolean filtered = false;
			for(QrItem node : nodes) {
				PersistentJenaModel model = node.getModel();
				
				QrItemWrapper rbean = new QrItemWrapper(node);
				
				String nodeUri = node.getUri();
				if(nodeUri != null) {
					rbean.addPropertyValue("#URI", nodeUri); // fake property
					
					if(node.isLiteral()) {
						rbean.setPropertyValues(model.getLiterals(nodeUri));
					} else if(node.isIndividual()) {
						Map<String, Set<Object>> props = model.getLiterals(nodeUri);
						
						for(String propUri : props.keySet()) {
							for(Object value : props.get(propUri)) {
								rbean.addPropertyValue(propUri, value);
							}
						}
					} else {
						rbean.addPropertyValue("#TYPE", nodeUri.substring(nodeUri.lastIndexOf('#') + 1)); // fake property
					}
					resources.add(rbean);
				} else {
					filtered = true;
				}
			}
			
			final Set<QrItemWrapper> finalResources = resources;
			final boolean finalFiltered = filtered;
			Display.getDefault().asyncExec(new Runnable() {
				@Override
				public void run() {
					if(postDropMode != null) {
						callback.setInput(finalResources, postDropMode, finalFiltered);
						postDropMode = null;
					} else {
						callback.setInput(finalResources, finalFiltered);
					}
				}
			});
		}
	}
}