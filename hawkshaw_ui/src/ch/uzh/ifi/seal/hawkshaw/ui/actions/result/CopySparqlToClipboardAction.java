package ch.uzh.ifi.seal.hawkshaw.ui.actions.result;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ui
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.Iterator;

import org.eclipse.jface.action.Action;
import org.eclipse.swt.dnd.Clipboard;
import org.eclipse.swt.dnd.TextTransfer;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.ISharedImages;
import org.eclipse.ui.PlatformUI;

import ch.uzh.ifi.seal.hawkshaw.core.query.Answer;

public class CopySparqlToClipboardAction extends Action {
	private final Answer answer;
	
	public CopySparqlToClipboardAction(Answer answer) {
		super("Copy SPARQL Query to Clipboard");
		this.answer = answer;
		
		setActionDefinitionId("ch.uzh.ifi.seal.hawkshaw.ui.commands.result.copySparqlToClipboard");
		setImageDescriptor(PlatformUI.getWorkbench().getSharedImages().getImageDescriptor(ISharedImages.IMG_TOOL_COPY));
	}
	
	@Override
	public void run() {
		StringBuilder builder = new StringBuilder();
		
		Iterator<String> iter = answer.getFormalQueryStrings().iterator();
		while(iter.hasNext()) {
			builder.append(iter.next());
			
			if(iter.hasNext()) {
				builder.append('\n');
			}
		}
		
		Clipboard cb = new Clipboard(getDisplay());
		cb.setContents(new Object[] { builder.toString() }, new Transfer[] { TextTransfer.getInstance() });
	}
	
	public static Display getDisplay() {
		Display display = Display.getCurrent();
		
		//may be null if outside the UI thread
		if (display == null) {
			display = Display.getDefault();
		}
		
		return display;		
	}
}
