package ch.uzh.ifi.seal.hawkshaw.ui.wizards.queryx;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ui
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;

import ch.uzh.ifi.seal.hawkshaw.ontology.OntologyCore;
import ch.uzh.ifi.seal.hawkshaw.ontology.java.JavaOntologyCore;
import ch.uzh.ifi.seal.hawkshaw.ui.IHawkshawImages;
import ch.uzh.ifi.seal.hawkshaw.ui.QueryUI;

public class JavaProjectLabelProvider extends LabelProvider implements ITableLabelProvider {
	@Override
	public Image getColumnImage(Object element, int columnIndex) {
		if(element instanceof String) { // show dummy icon when loading from tdb
			Image columnImage = null;
			if(columnIndex == 0) {
				columnImage = QueryUI.getDefault().getImage(IHawkshawImages.WAIT);
			}
			
			return columnImage;
		}
		
		IJavaProject javaProject = ((IJavaProject) element);
		
		Image columnImage;
		if(columnIndex == 0) {
			columnImage = PlatformUI.getWorkbench().getSharedImages().getImage(IDE.SharedImages.IMG_OBJ_PROJECT);
		} else if(columnIndex == 1){
			ImageDescriptor descriptor = OntologyCore.modelExists(javaProject.getProject()) ? IHawkshawImages.ACCEPT : IHawkshawImages.CANCEL;
			columnImage =  QueryUI.getDefault().getImage(descriptor);
		} else {
			columnImage = null;
		}
		
		return columnImage;
	}

	@Override
	public String getColumnText(Object element, int columnIndex) {
		if(element instanceof String) { // show dummy text when loading from tdb
			String columnText = "";
			if(columnIndex == 0) {
				columnText = element.toString();
			}
			
			return columnText;
		}
		
		IJavaProject javaProject = ((IJavaProject) element);		
		
		String columnText;
		
		switch(columnIndex) {
		case 0:
			columnText = javaProject.getProject().getName();
			
			break;
		case 1:
			if(JavaOntologyCore.hasNatureEnabled(javaProject)) {
				columnText = "Nature present.";
				
				if(OntologyCore.modelExists(javaProject.getProject())) {
					columnText += " Model available.";
				} else {
					columnText += " Needs to be built.";
				}
			} else {
				columnText ="Needs Nature.";
			}
			
			break;
		case 2:
			if(OntologyCore.modelExists(javaProject.getProject())) {
				columnText = Long.toString(OntologyCore.fetchPersistentModelFor(javaProject.getProject()).size());
			} else {
				columnText = "n/a";
			}
			
			break;
		default:
			columnText = null;
			break;
		}		
		
		return columnText;
	}
}
