package ch.uzh.ifi.seal.hawkshaw.ui.views.favorites;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ui
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import org.eclipse.jface.viewers.CellEditor;
import org.eclipse.jface.viewers.EditingSupport;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TextCellEditor;

public class QueryEditing extends EditingSupport {
	private TextCellEditor cellEditor;
	 
	public QueryEditing(TableViewer viewer) {
		super(viewer);
		cellEditor = new TextCellEditor(viewer.getTable());
	}

	@Override
	protected CellEditor getCellEditor(Object element) {
		return cellEditor;
	}

	@Override
	protected boolean canEdit(Object element) {
		return ((FavoriteQueryItem) element).isEditable();
	}

	@Override
	protected Object getValue(Object element) {
		return ((FavoriteQueryItem) element).getQuestionPhrase();
	}

	@Override
	protected void setValue(Object element, Object value) {
		FavoriteQueryItem item = (FavoriteQueryItem) element;
		item.setQuestionPhrase(value.toString());
		item.setEditable(false);
		
		getViewer().update(element, null);
	}

}
