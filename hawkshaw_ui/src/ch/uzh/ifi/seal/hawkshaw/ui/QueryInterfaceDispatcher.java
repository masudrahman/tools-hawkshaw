package ch.uzh.ifi.seal.hawkshaw.ui;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ui
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.eclipse.core.runtime.IStatus;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.swt.widgets.Display;
import org.eclipse.ui.IViewPart;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.statushandlers.StatusManager;

import ch.uzh.ifi.seal.hawkshaw.core.query.Answer;
import ch.uzh.ifi.seal.hawkshaw.support.exceptions.HawkshawException;
import ch.uzh.ifi.seal.hawkshaw.ui.views.result.QueryResultView;
import ch.uzh.ifi.seal.hawkshaw.ui.views.result.Result;

// TODO Eclipse extension for result listeners? or use eclipse event mechanism?
public enum QueryInterfaceDispatcher {
	INSTANCE;
	
	public void showResult(Answer answer, IJavaProject context) {
		showResult(new Result(answer, context));
	}
	
	public void showResult(final Result result) {
		Display.getDefault().asyncExec(new Runnable() {
			@Override
			public void run() {
				IViewPart resultViewPart = openView(QueryResultView.VIEW_ID);
				QueryResultView resultView = (QueryResultView) resultViewPart;
				resultView.addResult(result);
			}
		});
	}

	/**
	 * Has to be invoked from an ui-thread. Use Display:
	 * 
	 * <pre>
	 * Display.getDefault().asyncExec(
	 *   new Runnable() {
	 *      public void run() {
	 *         Activator.getDefault().openView(MyView.ID);
	 *      }
	 *   }
	 * );
	 * </pre>
	 * 
	 * @param viewID
	 *            the ID of the view as defined in the plug-in contributing the
	 *            view part that should be opened.
	 * @return a reference to the view.
	 * @throws RuntimeException
	 *             if the view can not be initialized correctly. The exceptions
	 *             wraps a {@link PartInitException}.
	 */
	// TODO Check for proper way to communicate between views.
	public IViewPart openView(final String viewID) {
		IWorkbenchPage activePage = findActivePage();
		
		if (activePage != null) {
			try {
				return activePage.showView(viewID);
			} catch (PartInitException e) {
				throw new HawkshawException(QueryUI.getDefault(), "Could not open view with ID " + viewID, e); // TODO own exception
			}
		}

		return null;
	}

	private IWorkbenchPage findActivePage() {
		// null if called from non-ui thread.
		IWorkbenchWindow activeWorkbenchWindow = PlatformUI.getWorkbench().getActiveWorkbenchWindow();

		if (activeWorkbenchWindow == null) {
			return null;
		}
		
		return activeWorkbenchWindow.getActivePage();
	}

	public void showError(IStatus status) {
		StatusManager manager = StatusManager.getManager();
		manager.handle(status, StatusManager.SHOW | StatusManager.LOG | StatusManager.BLOCK);
	}
}
