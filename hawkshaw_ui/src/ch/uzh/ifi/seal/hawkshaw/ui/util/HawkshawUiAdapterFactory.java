package ch.uzh.ifi.seal.hawkshaw.ui.util;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ui
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.Set;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IAdapterFactory;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;

import ch.uzh.ifi.seal.hawkshaw.core.query.Answer;
import ch.uzh.ifi.seal.hawkshaw.ontology.OntologyCore;
import ch.uzh.ifi.seal.hawkshaw.ontology.java.owl.vocabulary.IHawkshawJavaOntology;
import ch.uzh.ifi.seal.hawkshaw.ontology.model.PersistentJenaModel;
import ch.uzh.ifi.seal.hawkshaw.ui.views.result.AnswerTreeItem;
import ch.uzh.ifi.seal.hawkshaw.ui.views.result.QrTreeItem;

public class HawkshawUiAdapterFactory implements IAdapterFactory {

	@Override @SuppressWarnings("rawtypes")
	public Object getAdapter(Object adaptableObject, Class adapterType) {
		Object result = null;
		
		if(adapterType == IJavaElement.class){
			result = adaptToIJavaElement(adaptableObject);
		} else if(adapterType == Answer.class) {
			result = adaptToAnswer(adaptableObject);
		}
		
		return result;
	}

	private Object adaptToAnswer(Object adaptableObject) {
		Object result = null;
		
		if(adaptableObject instanceof AnswerTreeItem) {
			result = ((AnswerTreeItem) adaptableObject).getItem();
		}
		
		return result;
	}

	private Object adaptToIJavaElement(Object adaptableObject) {
		Object result = null;
		if(adaptableObject instanceof QrTreeItem) {
			result = adaptToIJavaElement((QrTreeItem) adaptableObject);
		}
		
		return result;
	}

	private Object adaptToIJavaElement(QrTreeItem node) {
		Object result = null;
		
		
		String uri = node.getUri();
		
		if(uri != null) {
			IProject project = node.getAssociatedJavaProject().getProject();
			PersistentJenaModel model = OntologyCore.fetchPersistentModelFor(project);
			
			Set<String> pathSet = model.getLiterals(uri, IHawkshawJavaOntology.HAS_PATH);
			Set<String> offsetSet = model.getLiterals(uri, IHawkshawJavaOntology.STARTS_AT);
			
			
			for(String path : pathSet) {
				for(String offset : offsetSet) {
					try {
						IJavaElement parentElement = JavaCore.create(path);
						
						if(parentElement instanceof ICompilationUnit) {
							result = ((ICompilationUnit) parentElement).getElementAt(new Long(offset).intValue());
						}
					} catch(JavaModelException jme) {
						result = null;
					}
					
					break; // only one path/offset
				}
				break;
			}
		}
		
		return result;
	}

	@Override @SuppressWarnings("rawtypes")
	public Class[] getAdapterList() {
		return new Class[] { IJavaElement.class, Answer.class };
	}

}
