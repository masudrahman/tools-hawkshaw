package ch.uzh.ifi.seal.hawkshaw.ui.views.favorites;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ui
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.util.HashSet;
import java.util.Set;

import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jface.dialogs.Dialog;
import org.eclipse.jface.dialogs.IDialogConstants;
import org.eclipse.jface.viewers.ComboViewer;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.ModifyEvent;
import org.eclipse.swt.events.ModifyListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Combo;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Group;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Text;

import ch.uzh.ifi.seal.hawkshaw.ontology.misc.QueryValidator;
import ch.uzh.ifi.seal.hawkshaw.ui.IHawkshawImages;
import ch.uzh.ifi.seal.hawkshaw.ui.QueryUI;
import ch.uzh.ifi.seal.hawkshaw.ui.wizards.queryx.JavaProjectProvider;

public class AddQueryDialog extends Dialog {
	private static final int DEFAULT_HEIGHT = 500;
	private static final int DEFAULT_WIDTH = 400;

	private DialogValidator dialogValidator;
	
	private ComboViewer projectComboViewer;
	
	private Text questionTextField;
	private StyledText sparqlQueryText;
	
	private Text errorTextArea;
	
	private Button goToErrorButton;
	private Button prettyPrinterButton;
	
	private QueryValidator queryValidator;
	
	private int errorCarretPos;
	
	private FavoriteQueryItem item;
	
	public AddQueryDialog(FavoriteQueryView parent) {
		super(parent.getSite());
		dialogValidator = new DialogValidator();
		queryValidator = new QueryValidator();
	}
	
	public FavoriteQueryItem getItem() {
		return item;
	}
	
	@Override
	protected void configureShell(Shell shell) {
		super.configureShell(shell);
		shell.setText("Add a Custom Query");
		shell.setSize(DEFAULT_WIDTH,DEFAULT_HEIGHT);
	}
	
	@Override
	protected Control createDialogArea(Composite parent) {
		Composite dialogArea = (Composite) super.createDialogArea(parent);
		Composite queryDialogArea = createQueryDialogArea(dialogArea);
		
		addProjectComboViewerTo(queryDialogArea);
		
		
		Group queryGroup = addQueryGroupTo(queryDialogArea);
		
		addQuestionLabelTo(queryGroup);
		addQuestionTextFieldTo(queryGroup);
		
		addSparqlQueryLabelTo(queryGroup);
		addPrettyPrinterButtonTo(queryGroup);
		addSparqlQueryTextArea(queryGroup);
		
		addErrorTextAreaTo(queryDialogArea);
		addGoToErrorButtonTo(queryDialogArea);
		
		return dialogArea;
	}

	@Override
	protected Control createButtonBar(Composite parent) {
		Control buttonBar = super.createButtonBar(parent);
		
		Button okButton = getButton(IDialogConstants.OK_ID);
		okButton.setEnabled(false);
		
		return buttonBar;
	}

	@Override
	protected void okPressed() {
		Set<String> sparqlQueries = new HashSet<>();
		sparqlQueries.add(getSparqlQueryString());
		item = new FavoriteQueryItem(getQuestionString(), sparqlQueries, getSelectedProject().getHandleIdentifier());
		
		super.okPressed();
	}

	private String getQuestionString() {
		return questionTextField.getText();
	}
	
	private String getSparqlQueryString() {
		return sparqlQueryText.getText();
	}
	
	private IJavaProject getSelectedProject() {
		IJavaProject project = null;
		
		ISelection selection = projectComboViewer.getSelection();
		if(selection instanceof IStructuredSelection) {
			IStructuredSelection structuredSelection = (IStructuredSelection) selection;
			if(!structuredSelection.isEmpty()) {
				project = (IJavaProject) structuredSelection.getFirstElement();
			}
		}
		
		return project;
	}
	
	private void setQueryValid(boolean valid) {
		goToErrorButton.setEnabled(!valid);
		prettyPrinterButton.setEnabled(valid);
	}
	
	private Composite createQueryDialogArea(Composite dialogArea) {
		Composite queryDialogArea = new Composite(dialogArea, SWT.NONE);
		queryDialogArea.setLayoutData(new GridData(GridData.FILL_BOTH));
		queryDialogArea.setLayout(new GridLayout(2, false));
		return queryDialogArea;
	}

	private void addProjectComboViewerTo(Composite dialogArea) {
		Group projectGroup = new Group(dialogArea, SWT.NONE);
		projectGroup.setText("Project:");
		projectGroup.setLayout(new GridLayout());
		
		GridData gridData = new GridData();
		gridData.horizontalSpan = 2;
		gridData.horizontalAlignment = SWT.FILL;
		gridData.verticalAlignment = SWT.TOP;
		gridData.grabExcessHorizontalSpace = true;
		gridData.grabExcessVerticalSpace = false;
		projectGroup.setLayoutData(gridData);
		
		
		Combo combo = new Combo(projectGroup, SWT.READ_ONLY);
		combo.setLayoutData(new GridData(GridData.FILL_BOTH));
		combo.addListener(SWT.Selection, dialogValidator);
		
		projectComboViewer = new ComboViewer(combo);
		projectComboViewer.setContentProvider(new JavaProjectProvider());
		projectComboViewer.setLabelProvider(new SimpleJavaProjectLabelProvider());
		projectComboViewer.setInput(ResourcesPlugin.getWorkspace().getRoot());
	}

	private Group addQueryGroupTo(Composite queryDialogArea) {
		Group queryGroup = new Group(queryDialogArea, SWT.NONE);
		queryGroup.setText("Query:");
		queryGroup.setLayout(new GridLayout(2, false));
		GridData gridData = new GridData();
		gridData.horizontalSpan = 2;
		gridData.horizontalAlignment = SWT.FILL;
		gridData.verticalAlignment = SWT.FILL;
		gridData.grabExcessHorizontalSpace = true;
		gridData.grabExcessVerticalSpace = true;
		queryGroup.setLayoutData(gridData);
		return queryGroup;
	}

	private void addQuestionLabelTo(Composite dialogArea) {
		Label questionLabel = new Label(dialogArea, SWT.NONE);
		questionLabel.setText("Natural Language:");
		
		GridData gridData = new GridData();
		gridData.verticalIndent = 20;
		gridData.horizontalSpan = 2;
		gridData.horizontalAlignment = SWT.FILL;
		gridData.verticalAlignment = SWT.TOP;
		gridData.grabExcessHorizontalSpace = true;
		gridData.grabExcessVerticalSpace = false;
		questionLabel.setLayoutData(gridData);
	}

	private void addPrettyPrinterButtonTo(Composite dialogArea) {
		prettyPrinterButton = new Button(dialogArea, SWT.PUSH);
		prettyPrinterButton.setImage(QueryUI.getDefault().getImage(IHawkshawImages.FORMAT));
		prettyPrinterButton.setToolTipText("Format SPARQL query");
		prettyPrinterButton.setEnabled(false);
		prettyPrinterButton.addListener(SWT.Selection, new PrettyPrinterAction());
		
		GridData gridData = new GridData();
		gridData.verticalIndent = 10;
		gridData.horizontalSpan = 1;
		gridData.horizontalAlignment = SWT.END;
		gridData.verticalAlignment = SWT.TOP;
		gridData.grabExcessHorizontalSpace = false;
		gridData.grabExcessVerticalSpace = false;
		prettyPrinterButton.setLayoutData(gridData);
	}

	private void addQuestionTextFieldTo(Composite dialogArea) {
		questionTextField = new Text(dialogArea, SWT.SINGLE | SWT.BORDER);
		questionTextField.addListener(SWT.Modify, dialogValidator);
		
		GridData gridData = new GridData();
		gridData.horizontalSpan = 2;
		gridData.horizontalAlignment = SWT.FILL;
		gridData.verticalAlignment = SWT.TOP;
		gridData.grabExcessHorizontalSpace = true;
		gridData.grabExcessVerticalSpace = false;
		questionTextField.setLayoutData(gridData);
	}

	private void addSparqlQueryLabelTo(Composite dialogArea) {
		Label sparqlQueryLabel = new Label(dialogArea, SWT.NONE);
		sparqlQueryLabel.setText("SPARQL:");
		
		GridData gridData = new GridData();
		gridData.verticalIndent = 20;
		gridData.horizontalSpan = 1;
		gridData.horizontalAlignment = SWT.FILL;
		gridData.verticalAlignment = SWT.BOTTOM;
		gridData.grabExcessHorizontalSpace = true;
		gridData.grabExcessVerticalSpace = false;
		sparqlQueryLabel.setLayoutData(gridData);
	}

	private void addSparqlQueryTextArea(Composite dialogArea) {
		sparqlQueryText = new StyledText(dialogArea,SWT.MULTI | SWT.BORDER | SWT.V_SCROLL | SWT.H_SCROLL);
		sparqlQueryText.addModifyListener(new QuerySyntaxChecker());
		sparqlQueryText.addListener(SWT.Modify, dialogValidator);
		
		GridData gridData = new GridData();
		gridData.horizontalSpan = 2;
		gridData.verticalSpan = 4;
		gridData.horizontalAlignment = SWT.FILL;
		gridData.verticalAlignment = SWT.FILL;
		gridData.grabExcessHorizontalSpace = true;
		gridData.grabExcessVerticalSpace = true;
		sparqlQueryText.setLayoutData(gridData);
	}

	private void addErrorTextAreaTo(Composite dialogArea) {
		errorTextArea = new Text(dialogArea, SWT.BORDER | SWT.WRAP | SWT.READ_ONLY | SWT.V_SCROLL);
		errorTextArea.setText("No syntax errors found.");
		
		GridData gridData = new GridData();
		gridData.verticalIndent = 10;
		gridData.horizontalSpan = 1;
		gridData.verticalSpan = 2;
		gridData.horizontalAlignment = SWT.FILL;
		gridData.verticalAlignment = SWT.FILL;
		gridData.grabExcessHorizontalSpace = true;
		gridData.grabExcessVerticalSpace = true;
		errorTextArea.setLayoutData(gridData);
	}

	private void addGoToErrorButtonTo(Composite dialogArea) {
		goToErrorButton = new Button(dialogArea, SWT.PUSH);
		goToErrorButton.setImage(QueryUI.getDefault().getImage(IHawkshawImages.GOTO_ERROR));
		goToErrorButton.setToolTipText("Place Carret at Syntax Error");
		goToErrorButton.setEnabled(false);
		goToErrorButton.addListener(SWT.Selection, new GoToErrorAction());
		
		GridData gridData = new GridData();
		gridData.verticalIndent = 10;
		gridData.horizontalSpan = 1;
		gridData.horizontalAlignment = SWT.END;
		gridData.verticalAlignment = SWT.TOP;
		gridData.grabExcessHorizontalSpace = false;
		gridData.grabExcessVerticalSpace = false;
		goToErrorButton.setLayoutData(gridData);
	}

	private final class PrettyPrinterAction implements Listener {
		@Override
		public void handleEvent(Event event) {
			if(queryValidator.validates()) {
				sparqlQueryText.setText(queryValidator.format());
			}
		}
	}

	private final class GoToErrorAction implements Listener {
		@Override
		public void handleEvent(Event event) {
			sparqlQueryText.setFocus();
			sparqlQueryText.setCaretOffset(errorCarretPos);
		}
	}
	
	private final class DialogValidator implements Listener {
		@Override
		public void handleEvent(Event event) {
			ISelection selection = projectComboViewer.getSelection();
			boolean projectSelected = !selection.isEmpty();
			
			String question = questionTextField.getText();
			boolean questionEntered = question.length() > 0 && question.endsWith("?");
			
			boolean sparqlQueryEntered = queryValidator.validates();
			
			boolean dialogValidates = projectSelected && questionEntered && sparqlQueryEntered;
			Button okButton = getButton(IDialogConstants.OK_ID);
			okButton.setEnabled(dialogValidates);
		}
	}
	
	private final class QuerySyntaxChecker implements ModifyListener {
		@Override
		public void modifyText(ModifyEvent event) {
			queryValidator.setQueryString(getSparqlQueryString());
			
			if(queryValidator.validates()) {
				errorTextArea.setText("No syntax errors found.");
				
				setQueryValid(true);
			} else {
				String errorMsg = queryValidator.getErrorMessage();
				int errorLine = queryValidator.getErrorLine();
				int errorColumn = queryValidator.getErrorColumn();
				
				String faultyQuery = getSparqlQueryString();
				
				int lineCounter = 1;
				int carretPos = 0;
				for(int i = 0; i < faultyQuery.length(); i++) {
					if(faultyQuery.charAt(i) == '\n') {
						lineCounter++;
					}
					
					if(lineCounter >= errorLine) {
						carretPos = i + errorColumn; break;
					}
				}
				
				errorTextArea.setText(errorMsg);
				errorCarretPos = carretPos;
				
				setQueryValid(false);
			}
		}
	}
}
