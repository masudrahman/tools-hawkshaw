package ch.uzh.ifi.seal.hawkshaw.ui.util;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ui
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.viewers.IContentProvider;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.IViewSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.part.ViewPart;

import ch.uzh.ifi.seal.hawkshaw.ui.messages.HawkshawMessage;
import ch.uzh.ifi.seal.hawkshaw.ui.messages.HawkshawMessageControl;

// TODO should go into a general plug-in, e.g. core.util.ui
public abstract class TableView extends ViewPart {	
	private SimpleTreeViewer viewer;
	
	private HawkshawMessageControl messageControl;
	
	private Label statusLabel;
	
	private IMemento memento;

	private int viewerStyle;
	
	public TableView(int viewerStyle) {
		this.viewerStyle = viewerStyle;
	}
	
	@Override
	public void init(IViewSite site, IMemento memento) throws PartInitException {
		super.init(site, memento);
		this.memento = memento;
	}
	
	@Override
	public void createPartControl(Composite parent) {
		GridLayout layout = new GridLayout(1, false);
		layout.marginWidth = 0;
		layout.marginHeight = 0;
		layout.horizontalSpacing = 0;
		layout.verticalSpacing = 0;
		parent.setLayout(layout);
		
		statusLabel = new Label(parent, SWT.NONE);
		statusLabel.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, false));
		
		viewer = new SimpleTreeViewer(parent, viewerStyle, getAllFields());
		viewer.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));
		viewer.init(memento); // restores column order and width
		
		viewer.setContentProvider(getContentProvider());
		
		// selections
		viewer.addSelectionChangedListener(new ISelectionChangedListener() {
			public void selectionChanged(SelectionChangedEvent event) {
				IStructuredSelection selection = (IStructuredSelection) event.getSelection();
				viewerSelectionChanged(selection);
			}
		});
		
		//context menu
		MenuManager mgr = initContextMenu();
		Menu menu = mgr.createContextMenu(viewer.getControl());
		viewer.getControl().setMenu(menu);
		
		
		IActionBars actionBars = getViewSite().getActionBars();
		initMenu(actionBars.getMenuManager());
		initToolBar(actionBars.getToolBarManager());
		
		viewer.setInput(createViewerInput());
		
		getSite().setSelectionProvider(viewer); // TODO Check this: http://www.eclipse.org/articles/Article-WorkbenchSelections/article.html and http://www.ibm.com/developerworks/opensource/library/os-ecllink/
	
		messageControl = new HawkshawMessageControl(parent);
	}
	
	public void enableSelectionDrag() {
		viewer.enableSelectionDrag();
	}

	@Override
	public void saveState(IMemento memento) {
		super.saveState(memento);
		viewer.saveState(memento);
	}
	
	public Object getViewerInput() {
		return getViewer().getInput();
	}

	@Override
	public void setFocus() {
		viewer.setFocus();
	}
	
	protected void setMessage(HawkshawMessage message) {
		messageControl.setMessage(message);
	}
	
	protected void closeMessage() {
		messageControl.closeMessage();
	}
	
	protected abstract Object createViewerInput();
	
	protected abstract void initMenu(IMenuManager menu);
	
	protected abstract void initToolBar(IToolBarManager toolBarManager);
	
	protected abstract void fillContextMenu(IMenuManager manager);
	
	protected abstract void viewerSelectionChanged(IStructuredSelection selection);
	
	protected abstract IField[] getAllFields();
	
	protected abstract IContentProvider getContentProvider();
	
	protected void updateStatusLabelWith(String text) {
		statusLabel.setText(text);
	}
	
	protected void setInput(Object input) {
		viewer.setInput(input);
	}
	
	protected SimpleTreeViewer getViewer() {
		return viewer;
	}
	
	private MenuManager initContextMenu() {
		MenuManager mgr = new MenuManager();
		mgr.setRemoveAllWhenShown(true);
		mgr.addMenuListener(new IMenuListener() {
			public void menuAboutToShow(IMenuManager mgr) {
				fillContextMenu(mgr);
			}
		});
		
		return mgr;
	}
}