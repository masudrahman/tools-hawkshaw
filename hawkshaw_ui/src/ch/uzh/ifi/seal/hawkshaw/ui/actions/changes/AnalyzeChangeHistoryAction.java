package ch.uzh.ifi.seal.hawkshaw.ui.actions.changes;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ui
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.Iterator;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.IJobChangeEvent;
import org.eclipse.core.runtime.jobs.JobChangeAdapter;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IObjectActionDelegate;
import org.eclipse.ui.IWorkbenchPart;

import ch.uzh.ifi.seal.hawkshaw.ontology.changes.importer.ChangeImporterJob;
import ch.uzh.ifi.seal.hawkshaw.ui.IHawkshawImages;
import ch.uzh.ifi.seal.hawkshaw.ui.QueryUI;
import ch.uzh.ifi.seal.hawkshaw.ui.messages.HawkshawNotificationCenter;

public class AnalyzeChangeHistoryAction implements IObjectActionDelegate {
	private ISelection selection;
	@Override
	public void run(IAction action) {
		if (selection instanceof IStructuredSelection) {
			for (Iterator<?> it = ((IStructuredSelection) selection).iterator(); it.hasNext();) {
				Object element = it.next();
				IProject project = null;
				if (element instanceof IProject) {
					project = (IProject) element;
				} else if (element instanceof IAdaptable) {
					project = (IProject) ((IAdaptable) element)
							.getAdapter(IProject.class);
				}
				if (project != null) {
					analyze(project);
				}
			}
		}
	}

	private void analyze(IProject project) {
		ChangeImporterJob importer = new ChangeImporterJob(project);
		importer.addJobChangeListener(new ChangeHistoryImporterJobListener());
		importer.schedule();
	}

	@Override
	public void selectionChanged(IAction action, ISelection selection) {
		this.selection = selection;
	}

	@Override
	public void setActivePart(IAction action, IWorkbenchPart targetPart) {
		// TODO Auto-generated method stub

	}
	
	private static final class ChangeHistoryImporterJobListener extends JobChangeAdapter {
		@Override
		public void done(IJobChangeEvent event) {
			ChangeImporterJob job = (ChangeImporterJob) event.getJob();
			IStatus status = event.getResult();
			int severity = status.getSeverity();
			
			String title;
			String notification;
			if(severity == Status.OK) {
				title = "Change History of " + job.getProjectName() + " analyzed";
				
				notification = "Hawkshaw parsed " + job.getNumberOfAnalyzedRevisions() + " " +
						"revisions in " + job.getDurationString() + ". " +
						"A total of " + job.getNumberOfAnalyzedChanges() + " changes were found. " +
						"Facts about the change history can now be queried.";
			} else if(severity == Status.CANCEL) {
					title = "History of " + job.getProjectName() + " incomplete";
					notification = "Analysis canceled by user after " + job.getNumberOfAnalyzedRevisions() + " " +
							"revisions and " + job.getDurationString() + ". " +
							"A total of " + job.getNumberOfAnalyzedChanges() + " changes were found. " +
							"Some Facts about the change history can still be queried and analysis can be restarted at any time.";
			} else { // severity == Status.ERROR
				title = "History of " + job.getProjectName() + " unavailable";
				notification = status.getMessage();
			}
			
			HawkshawNotificationCenter.INSTANCE.notifyUser(title,
														   notification,
														   QueryUI.getDefault().getImage(IHawkshawImages.HISTORY));
		}
	}
}