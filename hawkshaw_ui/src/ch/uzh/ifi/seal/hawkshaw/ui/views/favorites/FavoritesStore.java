package ch.uzh.ifi.seal.hawkshaw.ui.views.favorites;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ui
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.ui.IMemento;

import ch.uzh.ifi.seal.hawkshaw.core.query.Answer;

public class FavoritesStore {
	private static final String KEY_FAVORITE = "favorite";
	private static final String KEY_PROJECT_HANDLE = "project-handle";
	private static final String KEY_SPARQL_QUERY = "sparql-query";
	private static final String KEY_SPARQL_QUERIES = "sparql-queries";
	private static final String KEY_NL_QUERY = "nl-query";
	
	private List<FavoriteQueryItem> favorites;
	
	public FavoritesStore() {
		favorites = new LinkedList<>();
	}
	
	public void add(FavoriteQueryItem favorite) {
		favorites.add(favorite);
	}
	
	public void add(Answer answer, IJavaProject project) {
		add(new FavoriteQueryItem(answer, project));
	}
	
	public void remove(FavoriteQueryItem favorite) {
		favorites.remove(favorite);
	}
	
	public void save(IMemento memento) {
		if(memento != null) {
			for(FavoriteQueryItem favorite : favorites) {
				IMemento favMemento =  memento.createChild(KEY_FAVORITE);
				favMemento.putString(KEY_NL_QUERY, favorite.getQuestionPhrase());
				
				saveSparqlQueries(favorite, favMemento);
				
				favMemento.putString(KEY_PROJECT_HANDLE, favorite.getProjectHandle());
			}
		}
	}

	public void load(IMemento memento) {
		if(memento != null) {
			IMemento[] favMementos = memento.getChildren(KEY_FAVORITE);
			for(IMemento favMemento : favMementos) {
				String nlQuery = favMemento.getString(KEY_NL_QUERY);
				
				Set<String> sparqlQueries = loadSparqlQueries(favMemento);
				
				String projectHandle = favMemento.getString(KEY_PROJECT_HANDLE);
				
				add(new FavoriteQueryItem(nlQuery, sparqlQueries, projectHandle));
			}
		}
	}
	
	private void saveSparqlQueries(FavoriteQueryItem favorite, IMemento favMemento) {
		for(String sparql : favorite.getSparqlQueries()) {
			IMemento sparqlMemento = favMemento.createChild(KEY_SPARQL_QUERIES);
			sparqlMemento.putString(KEY_SPARQL_QUERY, sparql);
		}
	}
	
	private Set<String> loadSparqlQueries(IMemento favMemento) {
		IMemento[] sparqlMementos = favMemento.getChildren(KEY_SPARQL_QUERIES);
		Set<String> sparqlQueries = new HashSet<>();
		for(IMemento sparqlMemento : sparqlMementos) {
			sparqlQueries.add(sparqlMemento.getString(KEY_SPARQL_QUERY));
		}
		return sparqlQueries;
	}
	
	public FavoriteQueryItem[] toArray() {
		return favorites.toArray(new FavoriteQueryItem[favorites.size()]);
	}
}
