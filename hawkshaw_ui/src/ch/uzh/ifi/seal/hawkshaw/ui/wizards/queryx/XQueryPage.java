package ch.uzh.ifi.seal.hawkshaw.ui.wizards.queryx;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ui
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.lang.reflect.InvocationTargetException;

import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jface.operation.IRunnableWithProgress;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import ch.uzh.ifi.seal.hawkshaw.core.query.ISessionListener;
import ch.uzh.ifi.seal.hawkshaw.core.query.Question;
import ch.uzh.ifi.seal.hawkshaw.core.query.QuestionStatus;
import ch.uzh.ifi.seal.hawkshaw.core.query.Session;
import ch.uzh.ifi.seal.hawkshaw.support.exceptions.HawkshawException;
import ch.uzh.ifi.seal.hawkshaw.ui.IHawkshawImages;
import ch.uzh.ifi.seal.hawkshaw.ui.QueryUI;
import ch.uzh.ifi.seal.hawkshaw.ui.selections.IResourceSelection;
import ch.uzh.ifi.seal.hawkshaw.ui.util.SessionWrapper;

public class XQueryPage extends AbstractXQueryPage implements ISessionListener {
	public static final String PAGE_ID = "ch.uzh.ifi.seal.hawkshaw.ui.wizards.query.XQueryWizard.XQueryPage";
	
	private XContentProposalAdapter contentProposalAdapter;
	
	private Text queryTextField;
	
	private Label labelForNrActivePaths;
	private Label labelForNrResolvedPaths;

	private IJavaProject javaProject;

	private SessionWrapper session;
	
	XQueryPage() {
		super(XQueryPage.PAGE_ID, "Please enter your Question", IHawkshawImages.INSP_WIZ);
		setMessage("Start typing and then use the proposals to further compose a query.");
	}
	
	public void init(IResourceSelection selection) {
		this.javaProject = selection.getJavaProject();
		this.session = QueryUI.startSession(javaProject.getProject());
		session.register(this);
		
		String uri = selection.toUriString();
		String name = selection.getName();
		if(uri != null) {
			session.setActiveSelection(name, uri);
		}
		
//		IJavaElementSelection selection = QueryUI.getCurrentJavaElementSelection();
//		session.setActiveSelection(selection.toUriString());
		
		contentProposalAdapter = new XContentProposalAdapter(queryTextField, new ProposalHandler(queryTextField, session));
	}

	@Override
	public void createControl(Composite parent) {
		setPageComplete(false);
		
		Composite composite = new Composite(parent, SWT.NONE);
		GridLayout layout = new GridLayout();
		composite.setLayout(layout);
		
		addQueryTextFieldTo(composite);
		addDebugAreaTo(composite);
		
		setControl(composite);
	}
	
	@Override
	public void setVisible(boolean visible) {
		super.setVisible(visible);
		
		contentProposalAdapter.activate();
	}
	
	@Override
	public boolean hasNextPage() {
		return false;
	}
	
	@Override
	public void sessionChanged(Session session) {
		Question question = session.getActiveQuestion();
		if(question != null) {
			setDebugInfo(question.getStatus());
			setPageComplete(question.isComplete());
		}		
	}

	@Override
	public void finish() {
		IRunnableWithProgress queryRunner = new XQueryRunner(session.getActiveQuestion(), javaProject);
		try {
			getContainer().run(true, false, queryRunner);
		} catch (InvocationTargetException e) {
			throw new HawkshawException(QueryUI.getDefault(), "Unexpected error while executing query.", e);
		} catch (InterruptedException e) {
			// should not happen
			throw new HawkshawException(QueryUI.getDefault(), "Interrupted while executing query.", e);
		}
	}
	
	@Override
	public void dispose() {
		if(session != null) {
			session.unsetActiveSelection();
		}
		super.dispose();
	}

	private void addDebugAreaTo(Composite parent) {
		// TODO preferences: show or hide debug area
		
		Composite debugArea = new Composite(parent, SWT.NONE);
		
		GridData gridData = new GridData();
		gridData.horizontalAlignment = SWT.FILL;
		gridData.verticalAlignment = SWT.FILL;
		gridData.grabExcessHorizontalSpace = true;
		gridData.grabExcessVerticalSpace = true;
		debugArea.setLayoutData(gridData);
		
		GridLayout layout = new GridLayout(2, false);
		debugArea.setLayout(layout);
		
		addNrActivePathsLabelTo(debugArea);
		addNrResolvedPathsLabelTo(debugArea);
	}
	
	private void addNrActivePathsLabelTo(Composite debugArea) {
		labelForNrActivePaths = new Label(debugArea, SWT.NONE);
		labelForNrActivePaths.setText("Active paths: 0");
		
		GridData gridData = new GridData();
		gridData.minimumWidth = 150;
		gridData.grabExcessHorizontalSpace = true;
		labelForNrActivePaths.setLayoutData(gridData);
	}
	
	private void addNrResolvedPathsLabelTo(Composite debugArea) {
		labelForNrResolvedPaths = new Label(debugArea, SWT.NONE);
		labelForNrResolvedPaths.setText("Resolved Paths: 0");
		
		GridData gridData = new GridData();
		gridData.minimumWidth = 150;
		gridData.grabExcessHorizontalSpace = true;
		labelForNrResolvedPaths.setLayoutData(gridData);
	}
	
	private void addQueryTextFieldTo(Composite parent) {
			queryTextField = new Text(parent, SWT.SINGLE | SWT.SEARCH | SWT.ICON_CANCEL | SWT.ICON_SEARCH);
			queryTextField.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
	}
	
	private void setDebugInfo(QuestionStatus status) {
		labelForNrActivePaths.setText("Active Paths: " + status.getNrOfActivePaths());
		labelForNrResolvedPaths.setText("Resolved Paths: " + status.getNrOfResolvedPaths());
	}
}
