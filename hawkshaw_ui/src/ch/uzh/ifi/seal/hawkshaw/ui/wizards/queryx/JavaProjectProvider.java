package ch.uzh.ifi.seal.hawkshaw.ui.wizards.queryx;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ui
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IWorkspaceRoot;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.Viewer;

import ch.uzh.ifi.seal.hawkshaw.support.exceptions.HawkshawException;
import ch.uzh.ifi.seal.hawkshaw.ui.QueryUI;

public class JavaProjectProvider implements IStructuredContentProvider {

	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		// do nothing
	}

	@Override
	public Object[] getElements(Object inputElement) {
		List<IJavaProject> javaProjects = new ArrayList<>();
		
		if(inputElement instanceof IWorkspaceRoot) {
			IProject[] projects = ((IWorkspaceRoot) inputElement).getProjects();
			for(IProject project : projects) {
				try {
					if(project.isOpen() && project.isNatureEnabled(JavaCore.NATURE_ID)) {
						IJavaProject javaProject = JavaCore.create(project);
						javaProjects.add(javaProject);
					}
				} catch(CoreException cex) {
					// Should not happen
					throw new HawkshawException(QueryUI.getDefault(), "Unexpected error while accessing workspace projects.", cex); // TODO own exception
				}
			}
		} 
		
		return javaProjects.toArray();
	}

	@Override
	public void dispose() {
		// do nothing
	}
}
