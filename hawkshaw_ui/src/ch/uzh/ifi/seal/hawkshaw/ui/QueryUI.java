package ch.uzh.ifi.seal.hawkshaw.ui;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ui
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.net.URL;

import org.eclipse.core.resources.IProject;
import org.eclipse.jdt.ui.JavaUI;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.eclipse.ui.progress.IProgressService;
import org.osgi.framework.BundleContext;

import ch.uzh.ifi.seal.hawkshaw.core.QueryCore;
import ch.uzh.ifi.seal.hawkshaw.ontology.changes.importer.ChangeImporterJob;
import ch.uzh.ifi.seal.hawkshaw.ontology.issue.importer.JiraImporterJob;
import ch.uzh.ifi.seal.hawkshaw.ontology.team.importer.HistoryImporterJob;
import ch.uzh.ifi.seal.hawkshaw.support.io.ResourceLocator;
import ch.uzh.ifi.seal.hawkshaw.support.plugin.IHawkshawPlugin;
import ch.uzh.ifi.seal.hawkshaw.ui.util.SessionWrapper;

/**
 * The activator class controls the plug-in life cycle
 */
public class QueryUI extends AbstractUIPlugin implements IHawkshawPlugin {
	
	// The plug-in ID
	public static final String PLUGIN_ID = "ch.uzh.ifi.seal.hawkshaw.ui"; //$NON-NLS-1$

	public static final String EXTENSION_ID_EXPERIMENT = "ch.uzh.ifi.seal.hawkshaw.experiment";

	// The shared instance
	private static QueryUI plugin;
	
	// The image cache
	private ImageCache imageCache;
	
	public QueryUI() {
		imageCache = new ImageCache();
	}
	
	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
		
		registerIconsForJobs();
	}

	private void registerIconsForJobs() {
		IWorkbench wb = PlatformUI.getWorkbench();
		IProgressService ps = wb.getProgressService();

		ps.registerIconForFamily(IHawkshawImages.ISSUE, JiraImporterJob.ISSUE_JOB_FAMILY);
		ps.registerIconForFamily(IHawkshawImages.HISTORY, HistoryImporterJob.HISTORY_JOB_FAMILY);
		ps.registerIconForFamily(IHawkshawImages.TREE, ChangeImporterJob.CHANGE_JOB_FAMILY);
	}

	/*
	 * (non-Javadoc)
	 * @see org.eclipse.ui.plugin.AbstractUIPlugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		imageCache.dispose();
		
		plugin = null;
		
		super.stop(context);
	}
	
	public Image getImage(ImageDescriptor descriptor) {
		return imageCache.get(descriptor);
	}
	
	public ImageDescriptor getImageDescriptor(String localPath) {
		URL url = ResourceLocator.resolveUrl(this, localPath);
		return ImageDescriptor.createFromURL(url);
	}

	@Override
	public String getID() {
		return PLUGIN_ID;
	}
	
	public static SessionWrapper startSession(IProject project) {
		return new SessionWrapper(QueryCore.startSession(project));
	}

	/**
	 * Returns the shared instance
	 *
	 * @return the shared instance
	 */
	public static QueryUI getDefault() {
		return plugin;
	}

	public static boolean isJavaEditor(IWorkbenchPart part) {
		String partId = part.getSite().getId();
		return partId.equals(JavaUI.ID_CU_EDITOR) || partId.equals(JavaUI.ID_CF_EDITOR);
	}
}
