package ch.uzh.ifi.seal.hawkshaw.ui.views.inspector;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ui
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.viewers.Viewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;

import ch.uzh.ifi.seal.hawkshaw.ontology.vocabulary.IRDFS;

public class InspectorTableContentProvider implements IStructuredContentProvider {
	private static final int DEFAULT_WIDTH = 150;
	
	private QrItemWrapper[] input;
	
	@Override
	public void dispose() {
		input = null;
	}

	@Override
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		if(viewer instanceof TableViewer && newInput instanceof InspectorInput) {
			input = ((InspectorInput) newInput).applyDropMode(input);
			reconfigure((TableViewer)viewer);
		}
	}

	@Override
	public Object[] getElements(Object inputElement) {
		return Arrays.copyOf(input, input.length);
	}

	private void reconfigure(TableViewer viewer) {
		Table table = viewer.getTable();
		
		Set<String> newTableProps = retainAll();
		
		Set<String> oldTableProps = new HashSet<>();
		for(TableColumn tableColumn : table.getColumns()) {
			String columnPropUri = (String) tableColumn.getData("propUri");
			if(!newTableProps.contains(columnPropUri)) {
				tableColumn.dispose();
			} else {
				oldTableProps.add(columnPropUri);
			}
		}
		
		newTableProps.removeAll(oldTableProps); // no need to add columns for existing props.
		
		// Labels, local type names, and uris should always be the first columns and have fixed width - everything else proved to be confusing (even to me)		
		fixColumns(viewer, newTableProps, IRDFS.LABEL, "#TYPE", "#URI");
		
		// Other columns can be re-arranged freely
		for(String commonProp : newTableProps) {
			addColumn(viewer, commonProp, true);
		}
	}
	
	private void fixColumns(TableViewer viewer, Set<String> input, String... propUris) {
		for(String propUri : propUris) {
			boolean hasUri = input.remove(propUri);
			if(hasUri) {
				addColumn(viewer, propUri, false);
			}
		}
	}

	private void addColumn(TableViewer viewer, String commonProp, boolean moveable) {
		TableViewerColumn viewerColumn = new TableViewerColumn(viewer, SWT.NONE);
		TableColumn tableColumn = viewerColumn.getColumn();
		
		tableColumn.setData("propUri", commonProp);
		tableColumn.setText(commonProp.substring(commonProp.lastIndexOf('#') + 1));
		tableColumn.setToolTipText(commonProp);
		tableColumn.setWidth(DEFAULT_WIDTH);
		tableColumn.setData("restoredWidth", Integer.valueOf(DEFAULT_WIDTH));
		tableColumn.setMoveable(moveable);
		
		tableColumn.addSelectionListener(new ColumnSortOnSelection(viewerColumn));
		
		viewerColumn.setLabelProvider(new InspectorColumnLabelProvider(viewerColumn));
	}

	private Set<String> retainAll() {
		Set<String> globalProps = new TreeSet<>();
		for(QrItemWrapper bean : input) {
			Set<String> localProps = bean.getProperties();
			
			if(globalProps.size() < 1) {
				globalProps.addAll(localProps);
			} else {
				globalProps.retainAll(localProps);
			}
		}
		
		return globalProps;
	}
}
