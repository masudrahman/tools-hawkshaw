package ch.uzh.ifi.seal.hawkshaw.ui;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ui
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.eclipse.jface.resource.ImageDescriptor;

public interface IHawkshawImages {
	ImageDescriptor ACCEPT               = QueryUI.getDefault().getImageDescriptor("images/icons/famfamfam_silk/accept.png");
	ImageDescriptor CANCEL               = QueryUI.getDefault().getImageDescriptor("images/icons/famfamfam_silk/cancel.png");
	
	ImageDescriptor EDIT                 = QueryUI.getDefault().getImageDescriptor("images/icons/famfamfam_silk/pencil.png");
	ImageDescriptor EDIT_ADD             = QueryUI.getDefault().getImageDescriptor("images/icons/famfamfam_silk/pencil_add.png");
	ImageDescriptor EDIT_DEL             = QueryUI.getDefault().getImageDescriptor("images/icons/famfamfam_silk/pencil_delete.png");
	
	ImageDescriptor MESSAGE_CLOSE        = QueryUI.getDefault().getImageDescriptor("images/icons/misc/notification-close.gif");
	ImageDescriptor MESSAGE_CLOSE_ACTIVE = QueryUI.getDefault().getImageDescriptor("images/icons/misc/notification-close-active.gif");
	
	ImageDescriptor GOTO_ERROR           = QueryUI.getDefault().getImageDescriptor("images/icons/famfamfam_silk/bug_go.png");
	ImageDescriptor WAIT                 = QueryUI.getDefault().getImageDescriptor("images/icons/famfamfam_silk/hourglass.png");
	
	ImageDescriptor FORMAT               = QueryUI.getDefault().getImageDescriptor("images/icons/famfamfam_silk/page_lightning.png");
	ImageDescriptor SEARCH               = QueryUI.getDefault().getImageDescriptor("images/icons/misc/flashlight_shine.png");
	ImageDescriptor FAVS_TABLE           = QueryUI.getDefault().getImageDescriptor("images/icons/misc/table_star.png");
	ImageDescriptor FAVS                 = QueryUI.getDefault().getImageDescriptor("images/icons/famfamfam_silk/star.png");
	
	ImageDescriptor FIND                 = QueryUI.getDefault().getImageDescriptor("images/icons/misc/find.png");
	ImageDescriptor ANSWER               = QueryUI.getDefault().getImageDescriptor("images/icons/misc/help-and-support-icon.png");
	
	ImageDescriptor INTERSECTION         = QueryUI.getDefault().getImageDescriptor("images/icons/misc/intersection.png");
	ImageDescriptor UNION                = QueryUI.getDefault().getImageDescriptor("images/icons/misc/union.png");
	ImageDescriptor DIFFERENCE           = QueryUI.getDefault().getImageDescriptor("images/icons/misc/difference.png");
	ImageDescriptor SET                  = QueryUI.getDefault().getImageDescriptor("images/icons/misc/table-replace-icon.png");
	
	ImageDescriptor INSP_WIZ             = QueryUI.getDefault().getImageDescriptor("images/wizards/search2_65x65_wiz.png");
	ImageDescriptor HAWK_WIZ             = QueryUI.getDefault().getImageDescriptor("images/wizards/eagle_65x65_wiz.png");
	ImageDescriptor RDF_WIZ              = QueryUI.getDefault().getImageDescriptor("images/wizards/rdf_65x65_wiz.png");
	ImageDescriptor ISSUE_WIZ            = QueryUI.getDefault().getImageDescriptor("images/wizards/issue_65x65_wiz.png");
	ImageDescriptor RDF                  = QueryUI.getDefault().getImageDescriptor("images/icons/rdf/rdf_16x16.png");
	ImageDescriptor RDF_TINY             = QueryUI.getDefault().getImageDescriptor("images/icons/rdf/rdf_gen_16x16.png");

	ImageDescriptor INSP_ICON            = QueryUI.getDefault().getImageDescriptor("images/icons/misc/inspector_16x16.png");
	ImageDescriptor HAWK_ICON            = QueryUI.getDefault().getImageDescriptor("images/icons/eagle/eagle-icon_16x16.png");
	
	ImageDescriptor APP_GO               = QueryUI.getDefault().getImageDescriptor("images/icons/famfamfam_silk/application_side_expand.png");
	ImageDescriptor REPORT               = QueryUI.getDefault().getImageDescriptor("images/icons/famfamfam_silk/report_magnify.png");
	ImageDescriptor GEAR                 = QueryUI.getDefault().getImageDescriptor("images/icons/famfamfam_silk/cog.png");
	ImageDescriptor DB_GEAR              = QueryUI.getDefault().getImageDescriptor("images/icons/famfamfam_silk/database_gear.png");
	ImageDescriptor DB_LIGHTNING         = QueryUI.getDefault().getImageDescriptor("images/icons/famfamfam_silk/database_lightning.png");
	ImageDescriptor WARNING              = QueryUI.getDefault().getImageDescriptor("images/icons/famfamfam_silk/error.png");
	ImageDescriptor FILTER_WARNING       = QueryUI.getDefault().getImageDescriptor("images/icons/misc/funnel-exclamation-icon.png");
	
	ImageDescriptor REMOVE_ALL           = QueryUI.getDefault().getImageDescriptor("images/icons/misc/progress_remall.gif");
	ImageDescriptor REMOVE               = QueryUI.getDefault().getImageDescriptor("images/icons/misc/progress_rem.gif");
	
	ImageDescriptor MULTI_FILE           = QueryUI.getDefault().getImageDescriptor("images/icons/misc/copy_edit.gif");
	ImageDescriptor ISSUE                = QueryUI.getDefault().getImageDescriptor("images/icons/famfamfam_silk/bug.png");
	ImageDescriptor DEVELOPER            = QueryUI.getDefault().getImageDescriptor("images/icons/misc/developer.png");
	ImageDescriptor STAKEHOLDER          = QueryUI.getDefault().getImageDescriptor("images/icons/misc/stakeholder.png");
	ImageDescriptor REPORTER             = QueryUI.getDefault().getImageDescriptor("images/icons/misc/reporter.png");
	ImageDescriptor COMMENT              = QueryUI.getDefault().getImageDescriptor("images/icons/misc/comment-icon.png");
	ImageDescriptor HISTORY              = QueryUI.getDefault().getImageDescriptor("images/icons/misc/history-icon.png");
	
	ImageDescriptor PLAY                 = QueryUI.getDefault().getImageDescriptor("images/icons/famfamfam_silk/control_play_blue.png");
	ImageDescriptor PAUSE                = QueryUI.getDefault().getImageDescriptor("images/icons/famfamfam_silk/control_pause_blue.png");
	ImageDescriptor RESET                = QueryUI.getDefault().getImageDescriptor("images/icons/famfamfam_silk/control_repeat_blue.png");
	ImageDescriptor NEXT                 = QueryUI.getDefault().getImageDescriptor("images/icons/famfamfam_silk/control_end_blue.png");
	
	ImageDescriptor URL_GO               = QueryUI.getDefault().getImageDescriptor("images/icons/famfamfam_silk/world_go.png");
	ImageDescriptor TREE                 = QueryUI.getDefault().getImageDescriptor("images/icons/misc/tree_diagramm_move.png");
}
