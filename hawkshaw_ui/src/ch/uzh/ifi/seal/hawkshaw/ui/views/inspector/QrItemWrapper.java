package ch.uzh.ifi.seal.hawkshaw.ui.views.inspector;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ui
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import ch.uzh.ifi.seal.hawkshaw.ontology.model.QrItem;

public class QrItemWrapper {
	private QrItem item;
	private Map<String, Set<Object>> propertyValueMap;
	
	public QrItemWrapper(QrItem item) {
		this.item = item;
		propertyValueMap = new HashMap<>();
	}
	
	public void setPropertyValues(Map<String, Set<Object>> propertyValueMap) {
		this.propertyValueMap.putAll(propertyValueMap);
	}

	public QrItem getItem() {
		return item;
	}
	
	public void addPropertyValue(String prop, Object value) {
		Set<Object> values = propertyValueMap.get(prop);
		
		if(values == null) {
			values = new HashSet<>();
			propertyValueMap.put(prop, values);
		}
		
		values.add(value);
	}
	
	public Set<Object> getPropertyValues(String propUri) {
		Set<Object> props = propertyValueMap.get(propUri);
		
		Set<Object> result;
		if(props != null) {
			result = new TreeSet<>(props);
		} else {
			result = Collections.emptySet();
		}
		
		return result;
	}
	
	public String getPropertyValuesAsString(String propUri) {
		Set<Object> values = getPropertyValues(propUri);
		StringBuilder sb = new StringBuilder();
		Iterator<Object> iter = values.iterator();
		while(iter.hasNext()) {
			sb.append(asString(iter.next()));
			
			if(iter.hasNext()) {
				sb.append("; ");
			}
		}
		
		return sb.toString();
	}

	public Set<String> getProperties() {
		return new TreeSet<>(propertyValueMap.keySet());
	}
	
	@Override
	public boolean equals(Object other) {
		boolean equal = false;
		
		if(other instanceof QrItemWrapper) {
			QrItemWrapper otherBean = (QrItemWrapper)other;
			
			equal = this.item.equals(otherBean.item);
		}
		
		return equal;
	}
	
	@Override
	public int hashCode() {
		final int prime = 37;
		int result = 17;
		result = prime*result + item.hashCode();
		
		return result;
	}

	private Object asString(Object next) {
		String s;
		if(next instanceof Calendar) {
			SimpleDateFormat df = new SimpleDateFormat("MMMM d, yyyy h:mm a");
			s = df.format(((Calendar) next).getTime());
		} else {
			s = next.toString();
		}
		return s;
	}
}
