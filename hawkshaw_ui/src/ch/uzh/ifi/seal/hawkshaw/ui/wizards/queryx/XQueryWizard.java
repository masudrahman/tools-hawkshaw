package ch.uzh.ifi.seal.hawkshaw.ui.wizards.queryx;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ui
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.Wizard;

import ch.uzh.ifi.seal.hawkshaw.core.QueryCore;
import ch.uzh.ifi.seal.hawkshaw.ontology.OntologyCore;
import ch.uzh.ifi.seal.hawkshaw.ontology.java.JavaOntologyCore;
import ch.uzh.ifi.seal.hawkshaw.ontology.java.facade.BuildStatus;
import ch.uzh.ifi.seal.hawkshaw.ui.selections.IResourceSelection;
import ch.uzh.ifi.seal.hawkshaw.ui.selections.XSelectionUtil;

public class XQueryWizard extends Wizard {
	private XQuerySystemInitPage initPage;
	private XProjectChooserPage  projectPage;
	private XModelInitPage       modelPage;
	private XQueryPage           queryPage;
	
	private IResourceSelection selection;
	
	public XQueryWizard() {
		setWindowTitle("Natural Language Queries");
		setSelection(XSelectionUtil.resolve());
	}
		
	public final void setSelection(IResourceSelection selection) {
		this.selection = selection;
	}

	@Override
	public boolean needsProgressMonitor() {
		return true;
	}
	
	@Override
	public void addPages() {
		initPage = new XQuerySystemInitPage();
		projectPage = new XProjectChooserPage();
		modelPage = new XModelInitPage();
		queryPage = new XQueryPage();

		addPage(initPage);
		addPage(projectPage);
		addPage(modelPage);
		addPage(queryPage);
	}

	@Override
	public IWizardPage getStartingPage() {
		IWizardPage startingPage;
		
		if(initPageNeeded()) {
			startingPage = initPage;
		} else if(projectPageNeeded()) {
			startingPage = projectPage;
		} else if(modelPageNeeded()) {
			modelPage.init(selection, getBuildStatusForSelection());
			startingPage = modelPage;
		} else {
			queryPage.init(selection);
			startingPage = queryPage;
		}
		
		return startingPage;
	}

	@Override
	public IWizardPage getNextPage(IWizardPage currentPage) {
		if(currentPage instanceof AbstractXQueryPage) {
			((AbstractXQueryPage)currentPage).finish();
		}
		
		return getStartingPage();
	}
	
	@Override
	public boolean performFinish() {
		queryPage.finish();
		
		return true;
	}

	private boolean initPageNeeded() {
		return !(QueryCore.ready() && OntologyCore.ready());
	}

	private boolean projectPageNeeded() {
		return selection == null;
	}

	private boolean modelPageNeeded() {
		return !getBuildStatusForSelection().isReady();
	}

	private BuildStatus getBuildStatusForSelection() {
		return JavaOntologyCore.getBuildStatusFor(selection.getJavaProject());
	}
}
