package ch.uzh.ifi.seal.hawkshaw.ui.actions.result;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ui
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.net.MalformedURLException;
import java.net.URL;

import org.eclipse.core.runtime.Status;
import org.eclipse.jface.action.Action;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.browser.IWebBrowser;
import org.eclipse.ui.browser.IWorkbenchBrowserSupport;

import ch.uzh.ifi.seal.hawkshaw.ui.IHawkshawImages;
import ch.uzh.ifi.seal.hawkshaw.ui.QueryInterfaceDispatcher;
import ch.uzh.ifi.seal.hawkshaw.ui.QueryUI;

public class OpenInBrowserAction extends Action {
	private static final String ID = "ch.uzh.ifi.seal.hawkshaw.ui.commands.result.openInBrowser";
	
	private String url;
	
	public OpenInBrowserAction(String url) {
		super("Open in Browser...");
		this.url = url;
		
		setActionDefinitionId(ID);
		setImageDescriptor(IHawkshawImages.URL_GO);
	}
	
	@Override
	public void run() {
		try {
			IWebBrowser browser = PlatformUI.getWorkbench()
											.getBrowserSupport()
											.createBrowser(IWorkbenchBrowserSupport.NAVIGATION_BAR |
														   IWorkbenchBrowserSupport.LOCATION_BAR |
														   IWorkbenchBrowserSupport.PERSISTENT,
														   "ch.ifi.seal.hawkshaw.ui.browser", null, null);
			browser.openURL(new URL(url));
		} catch(PartInitException e) {
			QueryInterfaceDispatcher.INSTANCE.showError(new Status(Status.ERROR,
																   QueryUI.PLUGIN_ID,
																   Status.OK,
																   "Could not init browser.",
																   e));
		} catch (MalformedURLException e) {
			QueryInterfaceDispatcher.INSTANCE.showError(new Status(Status.ERROR,
																   QueryUI.PLUGIN_ID,
																   Status.OK,
																   "URL could not be parsed.",
																   e));
		}
	}
}
