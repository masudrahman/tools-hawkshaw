package ch.uzh.ifi.seal.hawkshaw.ui.wizards.queryx;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ui
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.wizard.IWizardPage;
import org.eclipse.jface.wizard.WizardPage;

public abstract class AbstractXQueryPage extends WizardPage {
	
	public AbstractXQueryPage(String pageName, String title, ImageDescriptor titleImage) {
		super(pageName, title, titleImage);
	}
	
	@Override
	public boolean canFlipToNextPage() {
		return isPageComplete() && hasNextPage(); // do not call getNextPage() != null -> results in unnecessary computation
	}

	public boolean hasNextPage() {
		return getWizard().getNextPage(this) != null;
	}
	
	@Override
	public IWizardPage getPreviousPage() {
		return null; // TODO enable 'back' button for query page.
	}
	
	public void finish() {
		// default: do nothing
	}
}
