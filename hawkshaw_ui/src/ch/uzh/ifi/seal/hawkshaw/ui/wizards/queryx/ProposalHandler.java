package ch.uzh.ifi.seal.hawkshaw.ui.wizards.queryx;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ui
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.LinkedList;
import java.util.List;

import org.eclipse.jface.fieldassist.IContentProposal;
import org.eclipse.jface.fieldassist.IContentProposalListener;
import org.eclipse.jface.fieldassist.IContentProposalProvider;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.KeyEvent;
import org.eclipse.swt.events.KeyListener;
import org.eclipse.swt.events.VerifyEvent;
import org.eclipse.swt.events.VerifyListener;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Text;

import ch.uzh.ifi.seal.hawkshaw.ui.util.SessionWrapper;

/**
 * Provides the proposals for the query text field.
 * 
 * @author wuersch
 *
 */
public class ProposalHandler implements IContentProposalProvider, Listener, IContentProposalListener, VerifyListener, KeyListener {	
	/**
	 * The query context.
	 */
	private SessionWrapper session;
	
	/**
	 * The text field.
	 */
	private Text text;
	
	/**
	 * A buffer capturing the last entered characters or word, not yet added to
	 * the active question.
	 */
	private StringBuilder buffer;
	
	/**
	 * Constructor.
	 * 
	 * @param text the query text field that should have content assist.
	 * @param context the query context.
	 */
	public ProposalHandler(Text text, SessionWrapper session) {
		this.text = text;
		this.session = session;
		
		resetBuffer();
	}

	/**
	 * Return an array of content proposals representing the valid proposals for
	 * a field.
	 * 
	 * @param contents
	 *            the current contents of the text field
	 * @param position
	 *            - ignored.
	 * 
	 * @return the array of {@link IContentProposal} that represent valid
	 *         proposals for the field.
	 * 
	 * @see IContentProposalProvider#getProposals(String, int)
	 */
	@Override
	public IContentProposal[] getProposals(String contents, int position) {
		return asContentProposal(internalGetProposals(contents));
	}

	/**
	 * If a proposal is accepted, it will be added to the active question via
	 * the context. Furthermore, the text field will be updated and the buffer
	 * will be cleared.
	 * 
	 * @param proposal
	 *            the accepted content proposal
	 * 
	 * @see IContentProposalListener#proposalAccepted(IContentProposal)
	 */
	@Override
	public void proposalAccepted(IContentProposal proposal) {
		accept(proposal);
		updateTextField();
		resetBuffer();
	}

	/**
	 * Intercepts events that occur when the space bar or backspace keys are
	 * pressed. All other key presses can pass.
	 * 
	 * @param the
	 *            event triggered from a key press.
	 * 
	 * @see #handleSpacePressed()
	 * @see #handleBackspacePressed()
	 * @see KeyListener
	 */
	@Override
	public void keyPressed(KeyEvent e) {
		boolean doit;
		
		switch(e.keyCode) {
			case SWT.SPACE:
				doit = handleSpacePressed();
				break;
			case SWT.BS:
				doit = handleBackspacePressed();
				break;
			default:
				doit = true;
				break;
		}
		
		e.doit = doit;
	}

	/**
	 * Consumes all events that occur when a key is released.
	 * 
	 * @param e the event triggered from a key press.
	 * @see KeyListener
	 */
	@Override
	public void keyReleased(KeyEvent e) {
		e.doit = false;
	}

	/**
	 * Verifies whether a modification to the contents of the text field is
	 * allowed.
	 * 
	 * @param e
	 *            the event triggered by modifying the text field's contents.
	 * 
	 * @see VerifyListener#verifyText(VerifyEvent)
	 */
	@Override
	public void verifyText(VerifyEvent e) {
		boolean doit = false;

		if(isCharacterPressed(e)) {			
			doit = handleCharacterPressed(e.character);
		} else if((e.keyCode == SWT.NONE || e.keyCode == SWT.BS) && e.character != '^') { // if proposal is accepted and text is set, this will also trigger an event with keycode = SWT.NONE
			doit = true;
		}
		
		e.doit = doit;
	}

	/**
	 * Called whenever cancel button in search field is clicked. The button is
	 * only visible on some platforms.
	 * 
	 * @param e
	 *            the event triggered by clicking the cancel button of the text
	 *            field.
	 */
	@Override
	public void handleEvent(Event e) {
		if (e.detail == SWT.ICON_CANCEL) {
			session.clearActiveQuestion();
		}
	}

	/**
	 * Converts an array of strings to instances of
	 * {@link SimpleContentProposal}.
	 * 
	 * @param internalProposals
	 *            the proposals as strings.
	 * @return the proposals wrapped as {@link SimpleContentProposal}s.
	 */
	private IContentProposal[] asContentProposal(String[] internalProposals) {
		IContentProposal[] externalProposals = new IContentProposal[internalProposals.length];
		
		for(int i = 0; i < internalProposals.length; i++) {
			externalProposals[i] = new SimpleContentProposal(internalProposals[i]);
		}
		
		return externalProposals;
	}
	
	/**
	 * Returns all proposals that match the current contents of the text field.
	 * 
	 * @param contents
	 *            the contents of the text field.
	 * @return the proposals that start with the last (incomplete) word in the
	 *         text field.
	 */
	private String[] internalGetProposals(String contents) {
		List<String> contentProposals = new LinkedList<>();
		
		for(String nextWord : session.nextWords()) {
			if(isProposalValid(nextWord, contents)) {
				contentProposals.add(nextWord);
			}
		}
		
		return contentProposals.toArray(new String[contentProposals.size()]);
	}
	
	/**
	 * Deals with character presses and returns whether the pressed character
	 * should be added to the text field or not.
	 * 
	 * @param character
	 *            the pressed character.
	 * @return <code>true</code> if the pressed character can be added to the
	 *         text field, <code>false</code> otherwise.
	 */
	private boolean handleCharacterPressed(char character) {
		boolean doit = false;
		
		for(String nextWord : session.nextWords()) {				
			if(startsWithIgnoreCase(nextWord, buffer.toString() + character)) {
				buffer.append(character);
				doit = true;
				
				break; // no need to look further - at least on proposal matches buffer + char
			}
		}
		
		return doit;
	}

	/**
	 * Deals with presses of the space bar.
	 * 
	 * @return always <code>false</code>, because white space is always managed
	 *         by the application, not by the user. Might change in the future,
	 *         therefore the return type.
	 */
	private boolean handleSpacePressed() {
		String bufferedString = buffer.toString();
		String[] nextProposals = internalGetProposals(bufferedString);
		
		if(nextProposals.length == 1) { // auto-complete if space is pressed and only one proposal exists
			String nextWord = nextProposals[0];
			
			if(startsWithIgnoreCase(nextWord, bufferedString)) {
				addWord(nextWord);
			}
		} else { // auto-complete if space is pressed and currently entered string if it matches a proposal exactly 
			for(String nextWord : session.nextWords()) {
				if(nextWord.equalsIgnoreCase(bufferedString)) {
					addWord(nextWord);
					
					break; // no need to continue, we have added the word
				}
			}
		}
		
		return false; // whitespace is only managed by the application
	}

	/**
	 * Deals with presses of backspace.
	 * 
	 * @return <code>true</code> if a buffered character was deleted (and
	 *         consequently the last character in the text field can be
	 *         deleted), <code>false</code> if pressing backspace resulted in
	 *         deletion of a whole word.
	 */
	private boolean handleBackspacePressed() {
		boolean doit;
		
		if(buffer.length() > 0) {
			deleteLastBufferedChar();
			doit = true;
		} else {
			deleteLastWord();
			doit = false;
		}
		
		return doit;
	}

	/**
	 * Unpacks the proposal and adds the corresponding word to the active
	 * question via the context. Triggers also a reset of the buffer and an
	 * update of the text field.
	 * 
	 * @param proposal
	 *            the accepted proposal
	 */
	private void accept(IContentProposal proposal) {
		addWordToSession(proposal.getContent());
	}

	/**
	 * Adds a word to the active question via the context.
	 * Triggers also a reset of the buffer and an update of the text field.
	 * 
	 * @param word
	 *            the word to be added.
	 */
	private void addWord(String word) {
		addWordToSession(word);
		updateTextField();
		resetBuffer();
	}

	/**
	 * Adds a word to the active question via the context
	 * 
	 * @param word
	 *            the word to be added.
	 */
	private void addWordToSession(String word) {
		session.add(word);
	}

	/**
	 * Deletes the last word from the active question via the context.
	 * Triggers also a reset of the buffer and an update of the text field.
	 */
	private void deleteLastWord() {
		removeLastWordFromContext();
		updateTextField();
		resetBuffer();
	}

	/**
	 * Deletes the last word from the active question via the context.
	 */
	private void removeLastWordFromContext() {
		session.removeLastWord();
	}

	/**
	 * Checks whether the proposal matches the current contents of the text
	 * field.
	 * 
	 * @param proposalValue
	 *            the proposal.
	 * @param textFieldContent
	 *            the contents of the text field.
	 * @return <code>true</code> if the proposal matches the contents of the
	 *         text field, <code>false</code> otherwise.
	 */
	private boolean isProposalValid(String proposalValue, String textFieldContent) {
		return startsWithIgnoreCase(proposalValue, getLastWord(textFieldContent));
	}

	/**
	 * Checks whether the key event was triggered by pressing a character,
	 * digit, or the question mark '?'.
	 * 
	 * @param e
	 *            the event.
	 * @return <code>true</code> if the event was triggered by pressing a
	 *         character, <code>false</code> otherwise.
	 */
	private boolean isCharacterPressed(KeyEvent e) {
		return e.keyCode >= 97 && e.keyCode <= 122 /* characters */ || e.keyCode >=48 && e.keyCode <=57 /* digits */ || e.keyCode == 45 /* dashes */ || e.character == '?';
	}

	/**
	 * Similar to {@link String#startsWith(String)} but case insensitive.
	 * 
	 * @param superstring
	 *            the containing string.
	 * @param substring
	 *            the prefix string.
	 * @return <code>true</code> if the character sequence represented by the
	 *         second argument is a prefix of the character sequence represented
	 *         by the first one; <code>false</code> otherwise. Ignores upper and
	 *         lower case.
	 */
	private boolean startsWithIgnoreCase(String superstring, String substring) {
		return superstring.length() >= substring.length() &&
			   superstring.toLowerCase().startsWith(substring.toLowerCase());
	}

	/**
	 * Returns the last (potentially incomplete) word in the text field,
	 * starting at the last white space. If there is no white space, then the
	 * contents of the text field will be returned unchanged.
	 * 
	 * @param contents
	 *            the contents of the text field.
	 * @return the last word in the text field.
	 */
	private String getLastWord(String contents) {
		String result = contents;
		
		int beginningOfLastWord = result.lastIndexOf(SWT.SPACE);
		
		if(beginningOfLastWord > 0) {
			result = result.substring(beginningOfLastWord + 1);
		}
		
		return result;
	}
	
	/**
	 * Updates the text field with the active question and sets the carret
	 * position accordingly.
	 */
	private void updateTextField() {
		String phrase = session.getActiveQuestionString();
		int phraseLength = phrase.length();
		
		if(phraseLength > 0 && !phrase.endsWith("?")) {
			phrase += SWT.SPACE;
			phraseLength++;
		}

		text.setText(phrase);
		text.setSelection(phraseLength);
	}

	/**
	 * Deletes the last character from the buffer.
	 */
	private void deleteLastBufferedChar() {
		buffer.deleteCharAt(buffer.length() - 1);
	}
	
	/**
	 * Clears the buffer.
	 */
	private void resetBuffer() {
		buffer = new StringBuilder();
	}
	
	/**
	 * Simple wrapper for the proposed words.
	 * 
	 * @author wuersch
	 *
	 */
	private static final class SimpleContentProposal implements IContentProposal {
		private String word;
		
		/**
		 * Constructor.
		 * 
		 * @param word the wrapped word.
		 */
		public SimpleContentProposal(String word) {
			this.word = word;
		}
		
		/**
		 * Returns the wrapped word.
		 * 
		 * @see IContentProposal#getContent()
		 */
		@Override
		public String getContent() {
			return word;
		}

		/**
		 * Returns the length of the word for setting the cursor correctly.
		 * 
		 * @see IContentProposal#getCursorPosition()
		 */
		@Override
		public int getCursorPosition() {
			return word.length();
		}

		/**
		 * Returns nothing - unused.
		 * 
		 * @see IContentProposal#getLabel()
		 */
		@Override
		public String getLabel() {
			return null; // content is used instead
		}

		/**
		 * Returns nothing - unused.
		 * 
		 * @see IContentProposal#getDescription()
		 */
		@Override
		public String getDescription() {
			return null; // no description available
		}	
	}
}
