package ch.uzh.ifi.seal.hawkshaw.ui.views.result;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ui
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import org.eclipse.jdt.core.IJavaProject;

import ch.uzh.ifi.seal.hawkshaw.core.query.Answer;

public class Result {
	private Answer answer;
	private IJavaProject associatedProject;
	
	public Result(Answer answer, IJavaProject associatedProject) {
		this.associatedProject = associatedProject;
		this.answer = answer;
	}

	public IJavaProject getAssociatedProject() {
		return associatedProject;
	}

	public Answer getAnswer() {
		return answer;
	}
	
	public int size() {
		return answer.size();
	}
}
