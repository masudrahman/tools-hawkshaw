package ch.uzh.ifi.seal.hawkshaw.ui.wizards.queryx;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ui
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.lang.reflect.InvocationTargetException;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jface.operation.IRunnableWithProgress;

import ch.uzh.ifi.seal.hawkshaw.ontology.OntologyCore;
import ch.uzh.ifi.seal.hawkshaw.ontology.java.JavaOntologyCore;
import ch.uzh.ifi.seal.hawkshaw.ontology.model.PersistentJenaModel;

public class XModelInitializer implements IRunnableWithProgress {
	private IJavaProject javaProject;
	private boolean needsNature;
	private boolean needsBuild;
	
	public XModelInitializer(IJavaProject javaProject, boolean needsNature, boolean needsBuild) {
		this.javaProject = javaProject;
		this.needsNature = needsNature;
		this.needsBuild  = needsBuild;
	}
	
	@Override
	public void run(IProgressMonitor monitor) throws InvocationTargetException, InterruptedException {
		SubMonitor progress = SubMonitor.convert(monitor, "Building model for " + javaProject.getElementName(), 100);
		
		if(needsNature) {
			JavaOntologyCore.addNatureTo(javaProject, progress.newChild(20));
		}
		
		progress.setWorkRemaining(80);
		
		if(needsBuild) {
			JavaOntologyCore.build(javaProject, progress.newChild(50));
		}
		
		progress.setTaskName("Waiting for build to complete.");
		JavaOntologyCore.waitForBuild(null);
		
		progress.setWorkRemaining(30);
		
		progress.setTaskName("Reasoning. This could take a moment.");
		Thread t = new ReasonerThread(javaProject.getProject());
		t.start();
		
		while(t.isAlive()) {
			progress.setWorkRemaining(1000);
			progress.worked(1);
			Thread.sleep(200);
		}
	}
	
	private static final class ReasonerThread extends Thread {
		private IProject project;
		
		public ReasonerThread(IProject project) {
			this.project = project;
		}
		
		@Override
		public void run() {
			PersistentJenaModel model = OntologyCore.fetchPersistentModelFor(project);
			model.reason();
		}
	}
}
