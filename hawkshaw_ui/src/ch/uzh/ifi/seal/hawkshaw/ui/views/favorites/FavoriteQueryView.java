package ch.uzh.ifi.seal.hawkshaw.ui.views.favorites;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ui
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.action.ToolBarManager;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.viewers.TableViewerColumn;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.ControlAdapter;
import org.eclipse.swt.events.ControlEvent;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.graphics.Cursor;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.IViewSite;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.ScrolledForm;
import org.eclipse.ui.forms.widgets.Section;
import org.eclipse.ui.part.ViewPart;

import ch.uzh.ifi.seal.hawkshaw.core.query.Answer;
import ch.uzh.ifi.seal.hawkshaw.ui.IHawkshawImages;
import ch.uzh.ifi.seal.hawkshaw.ui.QueryUI;

public class FavoriteQueryView extends ViewPart implements ISelectionChangedListener {
	public static final String VIEW_ID = "ch.uzh.ifi.seal.hawkshaw.ui.favoritequeriesview";
	
	private FormToolkit toolkit;
	private ScrolledForm form;
	
	private Composite content;
	
	private TableViewer queryViewer;
	private TableViewer sparqlViewer;
	
	private Action executeAction;
	private Action editAction;
	private Action addAction;
	private Action removeAction;

	
	private FavoritesStore favorites;
	
	public FavoriteQueryView() {
		favorites = new FavoritesStore();
	}

	@Override
	public void createPartControl(Composite parent) {
		toolkit = new FormToolkit(parent.getDisplay());
		form = toolkit.createScrolledForm(parent);
		form.setImage(QueryUI.getDefault().getImage(IHawkshawImages.FAVS_TABLE));
		form.setText("Favorite Queries");
		
		toolkit.decorateFormHeading(form.getForm());
		
		content = form.getBody();
		content.setLayout(new GridLayout(2, true));	

		createQueryListSection();
		createQueryDetailSection();
		
		/*
		createMenu();
		
		createContextMenu();
		hookGlobalActions();
		 */
		
//		createActions();
//		createToolbar();
	}
	
	@Override
	public void init(IViewSite site, IMemento memento) throws PartInitException {
		super.init(site, memento);
		favorites.load(memento);
	}

	@Override
	public void saveState(IMemento memento) {
		super.saveState(memento);
		favorites.save(memento);
	}

	@Override
	public void dispose() {
		if(toolkit != null) {
			toolkit.dispose();
		}
		
		super.dispose();
	}

	@Override
	public void setFocus() {
		form.setFocus();
	}

	@Override
	public void selectionChanged(SelectionChangedEvent event) {
		ISelection selection = event.getSelection();
		if(!selection.isEmpty()) {
			FavoriteQueryItem item = convertSelection(selection);
			if(item != null) {
				sparqlViewer.setInput(item);
			}
			
			executeAction.setEnabled(true);
			editAction.setEnabled(true);
			removeAction.setEnabled(true);
		}
	}
	
	public void add(Answer answer, IJavaProject project) {
		favorites.add(answer, project);
		queryViewer.refresh();
	}
	
	public void add(FavoriteQueryItem item) {
		favorites.add(item);
		queryViewer.refresh();
	}
	
	private void remove(FavoriteQueryItem item) {
		favorites.remove(item);
		queryViewer.refresh();
		sparqlViewer.setInput(null);
	}

	private void deleteCurrentlySelectedQuery() {
		FavoriteQueryItem item = convertSelection(queryViewer.getSelection());
		if(item != null)  {
			remove(item);
		}
	}

	private void createQueryListSection() {
		Section section = toolkit.createSection(content, Section.DESCRIPTION | Section.TITLE_BAR);
		section.setText("Favorite Questions");
		section.setDescription("The questions listed below were previously bookmarked:");
		
		GridData gridData = new GridData();
		gridData.horizontalAlignment = SWT.FILL;
		gridData.verticalAlignment = SWT.FILL;
		gridData.grabExcessHorizontalSpace = true;
		gridData.grabExcessVerticalSpace = true;
		section.setLayoutData(gridData);
				
		Composite sectionClient = toolkit.createComposite(section);
		sectionClient.setLayout(new GridLayout());
		
		Table table = toolkit.createTable(sectionClient, SWT.SINGLE);
		
		gridData = new GridData();
		gridData.horizontalAlignment = SWT.FILL;
		gridData.verticalAlignment = SWT.FILL;
		gridData.grabExcessHorizontalSpace = true;
		gridData.grabExcessVerticalSpace = true;
		table.setLayoutData(gridData);
		
		queryViewer = new TableViewer(table);
		TableViewerColumn viewerColumn = new TableViewerColumn(queryViewer, SWT.NONE);
		
		table.addControlListener(new ColumnResizer(table, viewerColumn));
		
		viewerColumn.setEditingSupport(new QueryEditing(queryViewer));
		
		queryViewer.addSelectionChangedListener(this);
		queryViewer.setContentProvider(new FavoritesProvider());
		queryViewer.setLabelProvider(new FavoritesLabelProvider());
		queryViewer.setInput(favorites);
		
		createSectionToolBar(section);
		
		section.setClient(sectionClient);
	}
	
	private void createQueryDetailSection() {
		Section section = toolkit.createSection(content, Section.DESCRIPTION | Section.TITLE_BAR);
		section.setText("SPARQL");
		section.setDescription("The corresponding formal query is given below:");
		
		GridData gridData = new GridData();
		gridData.horizontalAlignment = SWT.FILL;
		gridData.verticalAlignment = SWT.FILL;
		gridData.grabExcessHorizontalSpace = true;
		gridData.grabExcessVerticalSpace = true;
		section.setLayoutData(gridData);
		
		Composite sectionClient = toolkit.createComposite(section);
		sectionClient.setLayout(new GridLayout());
		
		Table table = toolkit.createTable(sectionClient, SWT.MULTI | SWT.H_SCROLL | SWT.V_SCROLL );
		
		gridData = new GridData();
		gridData.verticalIndent = 7;
		gridData.horizontalAlignment = SWT.FILL;
		gridData.verticalAlignment = SWT.FILL;
		gridData.grabExcessHorizontalSpace = true;
		gridData.grabExcessVerticalSpace = true;
		
		table.setLayoutData(gridData);
		
		sparqlViewer = new TableViewer(table);
		sparqlViewer.setContentProvider(new SparqlProvider());
		sparqlViewer.setLabelProvider(new SparqlLabelProvider());

		section.setClient(sectionClient);
	}

	private void createSectionToolBar(Section section) {
		ToolBarManager toolBarManager = new ToolBarManager(SWT.FLAT);
		ToolBar toolbar = toolBarManager.createControl(section);
		final Cursor handCursor = new Cursor(Display.getCurrent(), SWT.CURSOR_HAND);
		toolbar.setCursor(handCursor);
		// Cursor needs to be explicitly disposed
		toolbar.addDisposeListener(new CursorDisposalListener(handCursor));
		
		executeAction = new QueryExecutionAction();
		editAction = new QueryEditAction(queryViewer);
		addAction = new AddQueryAction();
		removeAction = new QueryRemovalAction();
		
		toolBarManager.add(executeAction);
		toolBarManager.add(new Separator("editGroup"));
		toolBarManager.add(editAction);
		toolBarManager.add(addAction);
		toolBarManager.add(removeAction);
		
		toolBarManager.update(true);
	
		section.setTextClient(toolbar);
	}

	private static FavoriteQueryItem convertSelection(ISelection selection) {
		FavoriteQueryItem item = null;
		
		if(selection instanceof IStructuredSelection) {
			IStructuredSelection structSelection = (IStructuredSelection) selection;
			Object selectedObject = structSelection.getFirstElement();
			
			if(selectedObject instanceof FavoriteQueryItem) {
				item = (FavoriteQueryItem) selectedObject;
			}
		}
		
		return item;
	}
	
	private static final class CursorDisposalListener implements DisposeListener {
		private final Cursor handCursor;

		private CursorDisposalListener(Cursor handCursor) {
			this.handCursor = handCursor;
		}

		public void widgetDisposed(DisposeEvent e) {
			if (handCursor != null && !handCursor.isDisposed()) {
				handCursor.dispose();
			}
		}
	}
	
	private static final class ColumnResizer extends ControlAdapter {
		private Table table;
		private TableViewerColumn viewerColumn;

		private ColumnResizer(Table table, TableViewerColumn viewerColumn) {
			this.table = table;
			this.viewerColumn = viewerColumn;
		}

		@Override
		public void controlResized(ControlEvent e) {
			Point vBarSize = table.getVerticalBar().getSize();
			viewerColumn.getColumn().setWidth(table.getSize().x - vBarSize.x);
		}
	}

	private static final class QueryEditAction extends Action {
		private TableViewer viewer;
		
		private QueryEditAction(TableViewer viewer) {
			super("Edit");
			this.viewer = viewer;
			
			setToolTipText("Rename Query");
			setImageDescriptor(IHawkshawImages.EDIT);
			setEnabled(false);
		}
		
		@Override
		public void run() {
			FavoriteQueryItem item = convertSelection(viewer.getSelection());
			
			if(item != null) {
				item.setEditable(true);			
				viewer.editElement(item, 0);
			}
		}
	}
	
	private final class AddQueryAction extends Action {
		private AddQueryAction() {
			super("Add");
			setToolTipText("Add Favorite Query");
			setImageDescriptor(IHawkshawImages.EDIT_ADD);
		}
		
		@Override
		public void run() {
			AddQueryDialog dialog = new AddQueryDialog(FavoriteQueryView.this);
			if(Window.OK == dialog.open()) {
				add(dialog.getItem());
			}
		}
	}
	
	private final class QueryRemovalAction extends Action {
		private QueryRemovalAction() {
			super("Remove");
			setToolTipText("Remove Favorite Query");
			setImageDescriptor(IHawkshawImages.EDIT_DEL);
			setEnabled(false);
		}
		
		@Override
		public void run() {
			deleteCurrentlySelectedQuery();
		}
	}

	private final class QueryExecutionAction extends Action {
		private QueryExecutionAction() {
			super("Execute");
			setToolTipText("Execute Query");
			setImageDescriptor(IHawkshawImages.FIND);
			setEnabled(false);
		}
		
		@Override
		public void run() {
			FavoriteQueryItem item = convertSelection(queryViewer.getSelection());
			if(item != null) {
				FavoriteQueryExecutionJob queryExecutor = new FavoriteQueryExecutionJob(item);
				queryExecutor.schedule();
			}
		}

	}
}
