package ch.uzh.ifi.seal.hawkshaw.ui.views.result;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ui
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.eclipse.core.runtime.Platform;
import org.eclipse.swt.graphics.Image;

import ch.uzh.ifi.seal.hawkshaw.core.query.Answer;
import ch.uzh.ifi.seal.hawkshaw.ui.IHawkshawImages;
import ch.uzh.ifi.seal.hawkshaw.ui.QueryUI;
import ch.uzh.ifi.seal.hawkshaw.ui.util.AbstractField;

public class ResultCompilationTimeField extends AbstractField {

	@Override
	public String getDescription() {
		return "The number of milliseconds that the compilation of the query took.";
	}

	@Override
	public String getColumnHeaderText() {
		return "Compilation Time";
	}

	@Override
	public Image getColumnHeaderImage() {
		Image image = null;
		
		if(!Platform.getOS().equals(Platform.OS_MACOSX)) { // column images look weird on mac osx
			image = QueryUI.getDefault().getImage(IHawkshawImages.GEAR);
		}
		
		return image;
	}
	
	@Override
	public String getValue(Object object) {
		String result = null;
		
		if(object instanceof AnswerTreeItem) {
			Answer answer = ((AnswerTreeItem)object).getItem();
			
			long compTime = answer.getCompilationTime();
			
			result = compTime == 0 ? "< 1 ms" : compTime + " ms";
		}
		
		return result;
	}

	@Override
	public int getPreferredWidth() {
		return 78;
	}

}
