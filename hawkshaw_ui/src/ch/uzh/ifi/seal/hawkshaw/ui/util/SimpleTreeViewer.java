package ch.uzh.ifi.seal.hawkshaw.ui.util;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ui
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import org.eclipse.jface.util.LocalSelectionTransfer;
import org.eclipse.jface.viewers.ColumnLayoutData;
import org.eclipse.jface.viewers.ColumnPixelData;
import org.eclipse.jface.viewers.ColumnViewerToolTipSupport;
import org.eclipse.jface.viewers.IContentProvider;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.ISelectionProvider;
import org.eclipse.jface.viewers.TableLayout;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.TreeViewerColumn;
import org.eclipse.swt.SWT;
import org.eclipse.swt.dnd.DND;
import org.eclipse.swt.dnd.DragSourceAdapter;
import org.eclipse.swt.dnd.DragSourceEvent;
import org.eclipse.swt.dnd.Transfer;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.ScrollBar;
import org.eclipse.swt.widgets.Scrollable;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.ui.IMemento;

public class SimpleTreeViewer implements ISelectionProvider {
	private static final String KEY_COLUMN_WIDTH = "columnWidth";
	private static final String KEY_COLUMN_ORDER_INDEX = "columnOrderIndex";
	private static final String KEY_COLUMN_ORDER = "columnOrder";
	private static final String KEY_VERTICAL_POSITION = "verticalPosition";
	private static final String KEY_HORIZONTAL_POSITION = "horizontalPosition";
	
	private TreeViewer viewer;
	private Tree tree;
	
	private IField[] fields;
	
	public SimpleTreeViewer(Composite parent, IField[] fields) {
		this(parent,
			 SWT.H_SCROLL
		   | SWT.V_SCROLL
		   | SWT.SINGLE
		   | SWT.FULL_SELECTION,
			 fields);
	}
	
	public SimpleTreeViewer(Composite parent, int style, IField[] fields) {
		this.fields = fields.clone();

		tree = new Tree(parent, style);
		tree.setLinesVisible(true);
		
		viewer = new TreeViewer(tree);
		ColumnViewerToolTipSupport.enableFor(viewer);
	}
	
	public void init(IMemento memento) {
		createColumns(memento);
		restoreScrollbars(memento);
	}
	
	public void enableSelectionDrag() {
		viewer.addDragSupport(DND.DROP_MOVE | DND.DROP_COPY,
							  new Transfer[] { LocalSelectionTransfer.getTransfer() },
							  new SelectionDragAdapter(viewer));
	}
	
	public void setLayoutData(Object layoutData) {
		tree.setLayoutData(layoutData);
	}
	
	public void setContentProvider(IContentProvider provider) {
		viewer.setContentProvider(provider);
	}
	
	public void setInput(Object input) {
		viewer.setInput(input);
	}
	
	public void saveState(IMemento memento) {		
		saveColumnWidth(memento);
		saveColumnOrder(memento);
		
		saveVerticalScrollbarPosition(memento);
		saveHorizontalScrollbarPosition(memento);
	}
	
	public void setFocus() {
		if (viewer != null && !viewer.getControl().isDisposed()) {
			viewer.getControl().setFocus();
		}
	}
	
	public Control getControl() {
		return viewer.getControl();
	}
	
	public void addSelectionChangedListener(ISelectionChangedListener listener){
		viewer.addSelectionChangedListener(listener);
	}

	public ISelection getSelection() {
		return viewer.getSelection();
	}
	
	@Override
	public void removeSelectionChangedListener(ISelectionChangedListener listener) {
		viewer.removeSelectionChangedListener(listener);
	}

	@Override
	public void setSelection(ISelection selection) {
		viewer.setSelection(selection);
	}

	public Object getInput() {
		return viewer.getInput();
	}
	
	
	private void saveColumnOrder(IMemento memento) {
		int[] columnOrder = tree.getColumnOrder();
		for (int i = 0; i < columnOrder.length; i++) {
			IMemento child = memento.createChild(KEY_COLUMN_ORDER);
			child.putInteger(KEY_COLUMN_ORDER_INDEX, columnOrder[i]);
		}
	}

	private void saveColumnWidth(IMemento memento) {
		ColumnPixelData[] data = getColumnData(memento);
		
		for (int i = 0; i < data.length; i++) {
			ColumnPixelData data2 = data[i];
			memento.putInteger(KEY_COLUMN_WIDTH + i, data2.width);
		}
	}
	
	private void saveHorizontalScrollbarPosition(IMemento memento) {
		Scrollable scrollable = (Scrollable) viewer.getControl();
		ScrollBar bar = scrollable.getHorizontalBar();
		int position = (bar != null) ? bar.getSelection() : 0;
		memento.putInteger(KEY_HORIZONTAL_POSITION, position);
	}

	private void saveVerticalScrollbarPosition(IMemento memento) {
		Scrollable scrollable = (Scrollable) viewer.getControl();
		ScrollBar bar = scrollable.getVerticalBar();
		int position = (bar != null) ? bar.getSelection() : 0;
		memento.putInteger(KEY_VERTICAL_POSITION, position);
	}

	private int[] restoreColumnOrder(IMemento memento) {
		if(memento == null) {
			return null;
		}
		
		IMemento children[] = memento.getChildren(KEY_COLUMN_ORDER);
		
		if(children != null) {
			int n = children.length;
			int[] values = new int[n];
			for(int i = 0; i < n; i++) {
				Integer val = children[i].getInteger(KEY_COLUMN_ORDER_INDEX);
				if(val != null) {
					values[i] = val.intValue();
				} else {
					// invalid entry so use default column order
					return null;
				}
			}
			
			return values;
		}
		
		return null;
	}

	private void restoreScrollbars(IMemento memento) {
		Scrollable scrollable = (Scrollable) viewer.getControl();
		ScrollBar bar = scrollable.getVerticalBar();
		if (bar != null) {
			bar.setSelection(restoreVerticalScrollBarPosition(memento));
		}
		bar = scrollable.getHorizontalBar();
		if (bar != null) {
			bar.setSelection(restoreHorizontalScrollBarPosition(memento));
		}
	}

	private int restoreVerticalScrollBarPosition(IMemento memento) {
		if (memento == null) {
			return 0;
		}
		Integer position = memento.getInteger(KEY_VERTICAL_POSITION);
		return (position == null) ? 0 : position.intValue();
	}

	private int restoreHorizontalScrollBarPosition(IMemento memento) {
		if (memento == null) {
			return 0;
		}
		Integer position = memento.getInteger(KEY_HORIZONTAL_POSITION);
		return (position == null) ? 0 : position.intValue();
	}

	private ColumnPixelData[] getColumnData(IMemento memento) {
		ColumnPixelData[] defaultData = getSavedColumnData(memento);
	
		if (tree != null && (tree.isDisposed() || tree.getBounds().width == 0)) {
			tree = null;
		}
	
		TreeColumn[] column = null;
		if (tree != null) {
			column = tree.getColumns();
		}
	
		ColumnPixelData[] result = new ColumnPixelData[defaultData.length];
		for (int i = 0; i < defaultData.length; i++) {
			ColumnPixelData defaultPixelData = defaultData[i];
			int width = defaultData[i].width;
	
			if (column != null && i < column.length) {
				TreeColumn col = column[i];
	
				if (col.getWidth() > 0) {
					width = col.getWidth();
				}
			}
	
			result[i] = new ColumnPixelData(width, defaultPixelData.resizable,
					defaultPixelData.addTrim);
		}
	
		return result;
	}
	
	private void createColumns(IMemento memento) {
		TableLayout layout = new TableLayout();
		tree.setLayout(layout);
		tree.setHeaderVisible(true);

		ColumnLayoutData[] columnWidths = getSavedColumnData(memento);
		for (int i = 0; i < fields.length; i++) {
			layout.addColumnData(columnWidths[i]);
			TreeColumn tc = new TreeColumn(tree, SWT.NONE, i);
			IField field = fields[i];
			tc.setText(field.getColumnHeaderText());
			tc.setImage(field.getColumnHeaderImage());
			tc.setResizable(columnWidths[i].resizable);
			tc.setMoveable(true);
			tc.addSelectionListener(getHeaderListener());
			tc.setData(field);
			TreeViewerColumn viewerColumn = new TreeViewerColumn(viewer, tc);
			viewerColumn.setLabelProvider(new DefaultSimpleTreeLabelProvider(field));
		}
		
		int[] order = restoreColumnOrder(memento);
		if(order != null && order.length == fields.length) {
			tree.setColumnOrder(order);
		}	
	}
	
	private ColumnPixelData[] getSavedColumnData(IMemento memento) {
		ColumnPixelData[] defaultData = getDefaultColumnLayouts();
		
		ColumnPixelData[] result = new ColumnPixelData[defaultData.length];
		for (int i = 0; i < result.length; i++) {
			ColumnPixelData defaultPixelData = defaultData[i];
			boolean addTrim = defaultPixelData.addTrim;
			int width = defaultPixelData.width;
			// non-resizable columns are always left at their default width
			if (defaultPixelData.resizable && memento != null) {
				Integer widthInt = memento.getInteger(KEY_COLUMN_WIDTH + i);
				if(widthInt != null && widthInt.intValue() > 0) {
					width = widthInt.intValue();
					addTrim = false;
				}
			}
			
			result[i] = new ColumnPixelData(width, defaultPixelData.resizable, addTrim);
		}
		
		return result;
	}
	
	private ColumnPixelData[] getDefaultColumnLayouts() {
		ColumnPixelData[] datas = new ColumnPixelData[fields.length];
	
		for (int i = 0; i < fields.length; i++) {
			int width = getWidth(fields[i]);
			boolean resizable = width > 0;
			datas[i] = new ColumnPixelData(width, resizable, resizable);
		}
		return datas;
	}
	
	private int getWidth(IField field) {
		if (!field.isShowing()) {
			return 0;
		}
		return field.getPreferredWidth();
	}
	
	private SelectionListener getHeaderListener() {
		return new HeaderListener();
	}
	
	private static final class HeaderListener extends SelectionAdapter {
		// place-holder
	}
	
	private static final class SelectionDragAdapter extends DragSourceAdapter {
		private TreeViewer viewer;
		
		public SelectionDragAdapter(TreeViewer viewer) {
			this.viewer = viewer;
		}
		
		public void dragSetData(DragSourceEvent event){
			LocalSelectionTransfer transfer = LocalSelectionTransfer.getTransfer();
			transfer.setSelection(viewer.getSelection());
		}
	}
}
