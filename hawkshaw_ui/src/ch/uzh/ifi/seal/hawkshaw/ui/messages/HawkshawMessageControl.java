package ch.uzh.ifi.seal.hawkshaw.ui.messages;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ui
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.net.MalformedURLException;
import java.net.URL;

import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.widgets.Composite;

import ch.uzh.ifi.seal.hawkshaw.ui.actions.query.OpenQueryDialogAction;
import ch.uzh.ifi.seal.hawkshaw.ui.actions.views.OpenFavoriteQueryViewAction;
import ch.uzh.ifi.seal.hawkshaw.ui.actions.views.OpenInspectorViewAction;
import ch.uzh.ifi.seal.hawkshaw.ui.actions.views.OpenQueryResultsViewAction;

public class HawkshawMessageControl extends MessageControl {
	private HawkshawMessage currentMessage;
	
	public HawkshawMessageControl(Composite parent) {
		super(parent);
	}

	@Override
	public void closeMessage() {
		if(currentMessage != null) {
			close();
		}
	}

	@Override
	protected SelectionListener getLinkListener() {
		return new LinkListener();
	}
	
	public void setMessage(HawkshawMessage message) {
		if (message != null) {
			ensureControl();
			
			setTitle(message.getTitle());
			setTitleImage(message.getImage());
			setDescription(message.getDescription());
			
			this.currentMessage = message;
		}
		
	}
	
	public static String getAction(String action) {
		String result = action;
		
		if (result.startsWith("http")) { //$NON-NLS-1$
			URL url;
			try {
				url = new URL(result);
				String query = url.getQuery();
				if (query != null && query.startsWith("action=")) { //$NON-NLS-1$
					int i = query.indexOf('&'); //$NON-NLS-1$
					if (i != -1) {
						result = query.substring(7, i);
					} else {
						result = query.substring(7);
					}
				} else {
					return null;
				}
			} catch (MalformedURLException e1) {
				// ignore
				return null;
			}
		}
		
		return result.toLowerCase();
	}

	private class LinkListener extends SelectionAdapter {
		@Override
		public void widgetSelected(SelectionEvent e) {
			if (e.text == null) {
				return;
			}
			
			String action = getAction(e.text);
			if ("open-query-dialog".equals(action)) { //$NON-NLS-1$
				closeMessage();
				
				new OpenQueryDialogAction().run();
			} else if("show-favorites-view".equals(action)){
				new OpenFavoriteQueryViewAction().run();
			} else if("show-inspector-view".equals(action)){
				new OpenInspectorViewAction().run();
			} else if("show-results-view".equals(action)){
				new OpenQueryResultsViewAction().run();
			}
		}
	}
}
