package ch.uzh.ifi.seal.hawkshaw.ui.prefs;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ui
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.eclipse.jface.preference.DirectoryFieldEditor;
import org.eclipse.swt.widgets.Composite;

public class XDirectoryFieldEditor extends DirectoryFieldEditor {
	private Composite parent;
	
	public XDirectoryFieldEditor(String name, String labelText, Composite parent) {
		super(name, labelText, parent);
		this.parent = parent;
	}
	
	@Override
	public boolean isValid() {
		return super.isValid() || !getLabelControl(parent).isEnabled();
	}
	
	@Override
	public void showErrorMessage(String message) {
		if(getLabelControl(parent).isEnabled()) {
			super.showErrorMessage(message);
		}
	}
	
	@Override
	public void setEnabled(boolean enabled, Composite parent) {
		super.setEnabled(enabled, parent);
		
		if(enabled) {
			showErrorMessage();
		} else {
			clearErrorMessage();
		}
	}
}
