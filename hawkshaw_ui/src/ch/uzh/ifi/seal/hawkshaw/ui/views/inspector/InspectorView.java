package ch.uzh.ifi.seal.hawkshaw.ui.views.inspector;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ui
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.Set;

import org.eclipse.core.runtime.Platform;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.action.Separator;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ColumnViewerToolTipSupport;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.part.ViewPart;

import ch.uzh.ifi.seal.hawkshaw.ontology.vocabulary.IRDFS;
import ch.uzh.ifi.seal.hawkshaw.ui.IHawkshawImages;
import ch.uzh.ifi.seal.hawkshaw.ui.QueryUI;
import ch.uzh.ifi.seal.hawkshaw.ui.actions.result.ShowSourceAction;
import ch.uzh.ifi.seal.hawkshaw.ui.messages.HawkshawMessage;
import ch.uzh.ifi.seal.hawkshaw.ui.messages.HawkshawMessageControl;

public class InspectorView extends ViewPart {
	public static final String VIEW_ID = "ch.uzh.ifi.seal.hawkshaw.ui.inspectorview";
	
	private Table table;
	private TableViewer viewer;
	
	private HawkshawMessageControl messageControl;
	
	private ClearInspectionAction clear;
	private SetDropModeAction replace;
	private SetDropModeAction union;
	private SetDropModeAction intersect;
	private SetDropModeAction diff;

	private DropMode dropMode;
	
	public InspectorView() {
	}
	
	@Override
	public void createPartControl(Composite parent) {
		GridLayout layout = new GridLayout(1, false);
		layout.marginWidth = 0;
		layout.marginHeight = 0;
		layout.horizontalSpacing = 0;
		layout.verticalSpacing = 0;
		parent.setLayout(layout);
		
		createTable(parent);
		createViewer();
		
		
		
		new InspectorViewDropSupport(this).start();
		
		initToolbar();
		initContextMenu();
		
		addWelcomeMessage(parent);
	}

	@Override
	public void setFocus() {
		if(!(table == null || table.isDisposed())) {
			table.setFocus();
		}
	}
	
	protected TableViewer getViewer() {
		return viewer;
	}
	
	protected DropMode getActiveDropMode() {
		return dropMode;
	}
	
	protected void setDropMode(DropMode dropMode) {
		if(dropMode == null) { // happens occasionally, not sure why
			return;
		}
		
		switch(dropMode) {
		case REPLACE:
			replace.setChecked(true);
			union.setChecked(false);
			intersect.setChecked(false);
			diff.setChecked(false);
			break;
		case UNION:
			replace.setChecked(false);
			union.setChecked(true);
			intersect.setChecked(false);
			diff.setChecked(false);
			break;
		case INTERSECTION:
			replace.setChecked(false);
			union.setChecked(false);
			intersect.setChecked(true);
			diff.setChecked(false);
			break;
		case DIFF:
			replace.setChecked(false);
			union.setChecked(false);
			intersect.setChecked(false);
			diff.setChecked(true);
			break;
		default:
			// should not happen
			break;
		}
		
		this.dropMode = dropMode;
		
	}
	
	protected void setInput(Set<QrItemWrapper> resources, boolean filtered) {
		setInput(resources, dropMode, filtered);
	}
	
	protected void setInput(Set<QrItemWrapper> resources, DropMode externalDropMode, boolean filtered) {
		viewer.setInput(new InspectorInput(resources.toArray(new QrItemWrapper[resources.size()]), externalDropMode));
		clear.setEnabled(true);
		
		if(filtered) {
			HawkshawMessage message = new HawkshawMessage();
			message.setTitle("Information filtered");
			message.setImage(QueryUI.getDefault().getImage(IHawkshawImages.FILTER_WARNING));
			message.setDescription("The data dragged into the Inspector contained elements that can not be processed. These elements have been filtered.");
			addMessage(message);
		} else {
			messageControl.closeMessage();
		}
	}
	
	protected void clear() {
		viewer.setInput(InspectorInput.EMPTY);
		clear.setEnabled(false);
	}

	private void createTable(Composite parent) {
		table = new Table(parent, SWT.H_SCROLL
								   | SWT.V_SCROLL
								   | SWT.SINGLE
								   | SWT.FULL_SELECTION);
		table.setHeaderVisible(true);
		table.setLinesVisible(true);
		
		GridData layoutData = new GridData();
		layoutData.horizontalAlignment       = SWT.FILL;
		layoutData.verticalAlignment         = SWT.FILL;
		layoutData.grabExcessHorizontalSpace = true;
		layoutData.grabExcessVerticalSpace   = true;
		
		table.setLayoutData(layoutData);
	}

	private void createViewer() {
		viewer = new TableViewer(table);
		viewer.setContentProvider(new InspectorTableContentProvider());
		viewer.setComparator(new InspectorComparator());
		
		ColumnViewerToolTipSupport.enableFor(viewer);
		
		getSite().setSelectionProvider(viewer);
	}

	private void initToolbar() {
		IActionBars actionBars = getViewSite().getActionBars();
		IToolBarManager toolbar = actionBars.getToolBarManager();
		
		replace = new SetDropModeAction(this, DropMode.REPLACE, "Replace with Drop", IHawkshawImages.SET);
		replace.setChecked(true);
		dropMode = DropMode.REPLACE;
		
		union = new SetDropModeAction(this, DropMode.UNION, "Enable Union Drop", IHawkshawImages.UNION);
		intersect = new SetDropModeAction(this, DropMode.INTERSECTION, "Enable Intersection Drop", IHawkshawImages.INTERSECTION);
		diff = new SetDropModeAction(this, DropMode.DIFF, "Enable Diff Drop", IHawkshawImages.DIFFERENCE);
		
		clear = new ClearInspectionAction(this);
		
		toolbar.add(replace);
		toolbar.add(union);
		toolbar.add(intersect);
		toolbar.add(diff);
		toolbar.add(new Separator("clearGroup"));
		toolbar.add(clear);
	}

	private void initContextMenu() {
		MenuManager mgr = new MenuManager();
		mgr.setRemoveAllWhenShown(true);
		mgr.addMenuListener(new ContextMenuListener(viewer));
		
		table.setMenu(mgr.createContextMenu(table));
	}
	
	private void addWelcomeMessage(Composite parent) {
		messageControl = new HawkshawMessageControl(parent);
		HawkshawMessage message = new HawkshawMessage();
		
		message.setTitle("Hawkshaw Inspector");
		message.setImage(QueryUI.getDefault().getImage(IHawkshawImages.INSP_ICON));
		message.setDescription("Ask a <a href=\"open-query-dialog\">Question</a> and drag its answer into the table above. Use the toolbar for selecting different drop modes. Like that, you can combine the answers of multiple questions.");
		
		messageControl.setMessage(message);
	}

	private static final class ContextMenuListener implements IMenuListener {
		private TableViewer viewer;
		private Table table;
		
		public ContextMenuListener(TableViewer viewer) {
			this.viewer = viewer;
			table = viewer.getTable();
		}
		
		public void menuAboutToShow(IMenuManager mgr) {
			fillContextMenu(mgr);
		}
		
		private void fillContextMenu(IMenuManager manager) {
			addColumnActions(manager);
			addOtherActions(manager);
		}
		
		private void addOtherActions(IMenuManager manager) {
			IStructuredSelection selection = (IStructuredSelection) viewer.getSelection();
			if(!selection.isEmpty()) {
				Object selectedElement = selection.getFirstElement();
				
				IJavaElement javaElement = null;
				if(selectedElement instanceof QrItemWrapper) {
					QrItemWrapper bean = (QrItemWrapper) selectedElement;
					javaElement = (IJavaElement) Platform.getAdapterManager().getAdapter(bean.getItem(), IJavaElement.class);
				}
				
				if(javaElement != null) {
					manager.add(new Separator());
					manager.add(new ShowSourceAction(javaElement));
				}
			}
		}
		
		private void addColumnActions(IMenuManager manager) {
			for( int i = 0; i < table.getColumnCount(); i++ ) {
				TableColumn column = table.getColumn(i);
				String propUri = (String) column.getData("propUri");
				if(!(IRDFS.LABEL.equals(propUri) || "#TYPE".endsWith(propUri))) {
					manager.add(new HideOrShowColumnAction(column));
				}
			}
		}
	}

	private static final class SetDropModeAction extends Action {
		private InspectorView callback;
		private DropMode dropMode;
		
		public SetDropModeAction(InspectorView callback, DropMode dropMode, String caption, ImageDescriptor descriptor) {
			super(caption, IAction.AS_CHECK_BOX);
			this.callback = callback;
			this.dropMode = dropMode;
			setImageDescriptor(descriptor);
		}
		
		@Override
		public void run() {
			callback.setDropMode(dropMode);
		}
	}
	
	private static final class ClearInspectionAction extends Action {
		private InspectorView callback;
		
		public ClearInspectionAction(InspectorView callback) {
			super("Clear Inspection");
			this.callback = callback;
			
			setImageDescriptor(IHawkshawImages.REMOVE_ALL);
			setEnabled(false);
		}
		
		@Override
		public void run() {
			callback.clear();
		}
	}

	protected void addMessage(HawkshawMessage message) {
		messageControl.setMessage(message);
	}
}
