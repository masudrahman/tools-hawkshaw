package ch.uzh.ifi.seal.hawkshaw.ui.views.inspector;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ui
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import org.eclipse.jface.action.Action;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Event;
import org.eclipse.swt.widgets.TableColumn;

public class HideOrShowColumnAction extends Action {
	private TableColumn column;
	
	public HideOrShowColumnAction(TableColumn column) {
		super(column.getText(), SWT.CHECK);
		setChecked(column.getWidth() > 0);
		this.column = column;
	}
	
	@Override
	public void runWithEvent(Event event) {
		Thread t;
		
		if(!isChecked()) {
			t = new ShrinkThread(column.getWidth(), column);
		} else {
			t = new ExpandThread(((Integer)column.getData("restoredWidth")).intValue(), column);
		}
		
		t.start();
	}
	
	private static final class ShrinkThread extends Thread {
		private int width;
		private TableColumn column;
		
		public ShrinkThread(int width, TableColumn column) {
			this.width = width;
			this.column = column;
		}
		
		public void run() {
			column.getDisplay().syncExec(new Runnable() {
				public void run() {
					column.setData("restoredWidth", Integer.valueOf(width));
					column.setWidth(0);
				}
			});
		}
	};
	
	private static final class ExpandThread extends Thread {
		private int width;
		private TableColumn column;
		
		public ExpandThread(int width, TableColumn column) {
			this.width = width;
			this.column = column;
		}
		
		public void run() {
			column.getDisplay().syncExec(new Runnable() {
				public void run() {
					column.setWidth(width);
				}
			});
		}
	}
}
