package ch.uzh.ifi.seal.hawkshaw.ontology.java.builder.model;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ontology.java
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.List;
import java.util.Stack;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IPath;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.dom.ASTVisitor;
import org.eclipse.jdt.core.dom.AnonymousClassDeclaration;
import org.eclipse.jdt.core.dom.ConstructorInvocation;
import org.eclipse.jdt.core.dom.EnumDeclaration;
import org.eclipse.jdt.core.dom.FieldAccess;
import org.eclipse.jdt.core.dom.FieldDeclaration;
import org.eclipse.jdt.core.dom.IBinding;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.IVariableBinding;
import org.eclipse.jdt.core.dom.Initializer;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.Modifier;
import org.eclipse.jdt.core.dom.Name;
import org.eclipse.jdt.core.dom.QualifiedName;
import org.eclipse.jdt.core.dom.SimpleName;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;
import org.eclipse.jdt.core.dom.Type;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;

import ch.uzh.ifi.seal.hawkshaw.ontology.java.misc.JDTUtil;
import ch.uzh.ifi.seal.hawkshaw.ontology.misc.UriUtil;

public class CompilationUnitVisitor extends ASTVisitor {
	private JavaOntModel model;
	
	private ICompilationUnit cu;
	
	private FileHandle fileHandle;
	
	private PackageHandle packageHandle;

	private Stack<AbstractTypeHandle> typeStack;
	private Stack<AbstractCallableHandle> callableStack;
	
	public CompilationUnitVisitor(ICompilationUnit cu, String qualifiedPackageName, JavaOntModel model) {
		this.cu = cu;
		this.model = model;

		typeStack = new Stack<>();
		callableStack = new Stack<>();
		
		initFileHandle();
		initPackageHandle(qualifiedPackageName, model);
	}

	@Override
	public boolean visit(AnonymousClassDeclaration node) {
		return false; // FIXME not yet supported.
	}
	
	@Override
	public boolean visit(EnumDeclaration node) {
		return false; // FIXME not yet supported.
	}
	
	@Override
	public boolean visit(Initializer node) {
		return false; // FIXME not yet supported.
	}
		
	@Override
	public boolean visit(TypeDeclaration node) {
		if(noCurrentType()) {
			AbstractTypeHandle typeHandle = createTypeFrom(node);
			addModifiersTo(typeHandle, node.getModifiers());
			addToFile(typeHandle);
			addInterfaces(typeHandle, node.superInterfaceTypes());
			remember(typeHandle);
		} else {
			// typeStack.peek().addClass
			return false; // TODO inner classes shall be skipped for now
		}
		
		return super.visit(node);
	}

	@Override
	public void endVisit(TypeDeclaration node) {
		if(!node.isLocalTypeDeclaration() && !node.isMemberTypeDeclaration()) { // FIXME local inner and member types *sigh*
			forgetCurrentType();
		}
	}

	@Override
	public boolean visit(FieldDeclaration node) {
		AbstractTypeHandle currentTypeHandle = currentType();
		Type fieldType = node.getType();
		
		List<?> fragments = node.fragments();
		for(Object o : fragments) {
			if(o instanceof VariableDeclarationFragment) {
				VariableDeclarationFragment fragment = (VariableDeclarationFragment) o;
				
				String fieldIdentifier = JDTUtil.identifierFor(fragment);
				String encodedParentTypeQualifier = JDTUtil.resolveAndEncode((TypeDeclaration) node.getParent()); // FIXME fails for anonymous class declarations
				String encodedFieldQualfier = encodedParentTypeQualifier + '.' + fieldIdentifier;
				
				FieldHandle fieldHandle = currentTypeHandle.addField(encodedFieldQualfier, fieldIdentifier, new SourceCodeLocationHelper(cu, fragment));
				fieldHandle.setType(getTypeHandle(fieldType));
				addModifiersTo(fieldHandle, node.getModifiers());
			}
		}
		
		return false; // No need to descend deeper into the subtree - var decl fragments are already handled above.
	}

	@Override
	public boolean visit(MethodDeclaration node) {
		String encodedCallableQualifier = JDTUtil.resolveAndEncode(node);
		String callableIdentifier = JDTUtil.identifierFor(node);
		
		AbstractCallableHandle callableHandle;
		if(JDTUtil.isConstructor(node)) {
			callableHandle = currentType().addConstructor(encodedCallableQualifier, callableIdentifier, new SourceCodeLocationHelper(cu, node));
		} else {
			MethodHandle methodHandle = currentType().addMethod(encodedCallableQualifier, callableIdentifier, new SourceCodeLocationHelper(cu, node));			
			addReturnType(node, methodHandle);
			callableHandle = methodHandle;
		}
		
		addModifiersTo(callableHandle, node.getModifiers());
		
		// Parameters
		List<?> parameters = node.parameters();
		for(int i = 0; i < parameters.size(); i++){ 
			SingleVariableDeclaration svd = (SingleVariableDeclaration) parameters.get(i);
			Type paramType = svd.getType();
						
			String encodedParamQualifier = encodedCallableQualifier + '/' + UriUtil.encode(JDTUtil.identifierFor(svd));
			MethodParameterHandle paramHandle = callableHandle.addParameter(encodedParamQualifier, JDTUtil.identifierFor(svd), JDTUtil.identifierFor(paramType), new SourceCodeLocationHelper(cu, svd));			
			paramHandle.setType(getTypeHandle(paramType));
			paramHandle.setPosition(i);
		}
		
		// Exceptions
		ITypeBinding[] exceptions = node.resolveBinding().getExceptionTypes();
		for(ITypeBinding exType : exceptions) {
			if(!exType.isTypeVariable()) { // FIX for <T extends Exception>
				ClassHandle exceptionHandle = model.getExceptionHandleFor(JDTUtil.resolveAndEncode(exType), JDTUtil.identifierFor(exType));
				callableHandle.addThrowable(exceptionHandle);
			}
		}
		
		remember(callableHandle);
		
		return super.visit(node);
	}

	@Override
	public void endVisit(MethodDeclaration node) {
		forgetLastCallable();
	}
	
	@Override
	public boolean visit(FieldAccess node) {
		createAccessFrom(node.resolveFieldBinding(), new SourceCodeLocationHelper(cu, node));
			
		return super.visit(node);
	}

	@Override
    public boolean visit(QualifiedName node) {
    	createAccessFrom(node, new SourceCodeLocationHelper(cu, node));
    	
    	return super.visit(node);
    }

	@Override
	public boolean visit(SimpleName node) {
		createAccessFrom(node, new SourceCodeLocationHelper(cu, node));
		
		return super.visit(node);
	}
	
	@Override
	public boolean visit(MethodInvocation node) {
		IMethodBinding binding = node.resolveMethodBinding();
		if(binding.getDeclaringClass() != null && 
		   binding.getDeclaringClass().isTopLevel() /* hack, resolve */) {
			
			// A constructor/method can only invoke other methods - no need to check for constructors here.
			MethodHandle methodHandle = model.getMethodHandleFor(JDTUtil.resolveAndEncodeCallee(node), JDTUtil.identifierFor(node));

			currentCallable().addMethodInvocation(methodHandle, new SourceCodeLocationHelper(cu, node));
		}
				
		return super.visit(node);
	}
	
	@Override
	public boolean visit(ConstructorInvocation node) {
		IMethodBinding binding = node.resolveConstructorBinding();
		if (binding.getDeclaringClass() != null &&
			binding.getDeclaringClass().isTopLevel() /* hack, resolve */) {

			ConstructorHandle constructorHandle = model.getConstructorHandleFor(JDTUtil.resolveAndEncodeCallee(node), JDTUtil.identifierFor(node));

			currentCallable().addMethodInvocation(constructorHandle, new SourceCodeLocationHelper(cu, node));
		}

		return super.visit(node);
	}

	private void initFileHandle() {
		IResource resource = cu.getResource();
		IPath localPath = resource.getProjectRelativePath();
		fileHandle = model.getFileHandleFor(UriUtil.encode(localPath.toString()), resource.getName());
	}

	private void addToFile(AbstractTypeHandle typeHandle) {
		fileHandle.add(typeHandle);
	}

	private void initPackageHandle(String qualifiedPackageName,
			JavaOntModel model) {
		String tmpQualifiedPackageName;
		if(qualifiedPackageName.length() == 0) {
			tmpQualifiedPackageName = "<default package>";
		} else {
			tmpQualifiedPackageName = qualifiedPackageName;
		}
		
		
		packageHandle = model.getPackageHandleFor(UriUtil.encode(tmpQualifiedPackageName), tmpQualifiedPackageName);
	}

	private AbstractTypeHandle getTypeHandle(Type fieldType) {
		String encodedTypeQualifier = JDTUtil.resolveAndEncode(fieldType);
		String typeIdentifier = JDTUtil.identifierFor(fieldType);
		
		AbstractTypeHandle fieldTypeHandle;
		if(JDTUtil.isInterface(fieldType)) {
			fieldTypeHandle = model.getInterfaceHandleFor(encodedTypeQualifier, typeIdentifier);
		} else {
			if(JDTUtil.isException(fieldType)) {
				fieldTypeHandle = model.getExceptionHandleFor(encodedTypeQualifier, typeIdentifier);
			} else {
				fieldTypeHandle = model.getClassHandleFor(encodedTypeQualifier, typeIdentifier);
			}
		}
		return fieldTypeHandle;
	}

	private AbstractTypeHandle createTypeFrom(TypeDeclaration node) {
		String encodedTypeQualifier = JDTUtil.resolveAndEncode(node);
		String typeIdentifier = JDTUtil.identifierFor(node);
		
		SourceCodeLocationHelper location = new SourceCodeLocationHelper(cu, node);
		
		AbstractTypeHandle typeHandle;
		if(JDTUtil.isInterface(node)) {
			typeHandle = packageHandle.addInterface(encodedTypeQualifier, typeIdentifier, location);
		} else {
			ClassHandle classHandle;
			if(JDTUtil.isException(node)) {
				classHandle = packageHandle.addException(encodedTypeQualifier, typeIdentifier, location);
			} else {
				classHandle = packageHandle.addClass(encodedTypeQualifier, typeIdentifier, location);
			}
			
			addSuperClass(classHandle, node.getSuperclassType());
			typeHandle = classHandle;
		}
		
		return typeHandle;
	}

	private void createAccessFrom(IVariableBinding fieldBinding, SourceCodeLocationHelper location) {
		if(!noCurrentCallable() /* hack, resolve */ &&
			fieldBinding.getDeclaringClass() != null &&
			fieldBinding.getDeclaringClass().isTopLevel()) { // TODO Ignores initializers, oinit cinit.
			String fieldIdentifier = JDTUtil.identifierFor(fieldBinding);
			String encodedParentTypeQualifier = JDTUtil.transformAndEncode(JDTUtil.qualifierFor(fieldBinding.getDeclaringClass()));
			String encodedFieldQualifier = encodedParentTypeQualifier + '.' + fieldIdentifier;
			
			FieldHandle fieldHandle = model.getFieldHandleFor(encodedFieldQualifier, fieldIdentifier);
			AbstractCallableHandle methodHandle = currentCallable();
			methodHandle.addAccess(fieldHandle, location);
		}
	}

	private void createAccessFrom(Name node, SourceCodeLocationHelper location) {
		IBinding binding = node.resolveBinding();
		if(binding instanceof IVariableBinding) {
			IVariableBinding varBinding = (IVariableBinding) binding;
			
			if(varBinding.isField()) {
				createAccessFrom(varBinding, location);
			}
		}
	}

	private void addSuperClass(ClassHandle classHandle, Type superClassType) {
		if(superClassType != null) {
			ClassHandle superClassHandle;
			if(JDTUtil.isException(superClassType)) {
				superClassHandle = model.getExceptionHandleFor(JDTUtil.resolveAndEncode(superClassType), JDTUtil.identifierFor(superClassType));
			} else {
				superClassHandle = model.getClassHandleFor(JDTUtil.resolveAndEncode(superClassType), JDTUtil.identifierFor(superClassType));
			}
			classHandle.setSuperClass(superClassHandle);
		}
	}

	private void addInterfaces(AbstractTypeHandle typeHandle, List<?> interfaces) {
		for(Object o : interfaces) {
			if(o instanceof Type) {
				Type ift = (Type) o;
				InterfaceHandle interfaceHandle = model.getInterfaceHandleFor(JDTUtil.resolveAndEncode(ift), JDTUtil.identifierFor(ift));
				typeHandle.addInterface(interfaceHandle);
			}
		}
	}

	private void addReturnType(MethodDeclaration node, MethodHandle methodHandle) {
		Type returnType = node.getReturnType2();
		methodHandle.setReturnType(getTypeHandle(returnType));
	}

	private boolean noCurrentType() {
		return typeStack.empty();
	}

	private boolean noCurrentCallable() {
		return callableStack.empty();
	}

	private AbstractTypeHandle currentType() {
		return typeStack.peek();
	}

	private AbstractCallableHandle currentCallable() {
		return callableStack.peek();
	}

	private void remember(AbstractTypeHandle typeHandle) {
		typeStack.push(typeHandle);
	}

	private void remember(AbstractCallableHandle callableHandle) {
		callableStack.push(callableHandle);
	}

	private void forgetCurrentType() {
		typeStack.pop();
	}

	private void forgetLastCallable() {
		callableStack.pop();
	}
	
	private static void addModifiersTo(AbstractMemberHandle member, int flags) {
		if(Modifier.isPublic(flags)) {
			member.setPublic();
		} else if(Modifier.isPrivate(flags)) {
			member.setPrivate();
		} else if(Modifier.isProtected(flags)) {
			member.setProtected();
		} else {
			member.setDefault();
		}
		
		if(Modifier.isAbstract(flags)) {
			member.setAbstract();
		}
		
		if(Modifier.isStatic(flags)) {
			member.setStatic();
		}
		
		if(Modifier.isFinal(flags)) {
			member.setFinal();
		}
	}
}
