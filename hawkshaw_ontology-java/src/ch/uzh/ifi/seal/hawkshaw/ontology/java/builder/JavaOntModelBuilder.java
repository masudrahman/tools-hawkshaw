package ch.uzh.ifi.seal.hawkshaw.ontology.java.builder;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ontology.java
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import java.util.Map;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;

import ch.uzh.ifi.seal.hawkshaw.ontology.java.JavaOntologyCore;
import ch.uzh.ifi.seal.hawkshaw.ontology.java.builder.model.JavaOntModel;
import ch.uzh.ifi.seal.hawkshaw.ontology.model.AbstractHawkshawModel;

public class JavaOntModelBuilder extends IncrementalProjectBuilder {
	public static final String BUILDER_ID = "ch.uzh.ifi.seal.hawkshaw.ontology.java.javaOntModelBuilder";
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see org.eclipse.core.internal.events.InternalBuilder#build(int,
	 *      java.util.Map, org.eclipse.core.runtime.IProgressMonitor)
	 */	
	@Override @SuppressWarnings("rawtypes")
	protected IProject[] build(int kind, Map args, IProgressMonitor monitor) throws CoreException {
		SubMonitor progress = SubMonitor.convert(monitor, 100);
		
		if (kind == FULL_BUILD) {
			fullBuild(progress);
		} else { // AUTO_BUILD, INCREMENTAL_BUILD, or CLEAN_BUILD.
			IResourceDelta delta = getDelta(getProject());
			if (delta == null) {
				fullBuild(progress);
			} else {
				incrementalBuild(delta, progress);
			}
		}
		
		return null; // we're not interested in the deltas of other projects - the current one is implicitly included anyway.
	}
	
	@Override
	protected void clean(IProgressMonitor monitor) {
		IJavaProject javaProject = getJavaProject();
		
		SubMonitor subMonitor = SubMonitor.convert(monitor, 10000);
		subMonitor.subTask("Cleaning model for " + javaProject.getElementName());
		
		Clean clean = new Clean(javaProject);
		clean.start();
		
		while(clean.isAlive()) {
			subMonitor.setWorkRemaining(10000);
			subMonitor.worked(1);
			
			try {
				Thread.sleep(20);
			} catch (InterruptedException e) {
				// do nothing
			}
		}
	}

	private void fullBuild(SubMonitor monitor) throws CoreException {
		IJavaProject javaProject = getJavaProject();
		
		if(!JavaOntologyCore.ready()) {
			JavaOntologyCore.waitForInit(null);
		}
		
		if(javaProject != null) { // there will be no analysis if project is not an IJavaProject.
			JavaOntModel model = JavaOntologyCore.fetchModelFor(javaProject);
			
			ProjectAnalyzer analyzer = new ProjectAnalyzer(model);
			analyzer.performFullAnalysis(javaProject, monitor);
		}
	}

	private void incrementalBuild(IResourceDelta delta, IProgressMonitor monitor) throws CoreException {
		// SubMonitor subMonitor = SubMonitor.convert(monitor);
		// reminder: do not relay to full build - this would trigger each time file is saved!
	}

	private IJavaProject getJavaProject() {
		return JavaCore.create(getProject());
	}
	
	private static final class Clean extends Thread {
		private IJavaProject javaProject;
		
		public Clean(IJavaProject javaProject) {
			this.javaProject = javaProject;
		}
		
		@Override
		public void run() {
			if(!JavaOntologyCore.ready()) {
				JavaOntologyCore.waitForInit(null);
			}

			if(JavaOntologyCore.modelExists(javaProject)) {
				AbstractHawkshawModel model = JavaOntologyCore.fetchModelFor(javaProject);
				model.clear();
			}
		}
	}
}
