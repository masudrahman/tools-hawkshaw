package ch.uzh.ifi.seal.hawkshaw.ontology.java.builder.model;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ontology.java
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import ch.uzh.ifi.seal.hawkshaw.ontology.java.owl.vocabulary.IHawkshawJavaOntology;

import com.hp.hpl.jena.ontology.Individual;

/**
 * Handle for Java classes.
 * 
 * @author wuersch
 *
 */
public class ClassHandle extends AbstractTypeHandle {
	/**
	 * Constructor.
	 * 
	 * @param theClass
	 *            the class to which this handle refers to.
	 * @param model
	 *            the associated Java source code model.
	 */
	public ClassHandle(Individual theClass, JavaOntModel model) {
		super(theClass, model);
	}
	
	/**
	 * Sets the superclass of the underlying class. Does not perform any
	 * validation - callers of the method need to ensure that they do not
	 * accidentally introduce multiple inheritance (which is not possible in
	 * Java).
	 * 
	 * @param superClassHandle
	 *            the handle of the superclass.
	 */
	public void setSuperClass(ClassHandle superClassHandle) {
		addRelation(IHawkshawJavaOntology.HAS_SUPERCLASS, superClassHandle);
	}
	
	/**
	 * Adds an interface to the underlying class.
	 * 
	 * @param the handle of the interface.
	 */
	public void addInterface(InterfaceHandle interfaceHandle) {
		addRelation(IHawkshawJavaOntology.IMPLEMENTS_INTERFACE, interfaceHandle);
	}
}
