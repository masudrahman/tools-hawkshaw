package ch.uzh.ifi.seal.hawkshaw.ontology.java.facade;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ontology.java
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import org.eclipse.jdt.core.IJavaProject;

public class BuildStatus {
	public static final int NONE       = 0;
	public static final int HAS_NATURE = 1 << 1;
	public static final int HAS_MODEL  = 1 << 2;
	public static final int IS_CLEAN   = 1 << 3;
	
	private IJavaProject javaProject;
	private int statusCode;
	
	public BuildStatus(IJavaProject javaProject, int statusCode) {
		this.javaProject = javaProject;
		this.statusCode = statusCode;
	}
	
	public IJavaProject getJavaProject() {
		return javaProject;
	}
	
	public int getStatus() {
		return statusCode;
	}
	
	public boolean hasStatus(int otherStatusCode) {
		return (statusCode & otherStatusCode) != 0;
	}
	
	public boolean lacksStatus(int otherStatusCode) {
		return !hasStatus(otherStatusCode);
	}

	public boolean isReady() {
		return hasStatus(HAS_NATURE) &&
			   hasStatus(HAS_MODEL) &&
			   hasStatus(IS_CLEAN);
	}
}
