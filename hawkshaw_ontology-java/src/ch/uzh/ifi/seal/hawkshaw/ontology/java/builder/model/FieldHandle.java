package ch.uzh.ifi.seal.hawkshaw.ontology.java.builder.model;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ontology.java
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import ch.uzh.ifi.seal.hawkshaw.ontology.java.owl.vocabulary.IHawkshawJavaOntology;

import com.hp.hpl.jena.ontology.Individual;

/**
 * Handle for fields or attributes of Java classes.
 * 
 * @author wuersch
 *
 */
public class FieldHandle extends AbstractMemberHandle {
	/**
	 * Constructor.
	 * 
	 * @param theField
	 *            the field to which this handle refers to.
	 * @param model
	 *            the associated Java source code model.
	 */
	public FieldHandle(Individual theField, JavaOntModel model) {
		super(theField, model);
	}
	
	/**
	 * Sets the type (class or interface) of the field.
	 * 
	 * @param fieldTypeHandle the handle of the field's type.
	 */
	public void setType(AbstractTypeHandle fieldTypeHandle) {
		addRelation(IHawkshawJavaOntology.HAS_DATA_TYPE, fieldTypeHandle);
	}
}
