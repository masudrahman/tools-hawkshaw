package ch.uzh.ifi.seal.hawkshaw.ontology.java.builder.model;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ontology.java
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import ch.uzh.ifi.seal.hawkshaw.ontology.java.owl.vocabulary.IHawkshawJavaOntology;

import com.hp.hpl.jena.ontology.Individual;

/**
 * Abstract base class for constructor and method handles. The distinction is
 * important, although a constructor is a special method. However, Java
 * Interfaces and Annotations cannot have constructors. Further a constructor's
 * signature differs from a method's one. Constructor signatures can only have
 * access modifiers, i.e., they cannot be 'abstract', 'final', 'native',
 * 'static', or synchronized. They cannot return anything and they need to have
 * the same identifier as the class they're defined in.
 * 
 * @author wuersch
 * 
 */
public class AbstractCallableHandle extends AbstractMemberHandle {

	/**
	 * Constructor.
	 * 
	 * @param theEntity
	 *            the method or constructor to which this handle refers to.
	 * @param model
	 *            the related Java model.
	 */
	public AbstractCallableHandle(Individual theEntity, JavaOntModel model) {
		super(theEntity, model);
	}

	public void addThrowable(ClassHandle exceptionHandle) {
		addRelation(IHawkshawJavaOntology.THROWS_EXCEPTION, exceptionHandle);
	}

	/**
	 * Adds a parameter to this method.
	 * 
	 * @param type
	 *            the handle for the type of the parameter.
	 * @param localName
	 *            the parameter name.
	 * @param location
	 *            location the source code location of the parameter declaration.
	 */
	public MethodParameterHandle addParameter(String qualifiedMethodParameterName, String localName, String typeName,
			SourceCodeLocationHelper location) {
				MethodParameterHandle paramHandle = model().getMethodParameterHandleFor(qualifiedMethodParameterName, localName, typeName);
				
				addRelation(IHawkshawJavaOntology.HAS_PARAMETER, paramHandle);
				
				return paramHandle;
			}

	/**
	 * Adds a method call from this method to another one.
	 * 
	 * @param calleeHandle
	 *            the handle for the called method.
	 * @param location
	 *            location the source code location of the invocation.
	 */
	public void addMethodInvocation(AbstractCallableHandle calleeHandle, SourceCodeLocationHelper location) { // TODO source location? reification?
		addRelation(IHawkshawJavaOntology.INVOKES_METHOD, calleeHandle);
	}

	/**
	 * Adds a field access from this method.
	 * 
	 * @param fieldHandle
	 *            the handle for the accessed field.
	 * @param location
	 *            location the source code location of the field access.
	 */
	public void addAccess(FieldHandle fieldHandle, SourceCodeLocationHelper location) { // TODO source location? reification?
		addRelation(IHawkshawJavaOntology.ACCESSES_FIELD, fieldHandle);	
	}

}
