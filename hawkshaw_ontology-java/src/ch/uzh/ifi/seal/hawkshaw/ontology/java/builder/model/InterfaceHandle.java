package ch.uzh.ifi.seal.hawkshaw.ontology.java.builder.model;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ontology.java
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import ch.uzh.ifi.seal.hawkshaw.ontology.java.owl.vocabulary.IHawkshawJavaOntology;

import com.hp.hpl.jena.ontology.Individual;

/**
 * Handle for Java interfaces.
 * 
 * @author wuersch
 *
 */
public class InterfaceHandle extends AbstractTypeHandle {
	/**
	 * Constructor.
	 * 
	 * @param theInterface
	 *            the interface to which this handle refers to.
	 * @param model
	 *            the associated Java source code model.
	 */
	public InterfaceHandle(Individual theInterface, JavaOntModel model) {
		super(theInterface, model);
	}

	/**
	 * Sets the super interface of the underlying interface. Does not perform
	 * any validation - callers of the method need to ensure that they do not
	 * accidentally introduce multiple inheritance (which is not possible in
	 * Java).
	 * 
	 * @param superInterfaceHandle
	 *            the handle of the super interface.
	 */
	public void setSuperInterface(InterfaceHandle superInterfaceHandle) {
		addRelation(IHawkshawJavaOntology.HAS_SUPER_INTERFACE, superInterfaceHandle);
	}

	/**
	 * Adds an interface to the underlying interface. Basically a delegate for
	 * {@link #setSuperInterface(InterfaceHandle)}.
	 * 
	 * @param the
	 *            handle of the interface.
	 */
	@Override
	public void addInterface(InterfaceHandle interfaceHandle) {
		setSuperInterface(interfaceHandle);
	}
}
