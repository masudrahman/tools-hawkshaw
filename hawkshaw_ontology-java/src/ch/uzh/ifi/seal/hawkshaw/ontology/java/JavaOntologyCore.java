package ch.uzh.ifi.seal.hawkshaw.ontology.java;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ontology.java
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IncrementalProjectBuilder;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.Plugin;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.jobs.IJobManager;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jdt.core.IJavaProject;
import org.osgi.framework.BundleContext;

import ch.uzh.ifi.seal.hawkshaw.ontology.OntologyCore;
import ch.uzh.ifi.seal.hawkshaw.ontology.java.builder.JavaOntModelNature;
import ch.uzh.ifi.seal.hawkshaw.ontology.java.builder.model.JavaOntModel;
import ch.uzh.ifi.seal.hawkshaw.ontology.java.facade.BuildStatus;
import ch.uzh.ifi.seal.hawkshaw.ontology.model.PersistentJenaModel;
import ch.uzh.ifi.seal.hawkshaw.support.exceptions.HawkshawException;
import ch.uzh.ifi.seal.hawkshaw.support.plugin.IHawkshawPlugin;

/**
 * The activator class controls the plug-in life cycle
 */
public class JavaOntologyCore extends Plugin implements IHawkshawPlugin{

	// The plug-in ID
	public static final String PLUGIN_ID = "ch.uzh.ifi.seal.hawkshaw.ontology.java";

	// The shared instance
	private static JavaOntologyCore plugin;
	
	private JavaModelCache modelCache;

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.core.runtime.Plugins#start(org.osgi.framework.BundleContext)
	 */
	public void start(BundleContext context) throws Exception {
		super.start(context);
		plugin = this;
		
		initModelCache();
	}
	
	public static boolean ready() {
		return OntologyCore.ready();
	}
	
	public static void waitForInit(IProgressMonitor monitor) {
		SubMonitor progress = SubMonitor.convert(monitor, 100);
		OntologyCore.waitForInit(progress.newChild(100));
	}
	
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.eclipse.core.runtime.Plugin#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext context) throws Exception {
		disposeModelCache();

		plugin = null;
		super.stop(context);
	}

	@Override
	public String getID() {
		return PLUGIN_ID;
	}

	/**
	 * Returns the shared instance
	 * 
	 * @return the shared instance
	 */
	public static JavaOntologyCore getDefault() {
		return plugin;
	}
		
	public static JavaOntModel fetchModelFor(IJavaProject javaProject) {		
		JavaModelCache cache = getDefault().modelCache;
		JavaOntModel javaModel = cache.getModelFor(javaProject);
		
		if(javaModel == null) {
			PersistentJenaModel baseModel = OntologyCore.fetchPersistentModelFor(javaProject.getProject());
			
			javaModel = new JavaOntModel(baseModel);

			cache.cache(javaProject, javaModel);
		}
		
		return javaModel;
	}

	public static boolean hasNatureEnabled(IProject project) throws HawkshawException {
		return JavaOntModelNature.isAppliedTo(project);
	}
	
	public static boolean hasNatureEnabled(IJavaProject javaProject) {
		return JavaOntModelNature.isAppliedTo(javaProject);
	}
	
	public static void addNatureTo(IJavaProject javaProject, IProgressMonitor monitor) {
		JavaOntModelNature.applyTo(javaProject, monitor);
	}
	
//	// TODO build stuff should go elsewhere
	public static void build(IJavaProject javaProject, IProgressMonitor monitor) {
		try {
			IProject project = javaProject.getProject();
			project.build(IncrementalProjectBuilder.FULL_BUILD, monitor);
		} catch (CoreException cex) {
			throw new HawkshawException(getDefault(), "Build did not succeed.", cex); // TODO own exception
		}
	}
	
	public static BuildStatus getBuildStatusFor(IJavaProject javaProject) {
		int statusCode = BuildStatus.NONE;
		
		if(hasNatureEnabled(javaProject)) {
			statusCode |= BuildStatus.HAS_NATURE;
		}
		
		if(modelExists(javaProject)) {
			statusCode |= BuildStatus.HAS_MODEL;
			
			if(modelClean(javaProject)) {
				statusCode |= BuildStatus.IS_CLEAN;
			}
		}
		
		return new BuildStatus(javaProject, statusCode);
	}
	
	public static boolean modelClean(IJavaProject javaProject) {
		return OntologyCore.modelClean(javaProject.getProject());
	}

	// TODO build stuff should go elsewhere
	public static void waitForBuild(IProgressMonitor monitor) {
		SubMonitor subMonitor = SubMonitor.convert(monitor, 100);
		
		IJobManager jobManager = Job.getJobManager();
		
		// we need to ensure that the build has completed:
		// for some strange reason join-thread gets interrupted
		// once in a while (at least on windows). In this case, we keep on
		// joining...
		boolean buildHasntFinishedYet = true;
		while(buildHasntFinishedYet && !subMonitor.isCanceled()) {
			subMonitor.setWorkRemaining(100);
			
			try {
				jobManager.join(ResourcesPlugin.FAMILY_AUTO_BUILD, subMonitor.newChild(50));
				jobManager.join(ResourcesPlugin.FAMILY_MANUAL_BUILD, subMonitor.newChild(50));
				
				buildHasntFinishedYet = false;
			} catch (InterruptedException e) {
				buildHasntFinishedYet = true;
			}
		}
	}

	public static boolean modelExists(IJavaProject javaProject) {
		return OntologyCore.modelExists(javaProject.getProject());
	}

	private void initModelCache() {
		modelCache = new JavaModelCache();
	}

	private void disposeModelCache() {
		modelCache.clear();
		modelCache = null;
	}
}