package ch.uzh.ifi.seal.hawkshaw.ontology.team.model;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ontology.team
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.util.Calendar;

import ch.uzh.ifi.seal.hawkshaw.ontology.model.AbstractModelEntityHandle;
import ch.uzh.ifi.seal.hawkshaw.ontology.team.owl.vocabulary.IHawkshawHistory;

import com.hp.hpl.jena.ontology.Individual;

public class FileHandle extends AbstractModelEntityHandle<HistoryOntModel> {

	public FileHandle(Individual theEntity, HistoryOntModel model) {
		super(theEntity, model);
	}
	
	public VersionHandle addVersion(String uniqueIdentifier, String contentIdentifier) {
		VersionHandle versionHandle = model().getVersionHandleFor(uniqueIdentifier, contentIdentifier); // TODO good uri?
		addRelation(IHawkshawHistory.HAS_VERSION, versionHandle);		
		return versionHandle;
	}
	
	public void setModifiedOn(long timestamp) {
		Calendar c = Calendar.getInstance();
		c.setTimeInMillis(timestamp);
		
		addLiteral(IHawkshawHistory.MODIFIED_ON, c);
	}
}
