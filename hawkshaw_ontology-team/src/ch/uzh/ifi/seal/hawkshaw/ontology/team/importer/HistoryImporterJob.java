package ch.uzh.ifi.seal.hawkshaw.ontology.team.importer;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ontology.team
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


import java.util.concurrent.TimeUnit;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.SubMonitor;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.team.core.RepositoryProvider;
import org.eclipse.team.core.history.IFileHistory;
import org.eclipse.team.core.history.IFileHistoryProvider;
import org.eclipse.team.core.history.IFileRevision;

import ch.uzh.ifi.seal.hawkshaw.ontology.OntologyCore;
import ch.uzh.ifi.seal.hawkshaw.ontology.misc.UriUtil;
import ch.uzh.ifi.seal.hawkshaw.ontology.model.PersistentJenaModel;
import ch.uzh.ifi.seal.hawkshaw.ontology.team.TeamOntologyCore;
import ch.uzh.ifi.seal.hawkshaw.ontology.team.model.FileHandle;
import ch.uzh.ifi.seal.hawkshaw.ontology.team.model.HistoryOntModel;
import ch.uzh.ifi.seal.hawkshaw.ontology.team.model.VersionHandle;
import ch.uzh.ifi.seal.hawkshaw.support.exceptions.HawkshawException;

public class HistoryImporterJob extends Job {
	public static final Object HISTORY_JOB_FAMILY = new Object();
	
	private IProject project;
	private HistoryOntModel model;
	
	private IFileHistoryProvider historyProvider;
	
	private int totalCompilationUnits;
	private int worked;
	private int totalPackages;
	private int revisionCounter;
	
	private long duration;

	public HistoryImporterJob(IProject project) {
		super("Importing History");
		this.project = project;
		
		totalPackages         = 0;
		totalCompilationUnits = 0;
		worked                = 0;
		revisionCounter       = 0;
		
		duration              = 0;
		
		setUser(true);
	}
	
	@Override
	public boolean belongsTo(Object family) {
		return HISTORY_JOB_FAMILY.equals(family);
	}

	public String getProjectName() {
		return project.getName();
	}
	
	public int getNumberOfImportedRevisions() {
		return revisionCounter;
	}
	
	public String getDurationString() {
		return String.format("%d min, %d sec", 
							 TimeUnit.MILLISECONDS.toMinutes(duration),
							 TimeUnit.MILLISECONDS.toSeconds(duration) - 
							 TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(duration))
		);
	}
	
	@Override
	protected IStatus run(IProgressMonitor monitor) {
		SubMonitor subMonitor = SubMonitor.convert(monitor);
		
		IStatus status;
		
		boolean success = initHistoryProvider();
		if(success) {
			initModel(); // only init history onts if history provider is present
			
			long start = System.currentTimeMillis();
			status = processJavaProject(JavaCore.create(project), subMonitor);
			long end   = System.currentTimeMillis();
			
			duration = end - start;
		} else {
			status = new Status(Status.ERROR,
								TeamOntologyCore.getDefault().getID(),
								"Could not initialize the team provider. " +
								"It seems like project " + project.getName() +
								" is not under source control.");
		}
		
		return status;
	}

	private boolean initHistoryProvider() {
		RepositoryProvider repositoryProvider = RepositoryProvider.getProvider(project);
		
		boolean success;
		if(repositoryProvider != null) {
			historyProvider = repositoryProvider.getFileHistoryProvider();
			success = true;
		} else {
			success = false;
		}
		
		return success;
	}

	private void initModel() {
		OntologyCore.waitForInit(null);
		PersistentJenaModel base = OntologyCore.fetchPersistentModelFor(project);
		model = new HistoryOntModel(base);
	}

	private IStatus processJavaProject(IJavaProject javaProject, SubMonitor subMonitor) {
		IStatus result = Status.OK_STATUS; // we're optimistic
		
		assessAmountOfWork(javaProject);
		subMonitor.subTask("Performing historical analysis of " + javaProject.getElementName() + " (" + totalCompilationUnits + " Java files)");
		
		try {
			IPackageFragment[] packageFragments = javaProject.getPackageFragments();
			
			subMonitor.setWorkRemaining(totalPackages);
			
			for(IPackageFragment packageFragment : packageFragments) {
				if(subMonitor.isCanceled()) {
					result = Status.CANCEL_STATUS;
					break;
				}
				
				if(packageFragment.getKind() == IPackageFragmentRoot.K_SOURCE && packageFragment.containsJavaResources()) {
					processPackage(packageFragment, subMonitor.newChild(1));
				}
			}
		} catch(JavaModelException jme) {
			result = new Status(Status.ERROR,
								TeamOntologyCore.getDefault().getID(),
								Status.OK /* unused */,
								"Error while processing history of " + project.getName() + ".",
								jme);
		}
		
		return result;
	}

	private void processPackage(IPackageFragment packageFragment, IProgressMonitor monitor) {
		SubMonitor subMonitor = SubMonitor.convert(monitor);
		subMonitor.subTask("Processing " + packageFragment.getElementName());
		
		try {
			ICompilationUnit[] compilationUnits = packageFragment.getCompilationUnits();
			
			subMonitor.setWorkRemaining(compilationUnits.length);
			
			for (ICompilationUnit compilationUnit : compilationUnits) {
				if(subMonitor.isCanceled()) {
					return;
				}
				
				processCompilationUnit(compilationUnit, subMonitor.newChild(1));
			}
		} catch(JavaModelException jmex) {
			throw new HawkshawException(TeamOntologyCore.getDefault(), "Error while processing history of package '" + packageFragment.getElementName() + "'. ", jmex); // TODO Build exception?
		}
	}

	private void processCompilationUnit(ICompilationUnit compilationUnit, IProgressMonitor monitor) {
		IResource resource = compilationUnit.getResource();
		String filePath = resource.getProjectRelativePath().toString();
		
		FileHandle fileHandle = model.getFileHandleFor(UriUtil.encode(filePath), resource.getName());
		
		IFileHistory history = historyProvider.getFileHistoryFor(resource, IFileHistoryProvider.NONE, null);
		IFileRevision[] revisions = history.getFileRevisions();		
		
		SubMonitor subMonitor = SubMonitor.convert(monitor, revisions.length);
		subMonitor.subTask("Analyzing history (" + (++worked) + '/' + totalCompilationUnits + "): " + compilationUnit.getElementName());
		
		for(IFileRevision revision : revisions) {
			if(subMonitor.isCanceled()) {
				return;
			}
			
			String contentIdentifier = revision.getContentIdentifier();
			
			VersionHandle versionHandle = fileHandle.addVersion(UriUtil.encode(filePath + '/' + contentIdentifier), contentIdentifier);
			versionHandle.setFilePath(filePath);
			versionHandle.setTimestamp(revision.getTimestamp());
			versionHandle.setComment(revision.getComment());
			
			versionHandle.addCommitter(revision.getAuthor());

			fileHandle.setModifiedOn(revision.getTimestamp());
			
			revisionCounter++;
			
			subMonitor.worked(1);
		}

	}

	private void assessAmountOfWork(IJavaProject javaProject) {
		int packageCounter = 0;
		int compilationUnitCounter = 0;
		
		try {
			for(IPackageFragment packageFragment :javaProject.getPackageFragments()) {
				if(packageFragment.getKind() == IPackageFragmentRoot.K_SOURCE && packageFragment.containsJavaResources()) {
					ICompilationUnit[] units = packageFragment.getCompilationUnits();
					
					packageCounter++;
					compilationUnitCounter += units.length;
				}
			}
		} catch(JavaModelException jme) {
			throw new HawkshawException(TeamOntologyCore.getDefault(), "Error while assessing the amount of work.", jme);
		}
		
		totalPackages = packageCounter;
		totalCompilationUnits = compilationUnitCounter;
	}
}
