package ch.uzh.ifi.seal.hawkshaw.ontology.team.owl.vocabulary;

/*
 * #%L
 * ch.uzh.ifi.seal.hawkshaw.ontology.team
 * %%
 * Copyright (C) 2011 - 2013 Software Architecture and Evolution Lab, Department of Informatics, UZH
 * %%
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *      http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * #L%
 */


public interface IHawkshawHistory {
	String BASE                       = "http://se-on.org/ontologies/hawkshaw/2012/02/history.owl";
	String LOCAL                      = "local_hawkshaw_history_12_02";
	
	// Classes
	String FILE_UNDER_VERSION_CONTROL = BASE + "#FileUnderVersionControl";
	String VERSION                    = BASE + "#Version";
	String COMMITTER                  = BASE + "#Committer";
	
	// Object properties:
	String COMMITS_VERSION            = BASE + "#commitsVersion";
	String IS_COMMITTED_BY            = BASE + "#isCommittedBy";
	String HAS_VERSION                = BASE + "#hasVersion";
	String IS_VERSION_OF              = BASE + "#isVersionOf";
//	String COUPLED_WITH = BASE + "#coupledWith";
	
	// Datatype properties:
	String COMMITTED_ON               = BASE + "#committedOn";
	String HAS_COMMIT_MESSAGE         = BASE + "#hasCommitMessage";
	String HAS_CONTENT_IDENTIFIER     = BASE + "#hasContentIdentifier";
	String MODIFIED_ON                = BASE + "#modifiedOn";
	String FILE_PATH                  = BASE + "#filePath";
}
